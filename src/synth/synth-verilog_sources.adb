with Netlists.Locations; use Netlists.Locations;

package body Synth.Verilog_Sources is
   procedure Set_Location2 (N : Net; Src : Node) is
   begin
      Set_Location (N, Get_Location (Src));
   end Set_Location2;

   procedure Set_Location (N : Net; Src : Node) is
   begin
      if Flag_Locations then
         Set_Location2 (N, Src);
      end if;
   end Set_Location;
end Synth.Verilog_Sources;
