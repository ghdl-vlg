with Netlists; use Netlists;

with Verilog.Nodes; use Verilog.Nodes;

package Synth.Verilog_Sources is
   procedure Set_Location (N : Net; Src : Node);
end Synth.Verilog_Sources;
