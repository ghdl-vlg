with Verilog.Nodes; use Verilog.Nodes;

with Synth.Verilog_Context; use Synth.Verilog_Context;

package Synth.Verilog_Elaboration is
   procedure Elaborate_Global;

   --  Allocate a scope for the parameters of module instance N and compute
   --  the parameters.
   function Elaborate_Sub_Instance_Params
     (Parent_Inst : Synth_Instance_Acc; Module : Node)
     return Synth_Instance_Acc;

   --  Complete sub instance elaboration.
   procedure Elaborate_Sub_Instance_Complete
     (Module : Node; Inst : Synth_Instance_Acc);
end Synth.Verilog_Elaboration;
