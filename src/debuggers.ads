with System;

package Debuggers is
   type Menu_Procedure is access procedure (Line : String);

   --  This exception can be raised by a debugger command to directly return
   --  to the prompt.
   Command_Error : exception;

   --  If set (by commands), call this procedure on empty line to repeat
   --  last command.
   Cmd_Repeat : Menu_Procedure;

   type Menu_Kind is (Menu_Command, Menu_Submenu);
   type Menu_Entry (Kind : Menu_Kind);
   type Menu_Entry_Acc is access all Menu_Entry;

   type Cst_String_Acc is access constant String;

   Menu_Top : Menu_Entry_Acc;

   --  Return the index of the first non-blank character.
   function Skip_Blanks (S : String) return Positive;

   --  Return entry for command CMD.
   function Find_Menu (Menu : Menu_Entry_Acc; Cmd : String)
                      return Menu_Entry_Acc;

   --  Return the position of the last character of the word (the last
   --  non-blank character).
   function Get_Word (S : String) return Positive;

   --  Extract command from LINE, return index P of the next argument and
   --  command MENU.
   procedure Parse_Command (Line : String;
                            P : in out Natural;
                            Menu : out Menu_Entry_Acc);

   --  The status of the debugger.  This status can be modified by a command
   --  as a side effect to resume or quit the debugger.
   type Command_Status_Type is (Status_Default, Status_Quit);
   Command_Status : Command_Status_Type;

   type Menu_Entry (Kind : Menu_Kind) is record
      Name : Cst_String_Acc;
      Next : Menu_Entry_Acc;

      case Kind is
         when Menu_Command =>
            Proc : Menu_Procedure;
         when Menu_Submenu =>
            First, Last : Menu_Entry_Acc := null;
      end case;
   end record;

   procedure Help_Proc (Line : String);

   procedure Debug_Loop (Prompt : System.Address);

end Debuggers;
