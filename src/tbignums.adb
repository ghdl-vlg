with GNAT.Table;

package body Tbignums is
   package Ds is new GNAT.Table
     (Table_Component_Type => Digit,
      Table_Index_Type => Bignum_Index,
      Table_Low_Bound => 8,
      Table_Initial => 1024,
      Table_Increment => 100);

   function Ds_Next return Bignum_Index is
   begin
      return Ds.Last + Bignum_Index'(1);
   end Ds_Next;

   function "+" (Index : Bignum_Index; Off : Bignum_Length)
                return Bignum_Index is
   begin
      return Index + Bignum_Index (Off);
   end "+";

   function "-" (L : Bignum_Index; R : Bignum_Index) return Bignum_Length
   is
      pragma Assert (L > R);
   begin
      return Bignum_Length (Natural (L) - Natural (R));
   end "-";

   function Dbn (Idx : Bignum_Index) return Digit is
   begin
      return Ds.Table (Idx);
   end Dbn;

   function Create_Bignum (D : Digit) return Bignum is
   begin
      Ds.Append (D);
      return (Index => Ds.Last, Length => 1);
   end Create_Bignum;

   function Create_Bignum_By_Length (Len : Bignum_Length) return Bignum is
      Res_Index : constant Bignum_Index := Ds_Next;
   begin
      if Ds.Allocate (Natural (Len)) /= Res_Index then
         --  Cannot ignore result of Allocate.
         raise Program_Error;
      end if;
      return (Index => Res_Index, Length => Len);
   end Create_Bignum_By_Length;

   function Is_Top (Bn : Bignum) return Boolean is
   begin
      return Bn.Index + Bn.Length = Ds_Next;
   end Is_Top;

   function Get_Digit (Bn : Bignum; Idx : Bignum_Length) return Digit is
   begin
      pragma Assert (Idx < Bn.Length);
      return Ds.Table (Bn.Index + Idx);
   end Get_Digit;

   procedure Set_Digit (Bn : Bignum; Idx : Bignum_Length; D : Digit) is
   begin
      pragma Assert (Idx < Bn.Length);
      Ds.Table (Bn.Index + Idx) := D;
   end Set_Digit;

   procedure Assign (Dest : Bignum; Src : Bignum) is
   begin
      pragma Assert (Dest.Length = Src.Length);
      for I in 0 .. Src.Length - 1 loop
         Set_Digit (Dest, I, Get_Digit (Src, I));
      end loop;
   end Assign;

   procedure Clear (B : Bignum) is
   begin
      for I in 0 .. B.Length - 1 loop
         Set_Digit (B, I, 0);
      end loop;
   end Clear;

   procedure Set (B : Bignum) is
   begin
      for I in 0 .. B.Length - 1 loop
         Set_Digit (B, I, not 0);
      end loop;
   end Set;

   --  Get the Most Significant Digit of D.
   function MSD (D : Dbl_Digit) return Digit is
   begin
      return Digit (D / Base);
   end MSD;
   pragma Unreferenced (MSD);

   --  Get the Least Significant Digit of D.
   function LSD (D : Dbl_Digit) return Digit is
   begin
      return Digit (D mod Base);
   end LSD;

   --  Prepend D to the MSB of TOP.  No-op if D is 0.
   function Top_Uextend (Top : Bignum; D : Digit) return Bignum is
   begin
      pragma Assert (Is_Top (Top));
      if D /= 0 then
         --  Extend
         Ds.Append (D);
         return (Index => Top.Index, Length => Top.Length + 1);
      else
         return Top;
      end if;
   end Top_Uextend;

   function Top_Uadd (Top : Bignum; D : Digit) return Bignum
   is
      Tmp : Dbl_Digit;
   begin
      pragma Assert (Is_Top (Top));
      Tmp := Dbl_Digit (D);
      for I in 0 .. Top.Length - 1 loop
         Tmp := Tmp + Dbl_Digit (Get_Digit (Top, I));
         Set_Digit (Top, I, LSD (Tmp));
         Tmp := Tmp / Base;
      end loop;

      return Top_Uextend (Top, LSD (Tmp));
   end Top_Uadd;

   function Top_Umul (Top : Bignum; D : Digit) return Bignum
   is
      D_Dbl : constant Dbl_Digit := Dbl_Digit (D);
      Tmp : Dbl_Digit;
   begin
      pragma Assert (Is_Top (Top));
      Tmp := 0;
      for I in 0 .. Top.Length - 1 loop
         Tmp := Tmp + Dbl_Digit (Get_Digit (Top, I)) * D_Dbl;
         Set_Digit (Top, I, LSD (Tmp));
         Tmp := Tmp / Base;
      end loop;

      return Top_Uextend (Top, LSD (Tmp));
   end Top_Umul;

   procedure Top_Udiv (Top : in out Bignum; D : Digit; Remainder : out Digit)
   is
      D_Dbl : constant Dbl_Digit := Dbl_Digit (D);
      Res_Last : Bignum_Length;
      Tmp : Dbl_Digit;
      Div : Dbl_Digit;
   begin
      pragma Assert (Is_Top (Top));
      Tmp := 0;
      Res_Last := Top.Length - 1;
      for I in reverse 0 .. Top.Length - 1 loop
         Tmp := Tmp * Base + Dbl_Digit (Get_Digit (Top, I));
         Div := Tmp / D_Dbl;
         if Div = 0 and then I = Res_Last and then I /= 0 then
            Res_Last := Res_Last - 1;
            Ds.Decrement_Last;
         else
            Set_Digit (Top, I, LSD (Div));
         end if;
         Tmp := Tmp - Div * D_Dbl;
      end loop;

      Remainder := LSD (Tmp);
      Top := (Top.Index, Res_Last + 1);
   end Top_Udiv;

   procedure Uadd_1 (L : Bignum; S : Bignum)
   is
      Tmp : Dbl_Digit;
   begin
      pragma Assert (L.Length >= S.Length);
      Tmp := 0;
      for I in 0 .. S.Length - 1 loop
         Tmp := Tmp + Dbl_Digit (Get_Digit (L, I))
             + Dbl_Digit (Get_Digit (S, I));
         Ds.Append (LSD (Tmp));
         Tmp := Tmp / Base;
      end loop;
      for I in S.Length .. L.Length - 1 loop
         Tmp := Tmp + Dbl_Digit (Get_Digit (L, I));
         Ds.Append (LSD (Tmp));
         Tmp := Tmp / Base;
      end loop;
      if Tmp /= 0 then
         Ds.Append (LSD (Tmp));
      end if;
   end Uadd_1;

   function Uadd (L : Bignum; R : Bignum) return Bignum
   is
      Res_Index : constant Bignum_Index := Ds_Next;
   begin

      if L.Length >= R.Length then
         Uadd_1 (L, R);
      else
         Uadd_1 (R, L);
      end if;
      return (Res_Index, Ds_Next - Res_Index);
   end Uadd;

   function Umul (L : Bignum; R : Bignum) return Bignum
   is
      Res_Index : constant Bignum_Index := Ds_Next;
      Res_Length : constant Bignum_Length := L.Length + R.Length;
      Tmp : Dbl_Digit;
      Off : Bignum_Length;
   begin
      if Ds.Allocate (Natural (Res_Length)) /= Res_Index then
         --  Cannot ignore result of Allocate.
         raise Program_Error;
      end if;

      for I in 0 .. Res_Length - 1 loop
         Ds.Table (Res_Index + I) := 0;
      end loop;

      for I in 0 .. L.Length - 1 loop
         for J in 0 .. R.Length - 1 loop
            Off := I + J;
            Tmp := Dbl_Digit (Ds.Table (Res_Index + Off))
              + Dbl_Digit (Get_Digit (L, I)) * Dbl_Digit (Get_Digit (R, J));
            Ds.Table (Res_Index + Off) := LSD (Tmp);

            Tmp := Tmp / Base;
            --  Propagate carry.
            while Tmp /= 0 loop
               Off := Off + 1;
               pragma Assert (Off < Res_Length);
               Tmp := Tmp + Dbl_Digit (Ds.Table (Res_Index + Off));
               Ds.Table (Res_Index + Off) := LSD (Tmp);
               Tmp := Tmp / Base;
            end loop;
         end loop;
      end loop;

      if Ds.Table (Res_Index + Res_Length - 1) = 0 then
         return (Res_Index, Res_Length - 1);
      else
         return (Res_Index, Res_Length);
      end if;
   end Umul;

   function Is_Zero (Bn : Bignum) return Boolean is
   begin
      for I in 0 .. Bn.Length - 1 loop
         if Get_Digit (Bn, I) /= 0 then
            return False;
         end if;
      end loop;
      return True;
   end Is_Zero;

   function In_Uns32 (Bn : Bignum) return Boolean is
   begin
      for I in 1 .. Bn.Length - 1 loop
         if Get_Digit (Bn, I) /= 0 then
            return False;
         end if;
      end loop;
      return True;
   end In_Uns32;

   function To_Uns32 (Bn : Bignum) return Uns32 is
   begin
      for I in 1 .. Bn.Length - 1 loop
         if Get_Digit (Bn, I) /= 0 then
            raise Constraint_Error;
         end if;
      end loop;
      return Get_Digit (Bn, 0);
   end To_Uns32;

   --  Return the (minimal) number of words for a bignum of SIZE bits.
   function Size_To_Length (Size : Uns32) return Bignum_Length is
   begin
      return Bignum_Length ((Size + Base_Width - 1) / Base_Width);
   end Size_To_Length;

   function Get_Zext_Digit (Bn : Bignum; Idx : Bignum_Length; Sz : Uns32)
                           return Digit
   is
      Res : constant Digit := Get_Digit (Bn, Idx);
      Len : constant Bignum_Length := Size_To_Length (Sz);
      Nbr_Bits : Natural;
   begin
      if Idx < Len - 1 then
         --  Not need to mask bits.
         return Res;
      else
         Nbr_Bits := Natural (Sz mod Base_Width);
         return Res and (Shift_Left (1, Nbr_Bits) - 1);
      end if;
   end Get_Zext_Digit;

   function Trunc (B : Bignum; Len : Bignum_Length) return Bignum
   is
      Res : Bignum;
   begin
      Res := Create_Bignum_By_Length (Len);

      for I in 0 .. Len - 1 loop
         Set_Digit (Res, I, Get_Digit (B, I));
      end loop;
      return Res;
   end Trunc;

   function Extend (B : Bignum; Size : Uns32; Signed : Boolean) return Bignum
   is
      Res_Length : constant Bignum_Length := Size_To_Length (Size);
      Res : Bignum;
      Nbr_Bits : constant Natural := Natural (Size mod Base_Width);
      D : Digit;
   begin
      --  B is extended (not truncated).
      pragma Assert (B.Length <= Res_Length);

      if Nbr_Bits /= 0 then
         Res := Create_Bignum_By_Length (Res_Length);

         --  Copy common part.
         for I in 0 .. B.Length - 2 loop
            Set_Digit (Res, I, Get_Digit (B, I));
         end loop;

         --  Bit extend last digit.
         D := Get_Digit (B, B.Length - 1);
         if Signed then
            D := Shift_Right_Arithmetic (Shift_Left (1, 32 - Nbr_Bits),
                                         Nbr_Bits);
         else
            D := D and (Shift_Left (1, Nbr_Bits) - 1);
         end if;
         Set_Digit (Res, B.Length - 1, D);

         if Res_Length = B.Length then
            return Res;
         end if;
      else
         if Res_Length = B.Length then
            --  No change.
            return B;
         end if;

         Res := Create_Bignum_By_Length (Res_Length);
         for I in 0 .. B.Length - 1 loop
            Set_Digit (Res, I, Get_Digit (B, I));
         end loop;
         D := Get_Digit (B, B.Length - 1);
      end if;

      --  Extend digits.
      if Signed then
         D := Shift_Right_Arithmetic (D, 31);
      else
         D := 0;
      end if;

      for I in B.Length .. Res_Length - 1 loop
         Set_Digit (Res, I, D);
      end loop;

      return Res;
   end Extend;

   function Zext (B : Bignum; Size : Uns32) return Bignum is
   begin
      return Extend (B, Size, False);
   end Zext;

   function Sext (B : Bignum; Size : Uns32) return Bignum is
   begin
      return Extend (B, Size, True);
   end Sext;

   function Shr (B : Bignum; Count : Uns32) return Bignum
   is
      Res : Bignum;
      Nbr_Digits : constant Bignum_Length :=
        Bignum_Length (Count / Base_Width);
      Cnt : Bignum_Length;
      Nbr_Bits : constant Natural := Natural (Count mod Base_Width);
      D0, D1 : Digit;
      D : Digit;
   begin
      Res := Create_Bignum_By_Length (B.Length);

      if Nbr_Digits >= B.Length then
         --  Very large count, result is 0.
         Clear (Res);
      elsif Nbr_Bits = 0 then
         --  No bit shifts, only words.
         Cnt := B.Length - Nbr_Digits;
         for I in 0 .. Cnt - 1 loop
            Set_Digit (Res, I, Get_Digit (B, Nbr_Digits + I));
         end loop;
         for I in Cnt .. B.Length - 1 loop
            Set_Digit (Res, I, 0);
         end loop;
      else
         --  General case.
         Cnt := B.Length - (Nbr_Digits + 1);
         D0 := Get_Digit (B, Nbr_Digits);  --  Note: Nbr_Digits < B.Length.
         for I in 0 .. Cnt - 1 loop
            D1 := Get_Digit (B, Nbr_Digits + I + 1);
            --  Note: Nbr_Bits /= 0.
            D := Shift_Right (D0, Nbr_Bits) or Shift_Left (D1, 32 - Nbr_Bits);
            Set_Digit (Res, I, D);
            D0 := D1;
         end loop;
         Set_Digit (Res, Cnt, Shift_Right (D0, Nbr_Bits));
         for I in Cnt + 1 .. B.Length - 1 loop
            Set_Digit (Res, I, 0);
         end loop;
      end if;

      return Res;
   end Shr;

   function Bit_Index (B : Bignum; Idx : Uns32) return Uns32 is
   begin
      return Shift_Right (Get_Digit (B, Bignum_Length (Idx / Base_Width)),
                          Natural (Idx mod Base_Width)) and 1;
   end Bit_Index;

   function Bit_Not (B : Bignum) return Bignum
   is
      Res : Bignum;
   begin
      Res := Create_Bignum_By_Length (B.Length);

      for I in 0 .. B.Length - 1 loop
         Set_Digit (Res, I, not Get_Digit (B, I));
      end loop;

      return Res;
   end Bit_Not;

   function Ucomp (L, R : Bignum; Size : Uns32) return Order_Type
   is
      Sz : Uns32;
      Mask : Uns32;
      P : Bignum_Length;
      Rd, Ld : Digit;
   begin
      pragma Assert (L.Length = R.Length);
      pragma Assert (Size_To_Length (Size) = L.Length);

      Sz := Size mod Base_Width;
      P := L.Length - 1;
      Ld := Get_Digit (L, P);
      Rd := Get_Digit (R, P);

      if Sz /= 0 then
         Mask := Shift_Left (1, Natural (Sz) - 1);
         Ld := Rd and Mask;
         Rd := Ld and Mask;
      end if;

      loop
         if Ld /= Rd then
            if Ld < Rd then
               return Less;
            else
               return Greater;
            end if;
         end if;
         exit when P = 0;
         P := P - 1;
         Ld := Get_Digit (L, P);
         Rd := Get_Digit (R, P);
      end loop;
      return Equal;
   end Ucomp;

   function Is_Eq (L, R : Bignum) return Boolean is
   begin
      pragma Assert (L.Length = R.Length);
      for I in 0 .. L.Length - 1 loop
         if Get_Digit (L, I) /= Get_Digit (R, I) then
            return False;
         end if;
      end loop;
      return True;
   end Is_Eq;

   function Is_Eq (L, R : Bignum; Size : Uns32) return Boolean
   is
      Sz : Uns32;
      Mask : Uns32;
      Last_Digit : constant Bignum_Length := L.Length - 1;
   begin
      pragma Assert (L.Length = R.Length);
      pragma Assert (Size_To_Length (Size) = L.Length);
      for I in 0 .. Last_Digit - 1 loop
         if Get_Digit (L, I) /= Get_Digit (R, I) then
            return False;
         end if;
      end loop;
      Sz := Size mod Base_Width;
      if Sz = 0 then
         return Get_Digit (L, Last_Digit) = Get_Digit (R, Last_Digit);
      else
         Mask := Shift_Left (1, Natural (Sz)) - 1;
         return (Get_Digit (L, Last_Digit) and Mask)
           = (Get_Digit (R, Last_Digit) and Mask);
      end if;
   end Is_Eq;
end Tbignums;
