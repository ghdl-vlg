with Interfaces; use Interfaces;
with Types; use Types;

package Tbignums is
   --  Digit base.
   Base_Width : constant := 32;
   Base : constant := 2**Base_Width;

   --  Single digit.
   subtype Digit is Uns32;

   --  Bignums are stored in a table.  The index is the table index of the
   --  least significant digit, and the length is the number of digits.
   --
   --  Because of this storage, it is not possible to free an arbitrary bignum.
   --  Only a mark and release memory management is possible: freeing bignum B
   --  also free later created bignums.  This is usually not an issue for a
   --  compiler.
   --
   --  The last created bignum (the top) is special as it is possible to resize
   --  it without copying it.

   type Bignum_Index is private;
   No_Bignum : constant Bignum_Index;
   type Bignum_Length is new Natural;

   type Bignum is record
      Index : Bignum_Index;
      Length : Bignum_Length;
   end record;

   --  Create a new simple containing only one digit D.
   function Create_Bignum (D : Digit) return Bignum;
   function Create_Bignum_By_Length (Len : Bignum_Length) return Bignum;

   --  Return True if BN is at the top (and therefore can be resized quickly).
   function Is_Top (Bn : Bignum) return Boolean;

   --  Get a BN digit.
   function Get_Digit (Bn : Bignum; Idx : Bignum_Length) return Digit;
   procedure Set_Digit (Bn : Bignum; Idx : Bignum_Length; D : Digit);

   --  Like Get_Digit but bits beyond SZ are cleared.
   function Get_Zext_Digit (Bn : Bignum; Idx : Bignum_Length; Sz : Uns32)
                           return Digit;

   --  DEST and SRC must have the same length.
   procedure Assign (Dest : Bignum; Src : Bignum);

   --  Clear all bits to 0.
   procedure Clear (B : Bignum);

   --  Set all bits to 1.
   procedure Set (B : Bignum);

   --  Return true iff BN is 0.
   function Is_Zero (Bn : Bignum) return Boolean;

   --  Return True iff BN fits in an Uns32 value.
   function In_Uns32 (Bn : Bignum) return Boolean;

   --  Convert BN to an uns32.  May raise Constraint_Error in case of overflow.
   function To_Uns32 (Bn : Bignum) return Uns32;

   --  Nomenclature.
   --
   --  Prefix Top_: the left argument is the Top bignum, and the result is
   --   also the Top bignum (therefore modified).
   --  Prefix u, s: unsigned, signed.  2-complement representation.

   function Top_Uadd (Top : Bignum; D : Digit) return Bignum;
   function Top_Umul (Top : Bignum; D : Digit) return Bignum;
   procedure Top_Udiv (Top : in out Bignum; D : Digit; Remainder : out Digit);

   function Uadd (L : Bignum; R : Bignum) return Bignum;
   function Umul (L : Bignum; R : Bignum) return Bignum;

   --  Unsigned comparaison.
   function Ucomp (L, R : Bignum; Size : Uns32) return Order_Type;

   --  Logical right shift of B by COUNT bits.  Inserted bits are 0.
   function Shr (B : Bignum; Count : Uns32) return Bignum;

   --  Select bit IDX of B.  LSB is 0, the result is 0 or 1.
   function Bit_Index (B : Bignum; Idx : Uns32) return Uns32;

   --  Consider B as an unsigned bit-vector of length SIZE, and normalize it
   --  by zero-extending the MSB.  Length of result is length of B, and the
   --  length of B must be minimal:
   --    Len * Base_Width >= SIZE > (Len - 1) * Base_Width
   function Zext (B : Bignum; Size : Uns32) return Bignum;
   function Sext (B : Bignum; Size : Uns32) return Bignum;

   --  Truncate B to LEN digits.
   function Trunc (B : Bignum; Len : Bignum_Length) return Bignum;

   --  Compute ~B
   function Bit_Not (B : Bignum) return Bignum;

   --  Return True iff L and R are equal.  They must have the same length.
   function Is_Eq (L, R : Bignum) return Boolean;

   --  Likewise, but compare only the SIZE bits.
   function Is_Eq (L, R : Bignum; Size : Uns32) return Boolean;

   --  TODO: extension, truncation, normalize
   --  TODO: sub, div, exp
   --  TODO: signed
   --  TODO: logical op, reduction.
private
   type Bignum_Index is new Natural;
   No_Bignum : constant Bignum_Index := 0;
   subtype Dbl_Digit is Interfaces.Unsigned_64;

   --  Debug routine: get digit at IDX.
   function Dbn (Idx : Bignum_Index) return Digit;
end Tbignums;
