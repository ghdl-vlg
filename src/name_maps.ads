with Types; use Types;

generic
   type T is private;
   No_Element : T;
package Name_Maps is
   type Map_Type is limited private;

   --  Initialize the map.  It is empty after this call.
   procedure Init (Map : out Map_Type);

   --  Get the element from MAP for NAME.  Returns No_Element if NAME is
   --  not present.
   function Get_Element (Map : Map_Type; Name : Name_Id) return T;

   --  Associate VAL to NAME in MAP.  Overwrite the existing association if
   --  NAME was already associated.  VAL cannot be No_Element.
   procedure Set_Element (Map : in out Map_Type; Name : Name_Id; Val : T);
private
   type Element is record
      Name : Name_Id;
      User : T;
   end record;

   type Element_Array is array (Uns32 range <>) of Element;
   type Element_Arr_Acc is access Element_Array;

   type Map_Type is record
      Els : Element_Arr_Acc;
      Count : Uns32;
   end record;
end Name_Maps;
