package Verilog.Disp_Preproc is
   --  Assume Set_File has been called, display preprocessed file.
   procedure Disp_Preprocessor;

   --  Display one token per line.
   procedure Disp_Tokens;
end Verilog.Disp_Preproc;
