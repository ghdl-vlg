with Types; use Types;
with Ortho_Nodes; use Ortho_Nodes;
with Ortho_Ident; use Ortho_Ident;

package Trans_Utils is
--  ALLOCATION_KIND defines the type of memory storage.
   --  ALLOC_STACK means the object is allocated on the local stack and
   --    deallocated at the end of the function.
   --  ALLOC_SYSTEM for object created during design elaboration and whose
   --    life is infinite.
   --  ALLOC_RETURN for unconstrained object returns by function.
   --  ALLOC_HEAP for object created by new.
   type Allocation_Kind is
     (Alloc_Stack, Alloc_Return, Alloc_Heap, Alloc_System);

   --  There are three data storage kind: global, local or instance.
   --  For example, a constant can have:
   --  * a global storage when declared inside a package.  This storage
   --    can be accessed from any point.
   --  * a local storage when declared in a subprogram.  This storage
   --    can be accessed from the subprogram, is created when the subprogram
   --    is called and destroy when the subprogram exit.
   --  * an instance storage when declared inside a process.  This storage
   --    can be accessed from the process via an instance pointer, is
   --    created during elaboration.
   --procedure Push_Global_Factory (Storage : O_Storage);
   --procedure Pop_Global_Factory;
   procedure Set_Global_Storage (Storage : O_Storage);

   --  Set the global scope handling.
   Global_Storage : O_Storage;

   --  Start to build an instance.
   --  If INSTANCE_TYPE is not O_TNODE_NULL, it must be an uncompleted
   --  record type, that will be completed.
   procedure Push_Instance_Factory (Instance_Type : O_Tnode);
   --  Manually add a field to the current instance being built.
   function Add_Instance_Factory_Field (Name : O_Ident; Ftype : O_Tnode)
                                       return O_Fnode;
   --  Finish the building of the current instance and return the type
   --  built.
   procedure Pop_Instance_Factory (Instance_Type : out O_Tnode);

   --  Create a new scope, in which variable are created locally
   --  (ie, on the stack).  Always created unlocked.
   procedure Push_Local_Factory;

   --  Destroy a local scope.
   procedure Pop_Local_Factory;

   --  Push_scope defines how to access to a variable stored in an instance.
   --  Variables defined in SCOPE_TYPE can be accessed via field SCOPE_FIELD
   --  in scope SCOPE_PARENT.
   procedure Push_Scope (Scope_Type : O_Tnode;
                         Scope_Field : O_Fnode; Scope_Parent : O_Tnode);
   --  Variables defined in SCOPE_TYPE can be accessed by dereferencing
   --  fiel SCOPE_FIELD defined in SCOPE_PARENT.
   procedure Push_Scope_Via_Field_Ptr
     (Scope_Type : O_Tnode;
      Scope_Field : O_Fnode; Scope_Parent : O_Tnode);
   --  Variables/scopes defined in SCOPE_TYPE can be accessed via
   --  dereference of parameter SCOPE_PARAM.
   procedure Push_Scope (Scope_Type : O_Tnode; Scope_Param : O_Dnode);
   --  No more accesses to SCOPE_TYPE are allowed.
   --  Scopes must be poped in the reverse order they are pushed.
   procedure Pop_Scope (Scope_Type : O_Tnode);

   --  Same as Push_Scope/Pop_Scope, but act only if SCOPE_TYPE is not
   --  null.
   procedure Push_Scope_Soft (Scope_Type : O_Tnode; Scope_Param : O_Dnode);
   procedure Pop_Scope_Soft (Scope_Type : O_Tnode);
   pragma Inline (Push_Scope_Soft);
   pragma Inline (Pop_Scope_Soft);

   --  Reset the identifier.
   type Id_Mark_Type is limited private;
   type Local_Identifier_Type is limited private;

   --  Current length.
   Identifier_Len : Natural := 0;

   --  Low level subprograms.
   procedure Save_Mark (Mark : out Id_Mark_Type);
   pragma Inline (Save_Mark);

   procedure Append_Identifier_Prefix (C : Character);
   procedure Append_Identifier_Prefix (S : String);
   procedure Append_Identifier_Prefix (N : Natural);
   procedure Append_Identifier_Prefix_Uniq;

   procedure Reset_Identifier_Prefix;
   procedure Push_Identifier_Prefix (Mark : out Id_Mark_Type;
                                     Name : String;
                                     Val : Iir_Int32 := 0);
   procedure Push_Identifier_Prefix (Mark : out Id_Mark_Type;
                                     Name : Name_Id;
                                     Val : Iir_Int32 := 0);
   procedure Push_Identifier_Prefix_Uniq (Mark : out Id_Mark_Type);
   procedure Pop_Identifier_Prefix (Mark : in Id_Mark_Type);

   --  Save/restore the local identifier number; this is used by package
   --  body, which has the same prefix as the package declaration, so it
   --  must continue local identifiers numbers.
   --  This is used by subprogram bodies too.
   procedure Save_Local_Identifier (Id : out Local_Identifier_Type);
   procedure Restore_Local_Identifier (Id : Local_Identifier_Type);

   --  Create an identifier from IIR node ID without the prefix.
--   function Create_Identifier_Without_Prefix (Id : Iir)
--                                             return O_Ident;
   function Create_Identifier_Without_Prefix (Id : Name_Id; Str : String)
                                             return O_Ident;

   --  Create an identifier from the current prefix.
   function Create_Identifier return O_Ident;

   --  Create an identifier from IIR node ID with prefix.
--   function Create_Identifier (Id : Iir; Str : String := "")
--                              return O_Ident;
--   function Create_Identifier
--     (Id : Iir; Val : Iir_Int32; Str : String := "")
--     return O_Ident;
   function Create_Identifier (Id : Name_Id; Str : String := "")
                              return O_Ident;
   --  Create a prefixed identifier from a string.
   function Create_Identifier (Str : String) return O_Ident;

   --  Create an identifier for a variable.
   --  IE, if the variable is global, prepend the prefix,
   --   if the variable belong to an instance, no prefix is added.
   type Var_Ident_Type is private;

   function Get_Identifier_Mark return Natural;

   function Create_Var_Identifier (Mark : Natural) return O_Ident;
--   function Create_Var_Identifier (Id : Iir)
--                                  return Var_Ident_Type;
   function Create_Var_Identifier (Id : String) return O_Ident;
--   function Create_Uniq_Identifier return Var_Ident_Type;

   type Var_Type (<>) is limited private;
   type Var_Acc is access Var_Type;

   --  Create a variable in the current scope.
   --  If the current scope is the global scope, then a variable is
   --   created at the top level (using decl_global_storage).
   --  If the current scope is not the global scope, then a field is added
   --   to the current scope.
   function Create_Var
     (Name : O_Ident;
      Vtype : O_Tnode;
      Storage : O_Storage := Global_Storage)
     return Var_Acc;

   --  Create a global variable.
   function Create_Global_Var
     (Name : O_Ident; Vtype : O_Tnode; Storage : O_Storage)
     return Var_Acc;

   --  Create a global constant and initialize it to INITIAL_VALUE.
   function Create_Global_Const
     (Name : O_Ident;
      Vtype : O_Tnode;
      Storage : O_Storage;
      Initial_Value : O_Cnode)
     return Var_Acc;
   procedure Define_Global_Const (Const : Var_Acc; Val : O_Cnode);

   --  Return the (real) reference to a variable created by Create_Var.
   function Get_Var (Var : Var_Acc) return O_Lnode;
   --function Get_Var (Var : Var_Acc) return O_Dnode;

   --  Return a reference to the instance of type ITYPE.
   function Get_Instance_Ref (Itype : O_Tnode) return O_Lnode;

   --  Return the address of the instance for block BLOCK.
--   function Get_Instance_Access (Block : Iir) return O_Enode;

   --  Return the storage for the variable VAR.
   function Get_Alloc_Kind_For_Var (Var : Var_Acc) return Allocation_Kind;

   --  Return TRUE iff VAR is stable, ie get_var (VAR) can be referenced
   --  several times.
   function Is_Var_Stable (Var : Var_Acc) return Boolean;

   --  Used only to generate RTI.
   function Is_Var_Field (Var : Var_Acc) return Boolean;
   function Get_Var_Field (Var : Var_Acc) return O_Fnode;
   function Get_Var_Label (Var : Var_Acc) return O_Dnode;
private
   type Local_Identifier_Type is new Natural;
   type Id_Mark_Type is record
      Len : Natural;
      Local_Id : Local_Identifier_Type;
   end record;

   type Var_Ident_Type is record
      Id : O_Ident;
   end record;

   --  Kind of variable:
   --  VAR_GLOBAL: the variable is a global variable (static or not).
   --  VAR_LOCAL: the variable is on the stack.
   --  VAR_SCOPE: the variable is in the instance record.
   type Var_Kind is (Var_Global, Var_Scope, Var_Local);

   --  An instance contains all the data (variable, signals, constant...)
   --  which are declared by an entity and an architecture.
   --  (An architecture inherits the data of its entity).
   --
   --  The processes and implicit guard signals of an entity/architecture
   --  are translated into functions.  The first argument of these functions
   --  is a pointer to the instance.

   type Inst_Build_Kind_Type is (Local, Global, Instance);
   type Inst_Build_Type (Kind : Inst_Build_Kind_Type);
   type Inst_Build_Acc is access Inst_Build_Type;
   type Inst_Build_Type (Kind : Inst_Build_Kind_Type) is record
      Prev : Inst_Build_Acc;
      Prev_Id_Start : Natural;
      case Kind is
         when Local =>
            --  Previous global storage.
            Prev_Global_Storage : O_Storage;
         when Global =>
            null;
         when Instance =>
            Elements : O_Element_List;
            Vars : Var_Acc;
      end case;
   end record;

   type Var_Type (Kind : Var_Kind) is record
      case Kind is
         when Var_Global
           | Var_Local =>
            E : O_Dnode;
         when Var_Scope =>
            I_Field : O_Fnode;
            I_Type : O_Tnode;
            I_Link : Var_Acc;
      end case;
   end record;
end Trans_Utils;
