with Verilog.Nodes; use Verilog.Nodes;

package Verilog.Disp_Tree is
   function Image_Binary_Ops (Op : Binary_Ops) return String;

   procedure Disp_Tree
     (N : Node; Indent : Natural := 0; Depth : Natural := 20);
end Verilog.Disp_Tree;
