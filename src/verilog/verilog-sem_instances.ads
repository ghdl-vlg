with Verilog.Nodes; use Verilog.Nodes;

package Verilog.Sem_Instances is
   --  Clone module in instances of CHAIN nodes.
   procedure Instantiate_Design (Chain : Node);

   function Instantiate_Parameters (Params : Node) return Node;

   --  Finish the instantiation of KLASS.  Only parameters are set.
   procedure Instantiate_Class (Klass : Node; Gen_Class : Node);

   function Instantiate_Generate_Block
     (Items : Node; Old_Parent : Node; New_Parent : Node) return Node;
end Verilog.Sem_Instances;
