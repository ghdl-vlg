/*
 *  Copyright (C) 2008-2014  Cary R. (cygcary@yahoo.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <assert.h>
#include <math.h>
#include <string.h>
#include "vpi_user.h"
#include "sys_priv.h"
#include "sv_vpi_user.h"

/* GHDL: hacked from vvp/vpi_priv.cc  */

/*
 * This routine calculated the return value for $clog2.
 * It is easier to do it here vs trying to to use the VPI interface.
 */
s_vpi_vecval vpip_calc_clog2(vpiHandle arg)
{
      s_vpi_vecval rtn;
      s_vpi_value val;
      PLI_INT32 res = 0;

      val.format = vpiVectorVal;
      vpi_get_value(arg, &val);
      unsigned width = vpi_get(vpiSize, arg);
      unsigned nwords = (width + 31) / 32;
      int i;

      /* Handle X/Z.  */
      for (unsigned i = 0; i < nwords; i++)
	if (val.value.vector[i].bval != 0) {
	  rtn.aval = rtn.bval = 0xFFFFFFFFU;  /* Set to 'bx. */
	  return rtn;
	}

      /* Find the MSB.  */
      for (i = nwords - 1; i >= 0; i--)
	if (val.value.vector[i].aval != 0)
	  break;

      if (i < 0) {
	/*  1800-2017 20.8.1
	    ... and an argument value of 0 shall produce a result of 0.  */
	res = 0;
      }
      else {
	PLI_UINT32 a = val.value.vector[i].aval;
	while (a != 1) {
	  a >>= 1;
	  res++;
	}
	res += i * 32;
	/* Ceiling.  */
	a = val.value.vector[i].aval ^ (1 << res);
	if (a != 0)
	  res++;
	else {
	  for (i--; i >= 0; i--)
	    if (val.value.vector[i].aval != 0) {
	      res++;
	      break;
	    }
	}
      }

      rtn.aval = res;
      rtn.bval = 0;
      return rtn;
}

/*
 * Check that the function is called with the correct argument.
 */
static PLI_INT32 sys_clog2_compiletf(ICARUS_VPI_CONST PLI_BYTE8 *name)
{
      vpiHandle callh = vpi_handle(vpiSysTfCall, 0);
      vpiHandle argv, arg;

      assert(callh != 0);
      argv = vpi_iterate(vpiArgument, callh);
      (void)name;  /* Parameter is not used. */

	/* We must have an argument. */
      if (argv == 0) {
	    vpi_printf("ERROR: %s:%d: ", vpi_get_str(vpiFile, callh),
	               (int)vpi_get(vpiLineNo, callh));
	    vpi_printf("$clog2 requires one numeric argument.\n");
	    vpi_control(vpiFinish, 1);
	    return 0;
      }

	/* The argument must be numeric. */
      arg = vpi_scan(argv);
      if (! is_numeric_obj(arg)) {
	    vpi_printf("ERROR: %s:%d: ", vpi_get_str(vpiFile, callh),
	               (int)vpi_get(vpiLineNo, callh));
	    vpi_printf("The first argument to $clog2 must be numeric.\n");
	    vpi_control(vpiFinish, 1);
      }

	/* We can have a maximum of one argument. */
      if (vpi_scan(argv) != 0) {
	    char msg[64];
	    unsigned argc;

	    snprintf(msg, sizeof(msg), "ERROR: %s:%d:",
	             vpi_get_str(vpiFile, callh),
	             (int)vpi_get(vpiLineNo, callh));
	    msg[sizeof(msg)-1] = 0;

	    argc = 1;
	    while (vpi_scan(argv)) argc += 1;

            vpi_printf("%s $clog2 takes at most one argument.\n", msg);
            vpi_printf("%*s Found %u extra argument%s.\n",
	               (int) strlen(msg), " ", argc, argc == 1 ? "" : "s");
            vpi_control(vpiFinish, 1);
      }

      return 0;
}

static PLI_INT32 sys_clog2_calltf(ICARUS_VPI_CONST PLI_BYTE8 *name)
{
      vpiHandle callh = vpi_handle(vpiSysTfCall, 0);
      vpiHandle argv = vpi_iterate(vpiArgument, callh);
      vpiHandle arg;
      s_vpi_value val;
      s_vpi_vecval vec;
      (void)name;  /* Parameter is not used. */

	/* Get the argument. */
      arg = vpi_scan(argv);
      vpi_free_object(argv);

      vec = vpip_calc_clog2(arg);

      val.format = vpiVectorVal;
      val.value.vector = &vec;
      vpi_put_value(callh, &val, 0, vpiNoDelay);
      return 0;
}

/*
 * Register the function with Verilog.
 */
void sys_clog2_register(void)
{
      s_vpi_systf_data tf_data;
      vpiHandle res;

      tf_data.type        = vpiSysFunc;
      tf_data.sysfunctype = vpiIntFunc;
      tf_data.calltf      = sys_clog2_calltf;
      tf_data.compiletf   = sys_clog2_compiletf;
      tf_data.sizetf      = 0;
      tf_data.tfname      = "$clog2";
      tf_data.user_data   = 0;
      res = vpi_register_systf(&tf_data);
      vpip_make_systf_system_defined(res);
}
