#include <assert.h>
#include "vpi_user.h"

extern unsigned char verilog__simulation__flag_trace;

static PLI_INT32
sys_no_compiletf(ICARUS_VPI_CONST PLI_BYTE8*name)
{
  return 0;
}

static PLI_INT32
sys_showscope_calltf(ICARUS_VPI_CONST PLI_BYTE8 *name)
{
  vpiHandle callh, scope;

  callh = vpi_handle(vpiSysTfCall, 0);
  scope = vpi_handle(vpiScope, callh);
  assert(scope);
  char *cp = vpi_get_str(vpiFullName, scope);
  vpi_printf ("current scope: %s\n", cp);

  return 0;
}

static PLI_INT32
sys_settrace_calltf(ICARUS_VPI_CONST PLI_BYTE8 *name)
{
  verilog__simulation__flag_trace = 1;
  return 0;
}

static PLI_INT32
sys_cleartrace_calltf(ICARUS_VPI_CONST PLI_BYTE8 *name)
{
  verilog__simulation__flag_trace = 0;
  return 0;
}

void sys_debug_register(void)
{
  s_vpi_systf_data tf_data;
  vpiHandle res;

  /* showscope */
  tf_data.type      = vpiSysTask;
  tf_data.tfname    = "$showscope";
  tf_data.calltf    = sys_showscope_calltf;
  tf_data.compiletf = sys_no_compiletf;
  tf_data.sizetf    = 0;
  tf_data.user_data = NULL;
  res = vpi_register_systf(&tf_data);
  vpi_free_object(res);


  /* settrace */
  tf_data.type      = vpiSysTask;
  tf_data.tfname    = "$settrace";
  tf_data.calltf    = sys_settrace_calltf;
  tf_data.compiletf = sys_no_compiletf;
  tf_data.sizetf    = 0;
  tf_data.user_data = NULL;
  res = vpi_register_systf(&tf_data);
  vpi_free_object(res);

  /* cleartrace */
  tf_data.type      = vpiSysTask;
  tf_data.tfname    = "$cleartrace";
  tf_data.calltf    = sys_cleartrace_calltf;
  tf_data.compiletf = sys_no_compiletf;
  tf_data.sizetf    = 0;
  tf_data.user_data = NULL;
  res = vpi_register_systf(&tf_data);
  vpi_free_object(res);
}
