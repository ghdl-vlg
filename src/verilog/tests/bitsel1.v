module bitsel1;
   reg [3:0] r;
   reg 	     v;
   reg [1:0] idx;
   
   initial begin
      r = 'hd;
      v = r[1];
      
      if (v != 0) begin
	 $display ("FAIL");
	 $finish;
      end
      
      if (r[3] != 1) begin
	 $display ("FAIL");
	 $finish;
      end
      $display ("PASS");
   end
endmodule
