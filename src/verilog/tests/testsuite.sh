#! /bin/sh

TESTS="if1.v if2.v if3.v if4.v add1.v add2.v display1.v finish1.v simple1.v
 simple2.v wire2.v timectl1.v wire3.v nba1.v clk1.v
 drive1.v
 sense1.v
 op_or_red1.v
 for1.v for2.v
 bitsel1.v bitsel2.v bitsel3.v
 concat1.v concat2.v concat3.v concat4.v
 shift1.v shift2.v shift3.v
 assign1.v assign2.v
 hierarchy1.v hierarchy2.v
 conn1.v
 cond1.v cond2.v cond3.v
 inst1.v inst2.v inst3.v inst4.v
 interface1a.v interface1b.v
 counter1.v gate1.v stime1.v
 macro_late1.v
 macro_nested1.v
 macro_empty1.v macro_empty2.v
 num1.v num2.v
 portdecl1.v portdecl3.v portdecl4.v
 packed1.v packed2.v packed3.v packed4.v
 memory1.v memory2.v
 enum1.v
 array1.v array2.v
 struct1.v struct2.v
 forward_typedef1.v
 func1.v func2.v func3.v func4.v func5.v func6.v func7.v func8.v func9.v
 writemem1.v readmem1.v"

ERR_TESTS="
err_param1.v err_param3.v err_param4.v
err_endmodule1.v err_undef1.v
err_systask1.v
err_noident1.v
"

LIST_TESTS="
ports_mh0 ports_mh1 ports_mh2 ports_mh3   ports_mh5 ports_mh6 ports_mh7
ports_mh8 ports_mh9 ports_mh10   ports_mh14
"

OUTPUT_TESTS="arbiter fsm_full_tb display2"

num=0

for f in $LIST_TESTS; do
    echo "testing $f"
    if ! ../vlg -lp --no-elab --list-implicit-type --list-port-omitted ${f}.v | cmp - ${f}-ref.v;
    then
	echo "$f: failed"
	exit 1
    fi
    num=$(($num + 1))
done

for f in `find ??_*/ -name '*.v'`; do
    echo "testing $f"
    flags=""
    line1=$(head -1 $f)
    status="--"
    case "$line1" in
        "// NA") continue;;           # Not applicable
        "// CO") flags="--no-elab";;  # CO compile only
	"// NE") status="NE";;        # No error
	"// RB") status="RB";;        # No error, compare with baseline
	"// CE") status="CE"; flags="--expect-failure";; # Compile error
	*) echo "incorrect status line for $f"; exit 2 ;;
    esac
    if ! ../vlg $flags $f > test.out; then
	echo "$f: failed";
	exit 1
    else
	if [ "$status" == "NE" ]; then
	    if ! grep -q PASS test.out; then
		echo "$f: failed"
		exit 1
	    fi
	elif [ "$status" == "RB" ]; then
	    baseline=`echo "$f" | sed -e 's/\.v/.ref/'`
	    if ! cmp test.out $baseline; then
		echo "$f: failed"
		exit 1
	    fi
	fi
    fi
    num=$(($num + 1))
done

for f in $TESTS; do
    echo "testing $f"
    output=`../vlg $f`
    if ! echo $output | grep -q PASS; then
	echo "$f: failed"
	exit 1
    fi
    num=$(($num + 1))
done

for f in $OUTPUT_TESTS; do
    echo "testing ${f}.v"
    if ! ../vlg ${f}.v | cmp ${f}.ref ; then
	echo "$f: failed"
	exit 1
    fi
    num=$(($num + 1))
done

for f in $ERR_TESTS; do
    echo "testing failing $f"
    if ! ../vlg --expect-failure $f; then
	echo "$f: error expected"
	exit 1
    fi
    num=$(($num + 1))
done

echo "$num tests"
echo "Success"
