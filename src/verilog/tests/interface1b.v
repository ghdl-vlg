// Interface definition
interface Bus;
  logic [7:0] Addr;
endinterface

// Using the interface
module TestRAM;
   Bus TheBus();                   // Instance the interface

   test i1(.TheBus(TheBus));
   
endmodule // TestRAM


module test (Bus TheBus);
   initial begin
      TheBus.Addr = 1;
      for (int I=0; I<7; I++)
	TheBus.Addr = TheBus.Addr + 1;
      if (TheBus.Addr !== 8'h08)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule
