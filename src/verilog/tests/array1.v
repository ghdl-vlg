module array1();
   byte a [0:1][0:2] = '{'{0,1,2},'{3{8'h9}}};

   initial begin
      if (a[0][0] != 0)
	begin
	   $display("FAIL 0,0");
	   $finish;
	end
      if (a[0][2] != 2)
	begin
	   $display("FAIL 0,2");
	   $finish;
	end
      if (a[1][1] != 9)
	begin
	   $display("FAIL 1,1");
	   $finish;
	end
      $display("PASS");
   end
endmodule
