// NE
module pad1;
  reg [0:13*8-1] r3 = "hello world\n";

  initial begin
     //$display("[]: %b", r3);
     //$display("[0:7]: %b", r3[0:7]);
     //$display("[8:15]: %b", r3[8:15]);
     //$display("[16:23]: %b", r3[16:23]);

    if (r3[8:15] != 8'h68) begin
      $display("FAIL-1");
      $finish;
    end
    if (r3[0:7] != 8'h00) begin
      $display("FAIL-0");
      $finish;
    end
    $display("PASS");
  end
endmodule
