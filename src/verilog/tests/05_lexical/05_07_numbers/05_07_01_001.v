// NE

//  Padding of numbers
module literal_tb;
   reg [7:0] v;
   reg [63:0] w;

   initial begin
      v = 8'h1;
      if (v !== 8'h01) begin
         $display("FAIL - 1");
         $finish;
      end
      v = 8'hf;
      if (v !== 8'h0f) begin
         $display("FAIL - 2");
         $finish;
      end
      v = 8'bz01;
      if (v !== 8'bzzzzzz01) begin
         $display("FAIL - 3");
         $finish;
      end
      v = 8'bx100;
      if (v !== 8'bxxxxx100) begin
         $display("FAIL - 4");
         $finish;
      end
      w = 48'hffff_ffff;
      if (w !== 48'h0000_ffff_ffff) begin
         $display("FAIL - 5");
         $finish;
      end
      w = 48'h1_ffff_ffff;
      if (w !== 48'h0001_ffff_ffff) begin
         $display("FAIL - 6");
         $finish;
      end
      w = 48'hx_zfff_ffff;
      if (w !== 48'hxxxx_zfff_ffff) begin
         $display("FAIL - 7");
         $finish;
      end
      $display("PASS");
   end // initial begin
endmodule // literal_tb
