// NE

//  Padding.  Exemple 4 of 1800-2017 5.7.1
module literal_tb;
   logic [11:0] a, b, c, d;
   logic [84:0] e, f, g;

   initial begin
      a = 'hx;
      if (a !== 12'hxxx) begin
         $display("FAIL - 1");
         $finish;
      end
      b = 'h3x;
      if (b !== 12'h03x) begin
         $display("FAIL - 2");
         $finish;
      end
      c = 'hz3;
      if (c !== 12'hzz3) begin
         $display("FAIL - 3");
         $finish;
      end
      d = 'h0z3;
      if (d !== 12'h0z3) begin
         $display("FAIL - 4");
         $finish;
      end
      e = 'h5;
      if (e !== 4'b0_101) begin
         $display("FAIL - 5");
         $finish;
      end
      f = 'hx;
      if (f !== {85{1'bx}}) begin
         $display("FAIL - 6");
         $finish;
      end
      g = 'hz;
      if (g !== {85{1'bz}}) begin
         $display("FAIL - 6");
         $finish;
      end
      $display("PASS");
   end // initial begin
endmodule // literal_tb
