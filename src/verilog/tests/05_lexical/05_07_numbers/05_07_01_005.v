// NE

//  Sign extension
module literal_tb;
   logic [15:0] a;

   initial begin
      a = 4'sb1001;
      if (a !== 16'hfff9) begin
         $display("FAIL - 1");
         $finish;
      end
      $display("PASS");
   end // initial begin
endmodule // literal_tb
