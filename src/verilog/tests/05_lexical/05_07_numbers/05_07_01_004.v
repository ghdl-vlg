// NE

//  Padding.  Exemple 5 of 1800-2017 5.7.1
module literal_tb;
   logic [15:0] a, b, c, d;

   initial begin
      a = '0;
      if (a !== 16'h0000) begin
         $display("FAIL - 1");
         $finish;
      end
      b = '1;
      if (b !== 16'hffff) begin
         $display("FAIL - 2");
         $finish;
      end
      c = 'x;
      if (c !== 16'hxxxx) begin
         $display("FAIL - 3");
         $finish;
      end
      d = 'z;
      if (d !== 16'hzzzz) begin
         $display("FAIL - 4");
         $finish;
      end
      $display("PASS");
   end // initial begin
endmodule // literal_tb
