// NE

//  Base
module literal_tb;
   reg [7:0] v;

   initial begin
      v = 8'h7e;
      if (v !== 126) begin
         $display("FAIL - 1");
         $finish;
      end
      if (v !== 8'b0111_1110) begin
         $display("FAIL - 2");
         $finish;
      end
      if (v !== 8'o176) begin
         $display("FAIL - 3");
         $finish;
      end
      $display("PASS");
   end // initial begin
endmodule // literal_tb
