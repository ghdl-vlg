// NE

//  Sign extension - Example 3 of 1800-2017 5.7.1 Integer literal constants
module literal_tb;
   logic [15:0] a;

   initial begin
      if (-8'd6 !== 8'b1111_1010) begin
         $display("FAIL - 1");
         $finish;
      end
      if (4'shf !== -4'h1) begin
         $display("FAIL - 2");
         $finish;
      end
      if (-4'sd15 !== 4'h1) begin
         $display("FAIL - 3");
         $finish;
      end
      $display("PASS");
   end // initial begin
endmodule // literal_tb
