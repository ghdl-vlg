//  From 23.2.2 Port declarations
typedef struct {
    bit isfloat;
    union { int i; shortreal f; } n;
  } tagged_st; // named structure

module mh1 (input var int in1,
            input var shortreal in2,
            output tagged_st out);
endmodule
