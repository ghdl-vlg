module memory2 ();
   reg [7:0] mem[0:3];
   reg [3:0] vec;

   reg [1:0] ptr;
   wire [7:0] data = mem[ptr];
   wire [7:0] data2 = data;

   wire       dv = vec[ptr];
   initial begin
      mem[0] = 20;
      mem[2] = 12;
      vec = 4'hx;
      # 50;
      ptr <= 2;
      # 10;
      mem[ptr] <= 22;
      vec[ptr] <= 1'b1;
      # 1;
      if (data2 !== 22) begin
         $display("FAILED-1");
         $finish;
      end
      if (dv !== 1) begin
         $display("FAILED-2");
         $finish;
      end

      $display("PASSED");

   end
endmodule
