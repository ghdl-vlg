// NE
module t1 #(L=3) (input reg [7:0] i [L]);
   initial begin
      # 1;
      if (i[0] !== 8'h3d)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule

module t;
   reg [7:0] inp [1];

   initial
     inp[0] = 8'h3d;

   t1 #(1) dut (inp);
endmodule
