// NE
module t1 #(W=2) (input wire [W-1:0] i);
   initial begin
      # 1;
      if (i !== 8'h3d)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule

module t;
   wire [7:0] inp = 8'h3d;

   t1 #(8) dut (inp);
endmodule
