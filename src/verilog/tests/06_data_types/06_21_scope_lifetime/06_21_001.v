// NE
// Example from LRM
module msl;
   int st0; // static
   initial begin
      int st1; // static
      static int st2; // static
      automatic int auto1;// automatic
      $display("PASS");
      $finish;
   end

   task automatic t1();
      int auto2;// automatic
      static int st3; // static
      automatic int auto3; // automatic
   endtask
endmodule
