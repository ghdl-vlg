// NE
module t;
   typedef struct packed {
      logic 	  e;
   } my_t;

   my_t a, b, c;
   initial begin
      a.e = 1;
      b = a;
      c = b;
      if (c.e !== 1)
       $fatal(0, "FAIL");

    $display("PASS");
  end
endmodule
