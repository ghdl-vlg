// NE
module t;
   reg [3:0] p;
   reg [7:0] e;

   initial begin
      p = 4'h1;
      e = 8'(p);
      if (e !== 4'h1)
	$fatal(0, "FAIL");
    $display("PASS");
  end
endmodule
