// NE

module m1;
   task my_task(string s);
      string n = {"hello ", s};
      $display(n);
   endtask // my_task
   
   initial  begin
      my_task("world");
      my_task("2");
      my_task("3");
      
      $display("PASS");
   end // initial begin
endmodule // m1
