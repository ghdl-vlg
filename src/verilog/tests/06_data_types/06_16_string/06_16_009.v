// NE
// toupper
module m1;
   string s;
   initial begin
      s = "Hello";
      s = s.toupper();
      if (s != "HELLO")
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
