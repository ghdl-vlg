// NE

module m1;
   task my_task(string s);
      automatic string a = {s, "2"};
      if (a == "hello2")
        $display("hello");
   endtask // my_task
   
   initial  begin
      my_task("world");
      my_task("2");
      my_task("3");
      my_task("hello");
      
      $display("PASS");
   end // initial begin
endmodule // m1
