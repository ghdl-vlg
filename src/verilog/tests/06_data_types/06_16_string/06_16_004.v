// NE

module m1;
   task my_task(string s);
      $display("%s", {"hello ", s});
   endtask // my_task
   
   initial  begin
      my_task("world");
      my_task("2");
      my_task("3");
      
      $display("PASS");
   end // initial begin
endmodule // m1
