// NE

module m1;
   string s;
   initial begin
      byte b;

      s = "hello";
      b = s[0];
      if (b !== "h")
        $fatal(0, "FAIL-1");
      if (s[1] !== 8'h65)
        $fatal(0, "FAIL-2");
      if (s[5] !== 0)
        $fatal(0, "FAIL-2");
      $display("PASS");
   end // initial begin
endmodule // m1
