// NE

module m1;
   initial  begin
      automatic string v = "var";
      if (v != "var")
        $fatal(0, "FAIL-1");
      
      v = "foo";
      if (v != "foo")
        $fatal(0, "FAIL-2");
      
      $display("PASS");
   end // initial begin
endmodule // m1
