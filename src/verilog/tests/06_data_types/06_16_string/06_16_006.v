// NE

module m1;
   function string my_func(string s);
      $display("return var: %s", my_func);
   endfunction

   function automatic string my_func2(string s);
      $display("return var: %s", my_func);
   endfunction
   
   initial  begin
      void'(my_func("world"));
      void'(my_func("1"));

      void'(my_func2("world"));
      void'(my_func2("2"));
      
      $display("PASS");
   end // initial begin
endmodule // m1
