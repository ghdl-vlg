// NE
// From uvm code
package uvm_pkg;

  class uvm_callbacks_base;
  endclass

  class uvm_typed_callbacks extends uvm_callbacks_base ;
     static string m_typename ;
     typedef uvm_callbacks_base super_type ;
  endclass

  class uvm_callbacks extends uvm_typed_callbacks;
     static string m_typename ;
     typedef uvm_typed_callbacks super_type ;

     static function void m_register_pair ( string tname ="");
	m_typename = tname ;
	super_type :: m_typename = tname ;
     endfunction
  endclass
endpackage

module top;
   import uvm_pkg::*;

   initial
     $display("PASS");
endmodule

