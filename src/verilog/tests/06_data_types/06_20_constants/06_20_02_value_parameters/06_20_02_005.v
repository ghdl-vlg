// NE
module sparam5 #([7:0] a = 123, b = 12)(output wire [7:0] res);

   assign res = b;
endmodule

module param5;
   wire [7:0] val;

   sparam5 #(120) inst (val);

   initial begin
      # 1;
      if (val == 12)
       $display ("PASS");
     else
       $display ("FAIL");
   end
endmodule
