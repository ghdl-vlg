// NE
module sparam11 (output wire [7:0] res);
   parameter [0:0] p = 0;

   assign res = p;
endmodule

module param11 #(parameter [0:0] p = 1);
   wire [7:0] val;

   sparam11 #(.p(p)) inst (val);

   initial begin
      # 1;
      if (val == 1)
       $display ("PASS");
     else
       $display ("FAIL");
   end
endmodule
