// NE
module sparam6 #([7:0] a = 123)(output wire [7:0] res);
   parameter b = 14;
   assign res = b;
endmodule

module param6;
   wire [7:0] val;

   sparam6 #(120, 13) inst (val);

   initial begin
      # 1;
      if (val == 13)
       $display ("PASS");
     else
       $display ("FAIL");
   end
endmodule
