// NE
module sparam3(output wire [7:0] res);
   parameter [7:0] a = 123;

   assign res = a;
endmodule

module param3;
   wire [7:0] val;

   sparam3 #(120) inst (val);

   initial begin
      # 1;
      $display("%x", val);
      if (val == 120)
       $display ("PASS");
     else
       $display ("FAIL");
   end
endmodule
