// NE
module sparam4 #([7:0] a = 123)(output wire [7:0] res);

   assign res = a;
endmodule

module param4;
   wire [7:0] val;

   sparam4 #(120) inst (val);

   initial begin
      # 1;
      if (val == 120)
       $display ("PASS");
     else
       $display ("FAIL");
   end
endmodule
