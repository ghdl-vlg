// NE
module sparam9 (output wire [7:0] res);
   parameter b = 14;

   assign res = b;
endmodule

module param9;
   wire [7:0] val;

   parameter b0 = 21;

   sparam9 #(.b(b0)) inst (val);

   initial begin
      # 1;
      if (val == 21)
       $display ("PASS");
     else
       $display ("FAIL");
   end
endmodule
