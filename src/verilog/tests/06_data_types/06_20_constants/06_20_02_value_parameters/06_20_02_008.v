// NE
module sparam8 #([7:0] a = 123)(output wire [7:0] res);
   parameter b = 14;
   parameter signed [5:0] c = 40;

   assign res = c;
endmodule

module param8;
   wire [7:0] val;

   sparam8 #(.c(25), .a(12)) inst (val);

   initial begin
      # 1;
      if (val == 25)
       $display ("PASS");
     else
       $display ("FAIL");
   end
endmodule
