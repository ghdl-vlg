// NE
module sparam10 (output wire [7:0] res);
   localparam b = 14;

   assign res = b;
endmodule

module param10;
   wire [7:0] val;

   sparam10 inst (val);

   initial begin
      # 1;
      if (val == 14)
       $display ("PASS");
     else
       $display ("FAIL");
   end
endmodule
