// CE
module redecl1a (a);
   output reg a;
   reg 	      a;
endmodule

module redecl1;
   wire a;
   redecl1a inst(a);
endmodule // redecl1


