// NE
module multi1a(sum, cout, a, b, cin);
   output [3:0] sum;
   output cout;
   input [3:0] a, b;
   input cin;
   wire [3:1] c;

   assign sum = a + b + cin;
   assign c = a & b;
   assign cout = 0;
endmodule

module multi1;
   wire [3:0] res;
   reg [3:0]  a;
   reg [3:0]  b;
   reg 	      cin;
   wire       cout;

   multi1a dut (res, cout, a, b, cin);

   initial begin
      a <= 3;
      b <= 4;
      # 1;
      if (res != 7)
	$fatal(0, "FAIL-1");

      a <= 4'hc;
      b <= 4'h3;
      # 1;
      if (res != 15)
	$fatal(0, "FAIL-2");
      $display("PASS");
   end
endmodule


