// NE
parameter logic [7:0] My_DataIn = 8'hFF;

module bus_conn ( output logic [7:0] dataout,
		 input [7:0] 	    datain = My_DataIn);
   assign dataout = datain;
   initial begin
      if (datain !== 8'hff)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule

module bus_connect1  ();
   wire logic [31:0] dataout;
   
   bus_conn bconn1 (dataout[23:16]);
   // Omitted port for datain, default parameter value 8'hFF in
   // bus_conn used
endmodule

