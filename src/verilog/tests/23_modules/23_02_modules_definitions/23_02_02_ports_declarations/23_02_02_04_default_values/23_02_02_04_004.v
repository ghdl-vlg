// NE
parameter logic [7:0] My_DataIn = 8'hFF;

module bus_conn ( output logic [7:0] dataout,
		 input [7:0] 	    datain = My_DataIn);
   assign dataout = datain;
   initial begin
      if (datain !== 8'h00)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule

module bus_connect1 ();
   wire logic [31:0] dataout;
   parameter logic [7:0] My_DataIn = 8'h00;

   bus_conn bconn2 (dataout[15:8], My_DataIn);
   // The parameter value 8'h00 from the instantiating scope is used
endmodule

