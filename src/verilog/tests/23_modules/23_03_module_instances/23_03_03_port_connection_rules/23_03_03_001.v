// NE
module sub(input wire v);
  initial begin
    # 1;
    if (v !== 1'b1)
      $fatal(0, "FAIL");
    $display("PASS");
  end
endmodule

module t;
  sub m(.v(1'b1));
endmodule
