// NE
// From example 2
package p;
   function void f();
      $display("p::f");
      $fatal(0, "FAIL");
   endfunction
endpackage

module top;
 import p::*;
   if (1) begin : b // generate block
      initial f();  // reference to “f”
      function void f();
	 $display("top.b.f");
	 $display("PASS");
      endfunction
   end
endmodule
