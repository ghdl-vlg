// NE
module mod3;
   reg [7:0] a = 8'hf3;
   initial begin
      if (dut.f !== 8'h5c)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule

module mod2;
   reg [7:0] f = 8'h5c;
   mod3 m1();
endmodule // mod2

module mod1;
   mod2 dut();
endmodule
