// NE
module a;
   integer i;
   b a_b1();
endmodule

module b;
   integer i;
   c b_c1();
   c b_c2();
   initial
     // downward path references two copies of i:
     // a.a_b1.b_c1.i, d.d_b1.b_c1.i
     #10 b_c1.i = 2;
endmodule

module c;
 integer i;
   initial begin
      // local name references four copies of i:
      // a.a_b1.b_c1.i, a.a_b1.b_c2.i,
      // d.d_b1.b_c1.i, d.d_b1.b_c2.i
      i= 1;
      // upward path references two copies of i:
      // a.a_b1.i, d.d_b1.i
      b.i =1;
   end
endmodule

module d;
   integer i;
   b d_b1();
   initial begin
      # 1;
      if (a.a_b1.b_c1.i !== 1)
	$fatal(0, "FAIL-1");
      if (a.a_b1.b_c2.i !== 1)
	$fatal(0, "FAIL-2");
      if (d.d_b1.b_c1.i !== 1)
	$fatal(0, "FAIL-3");
      if (d.d_b1.b_c2.i !== 1)
	$fatal(0, "FAIL-4");

      // full path name references each copy of i
      a.i = 1;
      a.a_b1.i = 2;
      a.a_b1.b_c1.i = 3;
      a.a_b1.b_c2.i = 4;

      d.i=5;
      d.d_b1.i = 6;
      d.d_b1.b_c1.i = 7;
      d.d_b1.b_c2.i = 8;

      if (d.d_b1.b_c2.i !== 8)
	$fatal(0, "FAIL-5");

      # 10;
      if (a.a_b1.b_c1.i !== 2)
	$fatal(0, "FAIL-6");
      if (a.a_b1.b_c2.i !== 4)
	$fatal(0, "FAIL-7");
      if (d.d_b1.b_c1.i !== 2)
	$fatal(0, "FAIL-8");
      if (d.d_b1.b_c2.i !== 8)
	$fatal(0, "FAIL-9");

      $display("PASS");
   end
endmodule
