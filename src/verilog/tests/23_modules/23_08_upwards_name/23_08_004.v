// NE
module DUT(input clk);
   wire a;
   function void hello;
      $display("Hello from %m");
   endfunction

   printer p(clk);
endmodule

module printer(input clk);
   always @(posedge clk)
      $display("a is %d", DUT.a);  // Can this "DUT" here be replaced with something else?
   initial begin
      DUT.hello;
      # 1;
      $display("PASS");
   end
endmodule

module TB;
   reg clock;
   DUT d1(clock);
   DUT d2(clock);
endmodule
