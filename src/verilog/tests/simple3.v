module simple3;
   reg [3:0] a;
   initial
     begin
	a = 1;
	#2 a = 0;
	#3 a = 1;
	#1 a = 7;
	#1 a = 2;
	$display ("PASS");
     end
endmodule // simple2
