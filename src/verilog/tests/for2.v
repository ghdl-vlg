module for2;
   integer cnt;

   initial begin
      cnt = 0;
      for (int i = 0; i < 10; i++)
	for (int j = 0; j < 5; j++)
	  cnt = cnt + 1;
      if (cnt != 50)
	$display ("FAIL");
      else
	$display ("PASS");
   end
endmodule
