// NE
class c1;
   local int v;

   function int get();
      return v;
   endfunction // get

   task set(int a);
      v = a;
   endtask
endclass // c1

module m1;
   c1 cls[5];
   initial begin
      cls[0] = new;

      begin
	 int i = 0;
	 cls[i].set(i + 1);
	 if (cls[0].get() !== 1)
	   $fatal(0, "FAIL");
	 $display("PASS");
      end
   end
endmodule
