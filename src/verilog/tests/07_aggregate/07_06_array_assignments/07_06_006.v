// NE
module arrasgn;
   var int arr1[3], val[];

   initial begin
      val = new[3];
      if (val.size != 3)
        $fatal(0, "FAIL-1");
      arr1 = val;
      $display("PASS");
   end
endmodule // arrasgn
