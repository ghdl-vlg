// NE
module arrasgn;
   var int arr1[3], arr2[];

   initial begin
      arr1 = '{1, 2, 3};
      arr2 = arr1;
      if (arr2.size != 3)
        $display("FAIL-1");
      if (arr2[1] != 2)
        $display("FAIL-2");
      $display("PASS");
   end
endmodule // arrasgn
