// NE
module dynarr;
   initial
     begin
        int idest[], isrc[3] ='{5, 6, 7};
        if (idest.size() !== 0)
          $fatal(0,"FAIL-1");
        idest = new[3](isrc);
        if (idest.size() !== 3)
          $fatal(0,"FAIL-2");
        if (idest[0] != 5)
          $fatal(0,"FAIL-3");
        if (idest[2] != 7)
          $fatal(0,"FAIL-4");

        $display("PASS");
     end
endmodule // dynarr
