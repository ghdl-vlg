// NE
module dynarr;
   int arr[];
   initial
     begin
        if (arr.size != 0)
          $fatal(0,"FAIL-1");
        arr = new [4];
        if (arr.size != 4)
          $fatal(0,"FAIL-2");

        $display("PASS");
     end
endmodule // dynarr
