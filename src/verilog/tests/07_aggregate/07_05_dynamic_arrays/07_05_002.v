// NE
module dynarr;
   bit [3:0] nibble[];
   initial
     begin
        if (nibble.size != 0)
          $fatal(0,"FAIL");

        $display("PASS");
     end
endmodule // dynarr
