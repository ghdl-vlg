// NE
module dynarr;
   int arr[];
   initial
     begin
        arr = new [4];
        arr[0] = 1;
        if (arr.size() !== 4)
          $fatal(0,"FAIL-1");
        arr.delete;
        if (arr.size() !== 0)
          $fatal(0,"FAIL-2");


        $display("PASS");
     end
endmodule // dynarr
