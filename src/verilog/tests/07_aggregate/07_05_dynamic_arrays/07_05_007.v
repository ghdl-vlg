// NA
module dynarr;
   initial
     begin
        int src[3], dest1[], dest2[];
        src = '{2, 3, 4};
        dest1 = new[2](src);
        dest2 = new[4](src);
        if (dest1 != '{2, 3})
          $fatal(0,"FAIL-1");
        if (dest2 != '{2, 3, 4, 0})
          $fatal(0,"FAIL-2");
        $display("PASS");
     end
endmodule // dynarr
