// NE
module dynarr;
   int arr[];
   initial
     begin
        arr = new [4];
        arr[0] = 1;
        arr[1] = 3;
        if (arr[0] !== 1)
          $fatal(0,"FAIL-1");

        $display("PASS");
     end
endmodule // dynarr
