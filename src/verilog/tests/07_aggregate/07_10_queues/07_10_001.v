// NE
module m;
   initial begin
      integer q1[$];

      q1 = {3, 2, 7};

      if (q1.size !== 3)
        $fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule
