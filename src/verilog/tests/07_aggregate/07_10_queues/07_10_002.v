// NE
module m;
   initial begin
      integer q1[$];

      q1.push_back(3);
      q1.push_back(5);

      if (q1.size !== 2)
        $fatal(0, "FAIL-1");
      if (q1[0] !== 3)
        $fatal(0, "FAIL-2");
      if (q1[1] !== 5)
        $fatal(0, "FAIL-3");
      $display("PASS");
   end
endmodule
