// NE
module tmod;
   struct packed unsigned {
      bit [3:0] a;
      bit [3:0] b;
   } r;

   initial begin
      r = 8'b1100_0011;
      if (r.a !== 4'hc)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule
