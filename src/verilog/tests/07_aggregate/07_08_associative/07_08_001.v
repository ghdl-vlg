// NE
module m1;
   int map[string];

   initial begin
      map["one"] = 1;
      if (map["one"] !== 1)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
