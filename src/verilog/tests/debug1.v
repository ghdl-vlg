module debug1a;
   initial
     begin
        $display("start");
        $stop;
        $display("continue...");
        $stop;
     end
endmodule // debug1a

module debug1;
   debug1a sub();
endmodule // debug1
