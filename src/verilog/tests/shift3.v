module shift3;
   reg signed [15:0] resff;
   reg       res = 1;
   reg 	     clk = 0;
   integer   i;

   initial begin
      if (resff !== 16'hxx) begin
	 $display("FAIL-1");
	 $finish;
      end
      resff = resff>>>2;
      if (resff !== 16'hxx) begin
	 $display("FAIL-2");
	 $finish;
      end

      resff = 16'h8000;
      resff = resff>>>2;
      if (resff !== 16'he000) begin
	 $display("FAIL-3");
	 $finish;
      end

      resff = 16'h4000;
      resff = resff>>>2;
      if (resff !== 16'h1000) begin
	 $display("FAIL-4");
	 $finish;
      end
      $display("PASS");
   end
endmodule
