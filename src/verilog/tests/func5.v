module func5();

   function par;
      input [7:0] val;
      integer 	  i;
      par = 0;
      for (i = 0; i < 8; i = i + 1)
	par = par ^ val[i];
   endfunction

   initial begin
      if (par(7) != 1)
	begin
	   $display("FAIL-1");
	   $finish;
	end
      if (par(.val(240)) != 0)
	begin
	   $display("FAIL-2");
	   $finish;
	end
      $display("PASS");
      $finish;
   end
endmodule
