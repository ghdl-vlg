module cond3;
   reg [7:0] r;
   reg 	      cond;
   wire [8:0] res = { cond ? r[7]:1'b0, r };
   initial begin
      r <= 8'h83;
      cond <= 1'b1;
      # 1;
      if (res != 9'b1_1000_0011)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // cond3

