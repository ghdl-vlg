module func2();
   function [3:0] add(reg [3:0] a);
      automatic reg [3:0] b = 0;
      add = b;
      b = a;
   endfunction // add

   initial begin
      if (add(1) != 0)
	begin
	   $display("FAIL-1");
	   $finish;
	end
      if (add(2) != 0)
	begin
	   $display("FAIL-2");
	   $finish;
	end
      $display("PASS");
      $finish;
   end
endmodule
