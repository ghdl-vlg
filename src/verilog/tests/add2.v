module main;

   reg [3:0] a;
   reg [3:0] result, b;
   reg [4:0] lres;
   
   initial begin
      a = 10;
      b = 15;
      lres = (a + b) >> 1;
      result = (a + b) >> 1;
      
      if (result !== 4) begin
         $display("FAILED: result === %b", result);
         $finish;
      end
      if (lres !== 12) begin
         $display("FAILED: lres === %b", lres);
         $finish;
      end

      $display("PASSED");
   end // initial begin
endmodule // main
