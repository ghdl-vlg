// CE

module t;
   function int myfunc(input int a);
      return a + 1;
   endfunction

   int myfunc;

   initial begin
      void'(myfunc(5));

      $display("PASS");
   end // initial begin
endmodule
