// NE
//  First example

module t;
   function [15:0] myfunc1 (input [7:0] x,y);
      myfunc1 = x * y - 1; // return value assigned to function name
   endfunction

   function [15:0] myfunc2 (input [7:0] x,y);
      return x * y - 1;   //return value is specified using return statement
   endfunction

   initial begin
      reg [7:0] a, b;

      a = myfunc1(7, 5);
      b = myfunc2(8, 9);

      if (a !== 34)
        $fatal(0, "FAIL-1");
      if (b !== 71)
        $fatal(0, "FAIL-2");
      $display("PASS");
   end // initial begin
endmodule
