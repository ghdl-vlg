// CE

module t;
   function int myfunc(input int a);
      int myfunc;
      return a + 1;
   endfunction


   initial begin
      void'(myfunc(5));

      $display("PASS");
   end // initial begin
endmodule
