// NE

module t;
   function int myfunc(input int a);
      return a + 1;
   endfunction

   initial begin
      myfunc(5);

      $display("PASS");
   end // initial begin
endmodule
