// NE

module t;
   int done;

   initial begin
      done = 0;

      myprint(5);

      if (done !== 1)
        $fatal(0, "FAIL-1");
      $display("PASS");
   end // initial begin

   function automatic void myprint(int a);
      done = a == 5;
   endfunction
endmodule
