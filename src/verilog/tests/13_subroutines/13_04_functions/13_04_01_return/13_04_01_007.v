// CE

module mod1;
   class c1;
   endclass // c1

   class c2;
   endclass // c1

   c2 c;

   function c1 myfunc;
      //  Incorrect type
      return c;
   endfunction

   initial begin
      if (myfunc() !== null)
	$fatal(0, "FAIL");
      $display("PASS");
   end // initial begin
endmodule
