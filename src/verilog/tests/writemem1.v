module writemem1_tb();
    reg [7:0] memory [0:15];
    initial begin
       memory[0] = 'h1f;
       memory[1] = 'h2e;
       memory[2] = 'h3d;
       $display("Saving ram.");
       $writememh("writemem1.mem", memory);
       $display("PASS");
    end
endmodule
