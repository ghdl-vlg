module inst1;
   reg [3:0] a;
   wire [3:0] r;

   copy inst2 (r, a);
   
   initial begin
      a = 2;
      #1 if (r != 2) begin
	 $display ("FAIL");
	 $finish;
      end
      a = 5;
      #1 if (r != 5) begin
	 $display ("FAIL");
	 $finish;
      end
      $display ("PASS");
   end
endmodule

module copy (a, o);
   input [3:0] a;
   output [3:0] o;
   wire [3:0] 	a;
   wire [3:0] 	o;
   
   assign o = a;
endmodule // copy
