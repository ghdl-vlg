module func7();

   reg t;
   
   function init(bit[7:0] val);
      $display("value is %x", val);
      t = 1;
      return 1;
   endfunction

   initial begin
      t = 0;
      init(7);
      if (t == 1)
	$display("PASS");
      else
	$display("FAIL");
      $finish;
   end
endmodule
