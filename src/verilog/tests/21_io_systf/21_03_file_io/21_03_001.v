// NE
module m1;
   initial begin
      int f = $fopen("21_io_systf/21_03_file_io/21_03_001.in","r");

      while(!$feof(f))
        begin
           int line;
           string cmd;

           void'($fscanf(f,"%x %s", line, cmd));
        end
      $display("PASS");
   end // initial begin
endmodule // m1
