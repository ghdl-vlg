module packed1();
   reg [15:0] w;

   initial
     begin
	w = 'h1234;
	if (w != 16'h1234)
	  begin
	     $display("FAIL-1");
	     $finish;
	  end
	w[15:8] = w[7:0];
	if (w != 16'h3434)
	  begin
	     $display("FAIL-2");
	     $finish;
	  end
	$display("PASS");
	$finish;
     end
endmodule
