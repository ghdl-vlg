module concat3;
   wire [1:0] out;
   reg        r;
   assign out = {r, 1'b1};

   initial begin
      r = 0;
      # 1;
      if (out !== 2'b01)
        begin
	   $display("FAIL");
           $finish;
        end
      r = 1;
      # 1;
      if (out !== 2'b11)
        begin
	   $display("FAIL");
           $finish;
        end

      $display("PASS");
   end
endmodule
