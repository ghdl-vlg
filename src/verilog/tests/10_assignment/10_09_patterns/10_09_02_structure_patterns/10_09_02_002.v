// NE
module t;
   typedef struct packed {
      logic 	  e;
      int 	  i;
   } my_t;

   my_t a;
   initial begin
      a = '{e: 1'b1, i: 25};
      if (a !== 33'h1_0000_0019)
       $fatal(0, "FAIL");

    $display("PASS");
  end
endmodule
