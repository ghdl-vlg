// NE
//  1800-2017 10.9.2 first example
module mod1;
   typedef struct {
      int 	  x;
      int 	  y;
   } st;
   st s1;
   initial begin
      s1 = '{default:2}; // sets x and y to 2
      if (s1.x !== 32'h2)
	$fatal(0, "FAIL-1");
      if (s1.y !== 32'h2)
	$fatal(0, "FAIL-2");
      $display("PASS");
   end
endmodule
