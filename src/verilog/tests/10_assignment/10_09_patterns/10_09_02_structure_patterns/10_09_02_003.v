// NE
//  1800-2017 10.9.2 first example
module mod1;
   typedef struct {
      int 	  x;
      int 	  y;
   } st;
   st s1;
   int 		  k = 1;
   initial begin
      #1 s1 = '{1, 2+k};
      #1 $display( s1.x, s1.y);
      if (s1.x !== 32'h1)
	$fatal(0, "FAIL-1");
      if (s1.y !== 32'h3)
	$fatal(0, "FAIL-2");
      #1 s1 = '{x:2, y:3+k};
      #1 $display( s1.x, s1.y);
      if (s1.x !== 32'h2)
	$fatal(0, "FAIL-3");
      if (s1.y !== 32'h4)
	$fatal(0, "FAIL-4");
      $display("PASS");
   end
endmodule
