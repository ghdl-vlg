// NE
//  1800-2017 10.9.2 6th example
module mod1;
   typedef struct {
      logic [7:0] a;
      bit 	  b;
      bit signed [31:0] c;
      string 		s;
   } sa;

   sa s2;
   initial begin
      s2 = '{default:'1, s : ""}; // set all to 1 except s to ""

      if (s2.a !== 8'h1)
	$fatal(0, "FAIL-1");
      if (s2.b !== 1'h1)
	$fatal(0, "FAIL-2");
      if (s2.c !== -32'h1)
	$fatal(0, "FAIL-3");
      if (s2.s !== "")
	$fatal(0, "FAIL-4");
      $display("PASS");
   end
endmodule
