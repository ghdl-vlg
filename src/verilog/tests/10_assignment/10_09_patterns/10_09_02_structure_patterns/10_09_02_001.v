// NE
module t;
   typedef struct packed {
      logic 	  e;
      integer 	  i;
   } my_t;

   localparam my_t a = '{e: 1'b1, i: 25};
   initial begin
      if (a !== 33'h1_0000_0019)
       $fatal(0, "FAIL");

    $display("PASS");
  end
endmodule
