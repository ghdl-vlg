// CO

module t;
   byte data[$];

   initial begin
      data = {};
      if (data.size() !== 0)
        $fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // t
