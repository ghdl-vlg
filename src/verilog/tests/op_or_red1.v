module op_or_red1;

   reg [1:5] a;
   wire      res;

   assign res = |a;

   initial begin
      if (res !== 1'bx) begin
	 $display("FAILED: a === %b", a);
	 $finish;
      end

      a = 5'b11111;
      #1 ;
      if (res !== 1) begin
         $display("FAILED: result === %b", res);
         $finish;
      end

      a = 5'b00000;
      #1 ;
      if (res !== 0) begin
         $display("FAILED: result === %b", res);
         $finish;
      end

      a = 5'b1010z;
      #1 ;
      if (res !== 1) begin
         $display("FAILED: result === %b", res);
         $finish;
      end

      a = 5'bx0xzz;
      #1 ;
      if (res !== 1'bx) begin
         $display("FAILED: result === %b", res);
         $finish;
      end

      $display("PASSED");
   end // initial begin
endmodule // main
