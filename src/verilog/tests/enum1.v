module enum1();
   enum {RED, ORANGE, GREEN} color;

   initial
     begin
	color = RED;
	if (color != RED)
	  begin
	     $display("FAIL-1");
	     $finish;
	  end
	if (color.name() != "RED")
	  begin
	     $display("FAIL-2");
	     $finish;
	  end
	color = GREEN;
	if (color != GREEN)
	  begin
	     $display("FAIL-3");
	     $finish;
	  end
	$display("PASS");
     end // initial begin
endmodule
