module mixed_direction (.p({a, e}));
input a;
// p contains both input and output directions.
output e;
endmodule
