module struct1;
   struct { reg [7:0] a; logic b; } v = '{ 'ha5, 'b0 };

   initial begin
      if (v.a == 'b10100101)
	$display("PASS");
      else
	$display("FAIL");
   end
endmodule
