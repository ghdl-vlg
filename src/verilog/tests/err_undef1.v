module err_undef1;
   reg [1:0] resff;

   reg 	     clk = 0;
   integer   i;

   initial
     for (i = 0; i < 10; i = i + 1)
       clk = ~clk;
   
    always @(posedge clk)
      resff <= resff<<1 | res;
endmodule
