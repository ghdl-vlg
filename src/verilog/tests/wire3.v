module wire3;
   wire a;
   reg 	r;
   assign a = r;

   initial
     begin
	r = 1;
	# 1 if (a !== 1) begin
	   $display ("FAIL");
	   $finish;
	end
	$display ("PASS");
     end
endmodule
