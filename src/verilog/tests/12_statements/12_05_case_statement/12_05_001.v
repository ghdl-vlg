// NE
module t;
   reg [3:0] v;
   reg [1:0] r;

   always @(v)
     case (v)
       4'h0: r <= 0;
       4'h1, 4'h2: r <= 1;
       4'h3: r <= 2;
       default: r <= 3;
     endcase // case (v)

   initial begin
      v <= 0;
      # 1;
      if (r != 0)
	$fatal(0, "FAIL-1");
      
      v <= 1;
      # 1;
      if (r != 1)
	$fatal(0, "FAIL-2");

      v <= 2;
      # 1;
      if (r != 1)
	$fatal(0, "FAIL-3");

      v <= 3;
      # 1;
      if (r != 2)
	$fatal(0, "FAIL-4");

      v <= 5;
      # 1;
      if (r != 3)
	$fatal(0, "FAIL-5");

      $display("PASS");
   end // initial begin
endmodule // t

