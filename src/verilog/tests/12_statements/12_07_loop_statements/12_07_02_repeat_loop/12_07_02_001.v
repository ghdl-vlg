// NE
module rep;
  reg [7:0] c;

  initial begin
    c = 0;
    repeat (3)
      repeat (4)
        #1 c = c + 1;
    if (c !== 12)
      $fatal(0, "FAIL");
    $display("PASS");
  end
endmodule
