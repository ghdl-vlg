// NE
module forever1;
   var integer i;

   initial begin
      i = 0;
      forever begin
         i++;
         if (i == 5) begin
            $display("PASS");
            $finish;
         end
         if (i > 10) begin
            $display("FAIL");
            $finish;
         end
      end // forever begin
   end // initial begin
endmodule // forever1
