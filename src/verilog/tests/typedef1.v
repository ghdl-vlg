typedef logic[15:0] word;

module typedef1;
   word w = 'h1234;
   
   initial begin
      if (w[12] == 1)
	$display("PASS");
      else
	$display("FAIL");
   end
endmodule
