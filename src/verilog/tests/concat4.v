module concat4;
   reg  [31:0] r;

   wire [31:0] d = { {21{ r[31] }}, r[30:25], r[24:21], r[20] };
   wire [31:0] d2 = { {21{ r[31] }}, r[30:20] };
   
   initial begin
      r = 32'b00_00001_0000_0_00010000010000010011;
      
      # 1;
      if (d !== 32'b000000100000)
	$fatal(0, "FAIL-1a");
      if (d2 !== 32'b000000100000)
	$fatal(0, "FAIL-1b");

      $display("PASS");
   end
endmodule
