module drive1;
   wire [7:0] byt;
   wire [7:0] cop = byt;
   reg [3:0] uh;
   reg [2:0] lh;
   reg       b0;

   assign byt[7:4] = uh;
   assign byt[3:0] = {lh, b0};

   initial begin
      uh = 0;
      lh = 0;
      b0 = 0;
      # 1;
      if (byt !== 8'h00)
        $fatal(0, "FAIL-1");
      lh = 3;
      # 1;
      if (byt !== 8'h06)
        $fatal(0, "FAIL-2");
      uh = 4'he;
      # 1;
      if (byt !== 8'he6)
        $fatal(0, "FAIL-3");
      b0 = 1;
      # 1;
      if (byt !== 8'he7)
        $fatal(0, "FAIL-4");

      $display("PASS");
      $finish;
   end
endmodule
