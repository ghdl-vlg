// NE
module len2;
   logic [3:0] addr;
   logic [15:0] val;

   initial begin
      val = 16'b1011001110001111;
      addr = 3;

      if (val[addr -: 4] !== 4'hf)
        $fatal(0, "FAIL-1");

      if (val[addr + 0 -: 4] !== 4'hf)
        $fatal(0, "FAIL-2");

      $display("PASS");
   end
endmodule
