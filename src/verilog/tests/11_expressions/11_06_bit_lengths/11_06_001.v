// NE
module len1;
   logic [3:0] addr;
   logic [15:0] val;

   initial begin
      val = 16'b1011001110001111;
      addr = 3;

      if (val[addr] !== 1)
        $fatal(0, "FAIL-1");

      if (val[addr + 0] !== 1)
        $fatal(0, "FAIL-2");

      if (val[addr + 1] !== 0)
        $fatal(0, "FAIL-3");

      $display("PASS");
   end
endmodule
