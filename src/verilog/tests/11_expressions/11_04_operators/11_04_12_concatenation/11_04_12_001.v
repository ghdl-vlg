// NE
module t;
   initial begin
      reg unsigned [3:0] a;

      a = {2'b10, 2'b01};
      if (a !== 4'b1001)
	$fatal(0, "FAIL-1");

      //  0-sized elements are allowed.
      a = {1'b1, {1-1{2'b11}}, 2'b01};
      if (a !== 4'b0101)
	$fatal(0, "FAIL-2");

      $display("PASS");
  end
endmodule
