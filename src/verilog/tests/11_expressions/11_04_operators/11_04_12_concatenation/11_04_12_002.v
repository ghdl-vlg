// CE
module t;
   initial begin
      reg unsigned [3:0] a;

      a = 3 + {1-1{2'b11}};

      $display("PASS");
  end
endmodule
