// NE
module t;
   initial begin
      string hello = "hello";
      string s;
      s = { hello, " ", "world" };
      if (s != "hello world")
        $fatal(0, "FAIL-1");
      s = { s, " and goodbye" };
      if (s != "hello world and goodbye")
        $fatal(0, "FAIL-2");

      $display("PASS");
  end
endmodule
