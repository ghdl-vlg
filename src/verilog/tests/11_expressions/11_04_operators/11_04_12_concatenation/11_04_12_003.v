// CE
module t;
   initial begin
      reg unsigned [3:0] a;

      a = {{0{1'b0}}, {1-1{2'b11}}};

      $display("PASS");
  end
endmodule
