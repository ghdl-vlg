// NE
// From table 11-6
module t;
   initial begin
      reg unsigned [3:0] a;

      a = ~4'bx0z1;
      if (a !== 4'bx1x0)
	$fatal(0, "FAIL-1");

      // &
      a = 4'bx0z1 & 4'b0000;
      $display("a=%b", a);
      if (a !== 4'b0000)
	$fatal(0, "FAIL-10");

      a = 4'bx0z1 & 4'b1111;
      $display("a=%b", a);
      if (a !== 4'bx0x1)
	$fatal(0, "FAIL-11");

      a = 4'bx0z1 & 4'bxxxx;
      $display("a=%b", a);
      if (a !== 4'bx0xx)
	$fatal(0, "FAIL-12");

      a = 4'bx0z1 & 4'bzzzz;
      $display("a=%b", a);
      if (a !== 4'bx0xx)
	$fatal(0, "FAIL-13");

      // |
      a = 4'bx0z1 | 4'b0000;
      $display("a=%b", a);
      if (a !== 4'bx0x1)
	$fatal(0, "FAIL-20");

      a = 4'bx0z1 | 4'b1111;
      $display("a=%b", a);
      if (a !== 4'b1111)
	$fatal(0, "FAIL-21");

      a = 4'bx0z1 | 4'bxxxx;
      $display("a=%b", a);
      if (a !== 4'bxxx1)
	$fatal(0, "FAIL-22");

      a = 4'bx0z1 | 4'bzzzz;
      $display("a=%b", a);
      if (a !== 4'bxxx1)
	$fatal(0, "FAIL-23");

      // ^
      a = 4'bx0z1 ^ 4'b0000;
      $display("a=%b", a);
      if (a !== 4'bx0x1)
	$fatal(0, "FAIL-30");

      a = 4'bx0z1 ^ 4'b1111;
      $display("a=%b", a);
      if (a !== 4'bx1x0)
	$fatal(0, "FAIL-31");

      a = 4'bx0z1 ^ 4'bxxxx;
      $display("a=%b", a);
      if (a !== 4'bxxxx)
	$fatal(0, "FAIL-32");

      a = 4'bx0z1 ^ 4'bzzzz;
      $display("a=%b", a);
      if (a !== 4'bxxxx)
	$fatal(0, "FAIL-33");

      // ~^
      a = 4'bx0z1 ~^ 4'b0000;
      $display("a=%b", a);
      if (a !== 4'bx1x0)
	$fatal(0, "FAIL-40");

      a = 4'bx0z1 ~^ 4'b1111;
      $display("a=%b", a);
      if (a !== 4'bx0x1)
	$fatal(0, "FAIL-41");

      a = 4'bx0z1 ~^ 4'bxxxx;
      $display("a=%b", a);
      if (a !== 4'bxxxx)
	$fatal(0, "FAIL-42");

      a = 4'bx0z1 ~^ 4'bzzzz;
      $display("a=%b", a);
      if (a !== 4'bxxxx)
	$fatal(0, "FAIL-43");

      // ^~
      a = 4'bx0z1 ^~ 4'b0000;
      $display("a=%b", a);
      if (a !== 4'bx1x0)
	$fatal(0, "FAIL-50");

      a = 4'bx0z1 ^~ 4'b1111;
      $display("a=%b", a);
      if (a !== 4'bx0x1)
	$fatal(0, "FAIL-51");

      a = 4'bx0z1 ^~ 4'bxxxx;
      $display("a=%b", a);
      if (a !== 4'bxxxx)
	$fatal(0, "FAIL-52");

      a = 4'bx0z1 ^~ 4'bzzzz;
      $display("a=%b", a);
      if (a !== 4'bxxxx)
	$fatal(0, "FAIL-53");


    $display("PASS");
  end
endmodule
