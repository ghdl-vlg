// NE
module mod1;
   typedef enum { A, B, C} my_enum;
   logic 	l;
   my_enum e1, e2;
   initial begin
      l = 1;
      e2 = C;
      e1 = l ? e2 : B;

      if (e1 !== C)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule
