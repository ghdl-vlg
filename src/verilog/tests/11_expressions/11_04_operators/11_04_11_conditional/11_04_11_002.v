// NE
module mod1;
   logic 	l;
   string 	s1, s2;

   initial begin
      l = 1;
      s1 = "hello";
      $display("res=%s", l ? s1 : "bye");
      $display("PASS");
   end
endmodule
