// NE
// From table 11-6
module t;
   initial begin
      reg unsigned [8:0] a;

      // &
      a = & 4'b1011;
      if (a !== 1'b0)
	$fatal(0, "FAIL-1");

      a = & 4'b1111;
      if (a !== 1'b1)
	$fatal(0, "FAIL-2");

      a = & 4'b11x1;
      if (a !== 1'bx)
	$fatal(0, "FAIL-3");

      a = & 4'bz110;
      if (a !== 1'b0)
	$fatal(0, "FAIL-4");

      // |
      a = | 4'b1011;
      if (a !== 1'b1)
	$fatal(0, "FAIL-5");

      a = | 4'b0000;
      if (a !== 1'b0)
	$fatal(0, "FAIL-6");

      a = | 4'b00x0;
      if (a !== 1'bx)
	$fatal(0, "FAIL-7");

      a = | 4'bz010;
      if (a !== 1'b1)
	$fatal(0, "FAIL-8");

      // ^
      a = ^ 4'b1011;
      if (a !== 1'b1)
	$fatal(0, "FAIL-9");

      a = ^ 4'b0000;
      if (a !== 1'b0)
	$fatal(0, "FAIL-10");

      a = ^ 4'b00x0;
      if (a !== 1'bx)
	$fatal(0, "FAIL-11");

      a = ^ 4'bz010;
      if (a !== 1'bx)
	$fatal(0, "FAIL-12");

      a = ^ 4'b1010;
      if (a !== 1'b0)
	$fatal(0, "FAIL-13");

      // ~&
      a = ~& 4'b1011;
      if (a !== 1'b1)
	$fatal(0, "FAIL-21");

      a = ~& 4'b1111;
      if (a !== 1'b0)
	$fatal(0, "FAIL-22");

      a = ~& 4'b11x1;
      if (a !== 1'bx)
	$fatal(0, "FAIL-23");

      a = ~& 4'bz110;
      if (a !== 1'b1)
	$fatal(0, "FAIL-24");

      // ~|
      a = ~| 4'b1011;
      if (a !== 1'b0)
	$fatal(0, "FAIL-25");

      a = ~| 4'b0000;
      if (a !== 1'b1)
	$fatal(0, "FAIL-26");

      a = ~| 4'b00x0;
      if (a !== 1'bx)
	$fatal(0, "FAIL-27");

      a = ~| 4'bz010;
      if (a !== 1'b0)
	$fatal(0, "FAIL-28");

      // ~^
      a = ~^ 4'b1011;
      if (a !== 1'b0)
	$fatal(0, "FAIL-29");

      a = ~^ 4'b0000;
      if (a !== 1'b1)
	$fatal(0, "FAIL-30");

      a = ~^ 4'b00x0;
      if (a !== 1'bx)
	$fatal(0, "FAIL-31");

      a = ~^ 4'bz010;
      if (a !== 1'bx)
	$fatal(0, "FAIL-32");

      a = ~^ 4'b1010;
      if (a !== 1'b1)
	$fatal(0, "FAIL-33");

      // ^~
      a = ^~ 4'b1011;
      if (a !== 1'b0)
	$fatal(0, "FAIL-34");

      a = ^~ 4'b0000;
      if (a !== 1'b1)
	$fatal(0, "FAIL-35");

      a = ^~ 4'b00x0;
      if (a !== 1'bx)
	$fatal(0, "FAIL-36");

      a = ^~ 4'bz010;
      if (a !== 1'bx)
	$fatal(0, "FAIL-37");

      a = ^~ 4'b1010;
      if (a !== 1'b1)
	$fatal(0, "FAIL-38");


    $display("PASS");
  end
endmodule
