// NE
// From table 11-6
module t;
  initial begin
    reg signed [8:0] a;

    a = 10 % 3;
    if (a !== 1)
      $fatal(0, "FAIL-1");

    a = 11 % 3;
    if (a !== 2)
      $fatal(0, "FAIL-2");

    a = 12 % 3;
    if (a !== 0)
      $fatal(0, "FAIL-3");

    a = -10 % 3;
    if (a !== -1)
      $fatal(0, "FAIL-4");

    a = 11 % -3;
    if (a !== 2)
      $fatal(0, "FAIL-5");

    /* unsigned mod
    a = -4'd12 % 3;
    if (a !== 1)
      $fatal(0, "FAIL-6");
    */

    $display("PASS");
  end
endmodule
