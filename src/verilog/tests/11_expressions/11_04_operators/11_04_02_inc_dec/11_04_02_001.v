// NE

module t1;
   initial begin
      int a, b, c;
      a = 10;
      b = ++a;
      c = a++;
      if (a !== 12)
        $fatal(0, "FAIL-1");
      if (b !== 11)
        $fatal(0, "FAIL-2");
      if (c !== 11)
        $fatal(0, "FAIL-3");
      $display("PASS");
   end
endmodule
