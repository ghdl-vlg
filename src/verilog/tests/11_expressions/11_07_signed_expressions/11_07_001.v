// NE

// Example from 1800-2017 11.7 Signed expressions
module sign1;
   logic [7:0] regA, regB;
   logic signed [7:0] regS;

   initial begin
      regA = $unsigned(-4);
      if (regA !== 8'b11111100)
        $fatal(0, "FAIL-1");

      regB = $unsigned(-4'sd4);
      if (regB !== 8'b00001100)
        $fatal(0, "FAIL-2");

      regS = $signed(4'b1100);
      if (regS !== -4)
        $fatal(0, "FAIL-3");

      $display("PASS");
   end
endmodule // sign1
