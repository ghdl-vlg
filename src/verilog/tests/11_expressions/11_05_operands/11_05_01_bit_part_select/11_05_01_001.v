// NE
module t;
  initial begin
     logic [31:0] a_vect;
     logic [0:31] b_vect;


     a_vect = 32'h12345678;
     b_vect = 32'h9abcdef2;

     if (a_vect[0 +: 8] !== a_vect[7:0])
       $fatal(0, "FAIL-1");
     if (a_vect[15 -: 8] !== a_vect[15:8])
       $fatal(0, "FAIL-2");

     if (b_vect[0 +: 8] !== b_vect[0:7])
       $fatal(0, "FAIL-3");
     if (b_vect[15 -: 8] !== b_vect[8:15])
       $fatal(0, "FAIL-4");

    $display("PASS");
  end
endmodule
