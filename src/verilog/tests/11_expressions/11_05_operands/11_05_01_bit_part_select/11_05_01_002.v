// NE
module t;
  initial begin
     logic [7:0] a_vect;
     logic [0:7] b_vect;

     a_vect = 8'h78;
     b_vect = 8'hf2;

     //$display("a[4:-1]=%b", a_vect[4:-1]);
     if (a_vect[4:-1] !== 6'b11000x)
       $fatal(0, "FAIL-1");

     if (a_vect[8:4] !== 5'bx0111)
       $fatal(0, "FAIL-2");

     if (a_vect[8:-1] !== 10'bx01111000x)
       $fatal(0, "FAIL-3");

     if (a_vect[-2:-3] !== 2'bxx)
       $fatal(0, "FAIL-4");

     if (a_vect[10:8] !== 3'bxxx)
       $fatal(0, "FAIL-5");


     if (b_vect[4:8] !== 5'b0010x)
       $fatal(0, "FAIL-6");

     if (b_vect[-1:3] !== 5'bx1111)
       $fatal(0, "FAIL-7");

     if (b_vect[-1:8] !== 10'bx11110010x)
       $fatal(0, "FAIL-8");

     if (b_vect[8:10] !== 3'bxxx)
       $fatal(0, "FAIL-9");

     if (b_vect[-5:-2] !== 4'bxxxx)
       $fatal(0, "FAIL-10");

    $display("PASS");
  end
endmodule
