// NE
module t;
  initial begin
     logic [31:1-1] a_vect;

     a_vect = 32'hf2345678;

     if (a_vect !== 32'hf2345678)
       $fatal(0, "FAIL-1");

     if (a_vect[3:-1+1] !== 4'h8)
       $fatal(0, "FAIL-2");

    $display("PASS");
  end
endmodule
