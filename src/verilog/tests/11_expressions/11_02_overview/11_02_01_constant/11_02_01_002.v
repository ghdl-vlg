// NE
module t;
   localparam int unsigned v = 17;
   localparam int unsigned lv = $clog2(v);
   wire [lv-1:0]  w;

   initial begin
     if (lv !== 5)
       $fatal(0, "FAIL");

    $display("PASS");
  end
endmodule
