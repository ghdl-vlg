module concat2;
   reg [31:0] c;

   initial begin
      c = {8{2'b10}};
      if (c == 'haaaa)
	$display("PASS");
      else
	$display("FAIL");
   end
endmodule
