module serrparam4 #([7:0] a = 123)(output wire [7:0] res);
   parameter b = 14;

   assign res = b;
endmodule

module err_param4;
   wire [7:0] val;

   serrparam4 #(.a(120), .unknown(13)) inst (val);

   initial begin
      # 1;
      $display ("PASS");
   end
endmodule
