module serrparam1 #([7:0] a = 123)(output wire [7:0] res);
   parameter b = 14;

   assign res = b;
endmodule

module err_param1;
   wire [7:0] val;

   serrparam1 #(120, 13, 25) inst (val);

   initial begin
      # 1;
      $display ("PASS");
   end
endmodule
