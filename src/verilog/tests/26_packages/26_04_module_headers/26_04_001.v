// NE
package pkg;
   typedef enum logic [1:0] {
     OP_ADD,
     OP_SUB,
     OP_AND
   } op_t;
endpackage // pkg

module tmod import pkg::*; #(localparam op_t op = OP_AND);

   initial begin
      if (op !== 2'b10)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
