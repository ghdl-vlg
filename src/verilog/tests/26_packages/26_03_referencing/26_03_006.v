// NE
package pkg;
   localparam [3:0] val = 4'hd;
endpackage // pkg

module tmod;
   import pkg::*;

   initial begin
      if (val !== 4'b1101)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
