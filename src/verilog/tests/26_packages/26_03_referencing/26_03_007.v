// NE
package pkg;
   typedef enum logic [1:0] {
     OP_ADD,
     OP_SUB,
     OP_AND
   } op_t;
endpackage // pkg

module tmod;
   import pkg::*;

   initial begin
      if (OP_AND !== 2'b10)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
