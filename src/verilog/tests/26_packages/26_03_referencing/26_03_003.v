// NE
package ComplexPkg;
  typedef struct {
    shortreal i, r;
  } Complex;

  function Complex add(Complex a, b);
    add.r = a.r + b.r;
    add.i = a.i + b.i;
  endfunction

  function Complex mul(Complex a, b);
    mul.r = (a.r * b.r) - (a.i * b.i);
    mul.i = (a.r * b.i) + (a.i * b.r);
  endfunction
endpackage : ComplexPkg

module t;
  initial begin
    ComplexPkg::Complex a, b;
    ComplexPkg::Complex cout;
    a = '{1.0, 0.0};
    b = a;
    cout = ComplexPkg::mul(a, b);
    if (cout.r != -1.0 || cout.i != 0.0)
      $fatal(0, "FAIL-1");
    $display("PASS");
  end
endmodule
