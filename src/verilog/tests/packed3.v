module packed3();
   //  1800-2017 7.4.5 Multidimensional arrays
   //  2 elements of 4 8-bit bytes
   reg [7:0][0:3] m[0:1] = '{{8'h0,8'h1,8'h2,8'h3},
			     {8'h4,8'h5,8'h6,8'h7}};

   initial
     begin
	$display("m[0] = %h", m[0]);
	$display("m[1] = %h", m[1]);
	if (m[0] != 32'h00010203)
	  begin
	     $display("FAIL-1");
	     $finish;
	  end
	#1
	m[0][7] = m[0][0];
	$display("m[0] = %h", m[0]);
	if (m[0] != 32'h30010203)
	  begin
	     $display("FAIL-2");
	     $finish;
	  end
	$display("PASS");
	$finish;
     end
endmodule
