`define debug(x)

module t;
   reg a;

   always @(a)
     begin
	a = 1;
	`debug($display("a: %x", a);)
     end
   initial
     $display("PASS");
endmodule // t

