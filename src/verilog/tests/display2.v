module display2;
   reg [14:0] w;

   initial begin
      w = 'b111_1101_xxxx_zzzz;
      $display ("v = x'%x", w);
      $display ("v = b'%b", w);
      w = ~w;
      $display ("v = x'%x", w);
      $display ("v = b'%b", w);
      w = 'b01x_011z_01xz_xxzz;
      $display ("v = x'%x", w);
      $display ("v = b'%b", w);
      $display ("PASS");
   end
endmodule
