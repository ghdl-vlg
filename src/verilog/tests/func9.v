module func9();
   function integer clog2;
      input integer value;
      integer 	    temp;

      temp = value - 1;
      for (clog2 = 0; temp > 0; clog2 = clog2 + 1)
         temp = temp >> 1;
   endfunction

   initial begin
      integer r;
      r = clog2(3);
      if (r !== 2)
	$fatal(0, "FAIL-0");
      r = clog2(17);
      if (r !== 5)
	$fatal(0, "FAIL-1");
      
      $display("PASS");
   end
endmodule
