module portdecl1a (
   output reg   a,
   output reg [3:0] b,
   output wire  c,
   output wire [3:0] d);

   initial
     begin
        a <= 1;
        b <= 5;
     end

   assign c <= 0;
   assign d <= 'ha;
endmodule // portdecl1a

module portdecl1 ();
   wire a1;
   wire [3:0] b1;
   wire       c1;
   wire [3:0] d1;

   inst portdecl1a (a1, b1, c1, d1);
endmodule // portdecl1
