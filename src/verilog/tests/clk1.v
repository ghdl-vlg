module clk1;
   reg clk;

   always
     #10 clk = ~clk;

   initial begin
      clk = 0;
      #80 ;
      if (clk != 1)
	$display ("FAIL");
      else
	$display ("PASS");
      $finish;
   end
endmodule // clk1
