module portdecl4a (a,b);
   input a;
   output b;
   reg 	  b;

   initial
        b <= 1;
endmodule

module portdecl4 ();
   wire a1, b1;

   portdecl4a inst (a1, b1);
   initial
     # 1 if (b1 == 1)
       $display("PASS");
     else
       $display("FAIL");

endmodule
