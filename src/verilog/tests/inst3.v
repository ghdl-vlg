module inst3;
   reg  a;

   copy inst1 (a, r1);
   copy inst2 (r1, r2);

   initial begin
      a = 1;
      #1 if (r2 != 1) begin
	 $display ("FAIL");
	 $finish;
      end
      a = 0;
      #1 if (r2 != 0) begin
	 $display ("FAIL");
	 $finish;
      end
      $display ("PASS");
   end
endmodule

module copy (a, o);
   input a;
   output o;
   wire   o;

   assign o = a;
endmodule // copy
