module wire1;
   wire a;
   wire b;

   assign b = a;
   assign a = 1;

   initial
     begin
	/* TODO:
	    #2 ;
	    b != 1
	 */
	# 1 if (b !== 1) begin
	   $display ("FAIL");
	   $finish;
	end
	$display ("PASS");
     end
endmodule // wire1
