module bitsel2;
   reg [0:3] r;
   reg 	     v;
   
   initial begin
      r = 'hd;
      v = r[1];
      
      if (v != 1) begin
	 $display ("FAIL 1");
	 $finish;
      end
      
      if (r[2] != 0) begin
	 $display ("FAIL 2");
	 $finish;
      end
      $display ("PASS");
   end
endmodule
