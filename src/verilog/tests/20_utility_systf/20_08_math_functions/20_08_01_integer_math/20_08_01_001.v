// NE
module tclog;
  initial begin
    integer res;

    res = $clog2(0);
    if (res != 0)
      $fatal(0, "FAIL-1");

    res = $clog2(4);
    if (res != 2)
      $fatal(0, "FAIL-2");

    res = $clog2(6);
    if (res != 3)
      $fatal(0, "FAIL-3");

    res = $clog2(63'h67_00000000);
    if (res != 39)
      $fatal(0, "FAIL-4");

    res = $clog2(16'b100x00);
    if (res !== 32'bx)
      $fatal(0, "FAIL-5");

    $display("PASS");
  end
endmodule
