// NE

module m1;
   localparam int int_len = $bits(int);
   typedef logic[int_len-1:0] my_int;

   initial begin
      my_int v = {$bits(byte){1'b1}};
      if (v !== 255)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule // m1
