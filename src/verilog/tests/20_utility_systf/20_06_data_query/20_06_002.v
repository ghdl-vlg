// NE

module m1;
   initial begin
      typedef logic[$bits(int)-1:0] my_int;
      if ($bits(my_int) !== 32)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule // m1
