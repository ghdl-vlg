// NE
module m1;
   typedef logic [16:1] Word;
   Word Ram[0:9];

   initial begin
      if ($size(Ram) !== 10)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // m1
