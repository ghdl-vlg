// CE
module gray2bin1;
  parameter SIZE = 8; // this module is parameterizable
  wire [SIZE-1:0] bin;
  reg [SIZE-1:0] gray;
  genvar i;

  assign bin = i;
endmodule
