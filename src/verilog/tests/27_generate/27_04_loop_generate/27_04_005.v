// NE
module mod1 #(logic v) (output o);
   assign o = v;
endmodule //

module top;
   parameter [7:0] VAL = 8'h2d;
   wire [7:0] w;

   for (genvar i=0; i<8; i=i+1)
     mod1 #(VAL[i]) asn (w[i]);

  initial begin
     #1;
     if (w != VAL)
       $fatal(0, "FAIL-0");
    $display("PASS");
  end
endmodule
