// NE
module gray2bin1;
  parameter SIZE = 8; // this module is parameterizable
  wire [SIZE-1:0] bin;
  reg [SIZE-1:0] gray;
  genvar i;
  generate
    for (i=0; i<SIZE; i=i+1) begin:bitnum
      assign bin[i] = ^gray[SIZE-1:i];
    // i refers to the implicitly defined localparam whose
    // value in each instance of the generate block is
    // the value of the genvar when it was elaborated.
    end
  endgenerate

  initial begin
     gray <= 8'h00;
     #1;
     if (bin != 8'h00)
       $fatal(0, "FAIL-0");
     gray <= 8'b1111;
     #1;
     if (bin != 8'b1010)
       $fatal(0, "FAIL-1");
    $display("PASS");
  end
endmodule
