// NE
module gray2bin1 (bin, gray);
  parameter SIZE = 8; // this module is parameterizable
  output [SIZE-1:0] bin;
  input [SIZE-1:0] gray;
  genvar i;
  generate
    for (i=0; i<SIZE; i=i+1) begin:bitnum
      assign bin[i] = ^gray[SIZE-1:i];
    // i refers to the implicitly defined localparam whose
    // value in each instance of the generate block is
    // the value of the genvar when it was elaborated.
    end
  endgenerate
endmodule

module tgray;
  reg [7:0] b;
  wire [7:0] g;

  gray2bin1 g2b(b, g);
  initial begin
    b <= 8'h00;
    #1;
    if (g != 8'h00)
      $fatal(0, "FAIL-0");
    $display("PASS");
  end
endmodule
