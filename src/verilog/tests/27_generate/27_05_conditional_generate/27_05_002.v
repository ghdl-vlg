// NE
module sgen(output o);
   parameter set = 0;

   if (set)
     assign o = 1'b1;
   else
     assign o = 1'b0;
endmodule // sgen

module genif;
   wire o1;
   wire o2;

   sgen #(0) s1(o1);
   sgen #(1) s2(o2);

   initial begin
      if (o1 !== 1'b0)
        $fatal(0, "FAIL-0");
      if (o2 !== 1'b1)
        $fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule // genif
