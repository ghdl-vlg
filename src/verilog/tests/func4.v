module func4();

   function automatic integer sub (input integer l, r);
      return l - r;
   endfunction: sub

   initial begin
      if (sub(6, 1) != 5)
	begin
	   $display("FAIL-1");
	   $finish;
	end
      if (sub(.r(3), .l(7)) != 4)
	begin
	   $display("FAIL-2");
	   $finish;
	end
      $display("PASS");
      $finish;
   end
endmodule
