module gate1;
   wire a, b, o;
   and g1 (o, a, b);
   assign a = 1;
   assign b = 0;
   always begin
      #1 ;
      if (o == 0)
	$display ("PASS");
      else
	$display ("FAIL");
      $finish;
   end
endmodule // gate1

   
