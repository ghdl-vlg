module hierarchy2_sub2 (input i);
endmodule

module hierarchy2_sub (input i);
   hierarchy2_sub2 t(i);
endmodule // hierarchy1_sub

module hierarchy2;
   reg r;
   wire ri = r;
   hierarchy2_sub s(ri);

   initial begin
      r = 1;
      # 1;
      if (s.t.i !== 1)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
