module cond2;
   wire [3:0] res;
   reg 	      cond;
   wire [3:0]  t;
   wire [3:0]  f;

   assign t = 4'b101x;
   assign f = 4'b011x;

   assign res = cond ? t : f;

   initial begin
      #1;
      if (res !== 4'bxx1x)
	begin
	   $display("res=%b cond=%b t=%b f=%b", res, cond, t, f);
	   $display("FAIL - 1");
	   $finish;
	end
      cond = 1;
      # 1;
      if (res !== t)
	begin
	   $display("FAIL - 2");
	   $finish;
	end
      cond = 0;
      # 1;
      if (res !== f)
	begin
	   $display("FAIL");
	   $finish;
	end
      $display("PASS");
   end // initial begin
endmodule // cond2
