// NE
module t;
   reg [7:0] r;

  initial begin
     r[7:0] = 8'h21;
  end

  initial begin
     # 2;
    if (r !== 33)
     $fatal(0, "FAIL");
    $display("PASS");
  end
endmodule

