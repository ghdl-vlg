// NE
module t;
  reg clk;
  reg r;

  initial begin
    r <= 0;
    repeat(3) @(posedge clk) ;
    r <= 1;
    @(posedge clk) r <= 0;
  end

  initial
    repeat(5) begin
      # 2 clk <= 0;
      # 2 clk <= 1;
    end

  initial begin
    @(posedge clk); 
    @(posedge clk); 
    @(posedge clk); 
    # 1;
    if (r !== 1)
     $fatal(0, "FAIL");
    $display("PASS");
  end
endmodule
     
