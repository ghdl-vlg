module readmem1_tb();
    reg [7:0] memory [0:15];
    initial begin
        $display("Loading rom.");
        $readmemh("readmem1.mem", memory);
        if (memory[0] != 8'h01)
	begin
	   $display("FAIL 0");
	   $finish;
	end
        if (memory[1] != 8'hab)
	begin
	   $display("FAIL 1");
	   $finish;
	end
        if (memory[15] != 8'h1c)
	begin
	   $display("FAIL 15");
	   $finish;
	end
       $display("PASS");
    end
endmodule
