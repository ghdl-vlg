module assign1;
   reg [0:3] r;
   reg 	     v;

   initial begin
      {v, r} = 'h15;

      if (v != 1) begin
	 $display ("FAIL 1a");
	 $finish;
      end

      if (r != 5) begin
	 $display ("FAIL 1b");
	 $finish;
      end

      {r, v} = 'b10100;
      
      if (v != 0) begin
	 $display ("FAIL 2a");
	 $finish;
      end

      if (r != 10) begin
	 $display ("FAIL 2b");
	 $finish;
      end
      $display ("PASS");
   end
endmodule
