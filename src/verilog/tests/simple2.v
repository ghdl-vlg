module simple2;
   reg a;
   initial
     begin
	a = 1;
	#2 a = 0;
	#3 a = 1;
	#1 a = 0;
	$display ("PASS");
     end
endmodule // simple2
