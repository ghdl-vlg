module dff02
  (input  d,
   input  clk,
   input  rstn,
   output q);
  wire n3_o;
  wire n9_q;
  assign q = n9_q;
  /* dff02.vhdl:15:13  */
  assign n3_o = ~rstn;
  /* dff02.vhdl:17:5  */
  process (clk, n3_o)
  begin
    if n3_o = '1' then
      n9_q <= 1'b0;
    elsif posedge clk then
      n9_q <= d;
    end if;
  end process;
endmodule

