module anon01_sub
  (input  [7:0] i,
   output [7:0] o);
  wire [7:0] n7_o;
  assign o = n7_o;
  /* anon01.vhdl:8:10  */
  assign n7_o = i ^ "10100101";
endmodule

module anon01
  (input  [6:0] i,
   output [6:0] o);
  wire [7:0] res;
  wire [7:0] dut_o;
  wire [7:0] n2_o;
  wire [6:0] n4_o;
  assign o = n4_o;
  /* anon01.vhdl:17:10  */
  assign res = dut_o; // (signal)
  /* anon01.vhdl:19:3  */
  dut : entity work.anon01_sub port map (
    i => n2_o,
    o => dut_o);
  /* anon01.vhdl:20:24  */
  n2_o <= '0' & i;
  /* anon01.vhdl:22:12  */
  assign n4_o = res[6:0];
endmodule

