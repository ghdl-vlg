module asgn06
  (input  clk,
   input  s0,
   output [65:0] r);
  wire [65:0] n10_o;
  wire n11_o;
  wire n12_o;
  wire [3:0] n13_o;
  wire [3:0] n14_o;
  wire [3:0] n15_o;
  wire [3:0] n16_o;
  wire [3:0] n17_o;
  wire [55:0] n18_o;
  wire [55:0] n19_o;
  wire [55:0] n20_o;
  wire n21_o;
  wire n22_o;
  wire [65:0] n23_o;
  wire [65:0] n26_q;
  assign r = n26_q;
  assign n10_o = {1'b1, "1111111111111111111011101110111011011101110111011100110011001100", 1'b1};
  assign n11_o = n10_o[0];
  /* asgn06.vhdl:15:8  */
  assign n12_o = s0 ? 1'b0 : n11_o;
  assign n13_o = n10_o[4:1];
  assign n14_o = n26_q[4:1];
  /* asgn06.vhdl:15:8  */
  assign n15_o = s0 ? n14_o : n13_o;
  assign n16_o = n10_o[8:5];
  /* asgn06.vhdl:15:8  */
  assign n17_o = s0 ? 4'b1001 : n16_o;
  assign n18_o = n10_o[64:9];
  assign n19_o = n26_q[64:9];
  /* asgn06.vhdl:15:8  */
  assign n20_o = s0 ? n19_o : n18_o;
  assign n21_o = n10_o[65];
  /* asgn06.vhdl:15:8  */
  assign n22_o = s0 ? 1'b0 : n21_o;
  assign n23_o = {n22_o, n20_o, n17_o, n15_o, n12_o};
  /* asgn06.vhdl:14:5  */
  process (clk)
  begin
    if rising_edge (clk) then
      n26_q <= n23_o;
    end if;
  end process;
endmodule

