module tdp_ram
  (input  clk_a,
   input  read_a,
   input  write_a,
   input  byteen_a,
   input  [11:0] addr_a,
   input  [7:0] data_write_a,
   input  clk_b,
   input  read_b,
   input  write_b,
   input  [3:0] byteen_b,
   input  [9:0] addr_b,
   input  [31:0] data_write_b,
   output [7:0] data_read_a,
   output [31:0] data_read_b);
  wire [7:0] reg_a;
  wire [31:0] reg_b;
  wire n9_o;
  wire [11:0] n11_o;
  wire [11:0] n19_o;
  wire [7:0] n31_q;
  wire n39_o;
  wire n40_o;
  wire [11:0] n42_o;
  wire [7:0] n47_o;
  wire [11:0] n51_o;
  wire [7:0] n57_o;
  wire [7:0] n58_o;
  wire n59_o;
  wire n60_o;
  wire [11:0] n62_o;
  wire [7:0] n67_o;
  wire [11:0] n71_o;
  wire [7:0] n77_o;
  wire [7:0] n78_o;
  wire n79_o;
  wire n80_o;
  wire [11:0] n82_o;
  wire [7:0] n87_o;
  wire [11:0] n91_o;
  wire [7:0] n97_o;
  wire [7:0] n98_o;
  wire n99_o;
  wire n100_o;
  wire [11:0] n102_o;
  wire [7:0] n107_o;
  wire [11:0] n111_o;
  wire [7:0] n117_o;
  wire [7:0] n118_o;
  wire [31:0] n121_o;
  wire [31:0] n124_q;
  wire [31:0] n133_q;
  reg n138_data[7:0] ;
  reg n140_data[7:0] ;
  reg n142_data[7:0] ;
  reg n144_data[7:0] ;
  reg n146_data[7:0] ;
  assign data_read_a = n31_q;
  assign data_read_b = n124_q;
  /* tdp_ram.vhdl:65:12  */
  assign reg_a = n146_data; // (signal)
  /* tdp_ram.vhdl:66:12  */
  assign reg_b = n133_q; // (signal)
  /* tdp_ram.vhdl:79:34  */
  assign n9_o = write_a & byteen_a;
  /* tdp_ram.vhdl:80:55  */
  assign n11_o = {addr_a, 0'b};
  /* tdp_ram.vhdl:86:59  */
  assign n19_o = {addr_a, 0'b};
  /* tdp_ram.vhdl:77:9  */
  always @(posedge clk_a)
      n31_q <= reg_a;
  /* tdp_ram.vhdl:98:46  */
  assign n39_o = byteen_b[0];
  /* tdp_ram.vhdl:98:34  */
  assign n40_o = write_b & n39_o;
  /* tdp_ram.vhdl:99:55  */
  assign n42_o = {addr_b, 2'b00};
  /* tdp_ram.vhdl:100:37  */
  assign n47_o = data_write_b[7:0];
  /* tdp_ram.vhdl:105:59  */
  assign n51_o = {addr_b, 2'b00};
  assign n57_o = reg_b[7:0];
  /* tdp_ram.vhdl:103:17  */
  assign n58_o = read_b ? n138_data : n57_o;
  /* tdp_ram.vhdl:98:46  */
  assign n59_o = byteen_b[1];
  /* tdp_ram.vhdl:98:34  */
  assign n60_o = write_b & n59_o;
  /* tdp_ram.vhdl:99:55  */
  assign n62_o = {addr_b, 2'b01};
  /* tdp_ram.vhdl:100:37  */
  assign n67_o = data_write_b[15:8];
  /* tdp_ram.vhdl:105:59  */
  assign n71_o = {addr_b, 2'b01};
  assign n77_o = reg_b[15:8];
  /* tdp_ram.vhdl:103:17  */
  assign n78_o = read_b ? n140_data : n77_o;
  /* tdp_ram.vhdl:98:46  */
  assign n79_o = byteen_b[2];
  /* tdp_ram.vhdl:98:34  */
  assign n80_o = write_b & n79_o;
  /* tdp_ram.vhdl:99:55  */
  assign n82_o = {addr_b, 2'b10};
  /* tdp_ram.vhdl:100:37  */
  assign n87_o = data_write_b[23:16];
  /* tdp_ram.vhdl:105:59  */
  assign n91_o = {addr_b, 2'b10};
  assign n97_o = reg_b[23:16];
  /* tdp_ram.vhdl:103:17  */
  assign n98_o = read_b ? n142_data : n97_o;
  /* tdp_ram.vhdl:98:46  */
  assign n99_o = byteen_b[3];
  /* tdp_ram.vhdl:98:34  */
  assign n100_o = write_b & n99_o;
  /* tdp_ram.vhdl:99:55  */
  assign n102_o = {addr_b, 2'b11};
  /* tdp_ram.vhdl:100:37  */
  assign n107_o = data_write_b[31:24];
  /* tdp_ram.vhdl:105:59  */
  assign n111_o = {addr_b, 2'b11};
  assign n117_o = reg_b[31:24];
  /* tdp_ram.vhdl:103:17  */
  assign n118_o = read_b ? n144_data : n117_o;
  assign n121_o = {n118_o, n98_o, n78_o, n58_o};
  /* tdp_ram.vhdl:96:9  */
  always @(posedge clk_b)
      n124_q <= reg_b;
  /* tdp_ram.vhdl:96:9  */
  always @(posedge clk_b)
      n133_q <= n121_o;
  /* tdp_ram.vhdl:86:30  */
  reg [4095:0] n136_oport[7:0] ;
 (others => (others => '0'));
    always @(posedge clk_b)
      if (n40_o)
        n137_oport[n42_o] <= n47_o;
  assign n138_data = n136_oport[n51_o];
    always @(posedge clk_b)
      if (n60_o)
        n139_oport[n62_o] <= n67_o;
  assign n140_data = n136_oport[n71_o];
    always @(posedge clk_b)
      if (n80_o)
        n141_oport[n82_o] <= n87_o;
  assign n142_data = n136_oport[n91_o];
    always @(posedge clk_b)
      if (n100_o)
        n143_oport[n102_o] <= n107_o;
  assign n144_data = n136_oport[n111_o];
    always @(posedge clk_a)
      if (n9_o)
        n145_oport[n11_o] <= data_write_a;
    if posedge clk_a and (read_a = '1') then
      n146_data <= n136_oport(to_integer (unsigned (n19_o)));
    end if;
  /* tdp_ram.vhdl:99:27  */
  /* tdp_ram.vhdl:105:31  */
  /* tdp_ram.vhdl:99:27  */
  /* tdp_ram.vhdl:105:31  */
  /* tdp_ram.vhdl:99:27  */
  /* tdp_ram.vhdl:105:31  */
  /* tdp_ram.vhdl:99:27  */
  /* tdp_ram.vhdl:105:31  */
  /* tdp_ram.vhdl:80:27  */
  /* tdp_ram.vhdl:81:37  */
endmodule

