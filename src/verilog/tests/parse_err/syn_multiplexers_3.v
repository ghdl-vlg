module multiplexers_3
  (input  [7:0] di,
   input  [7:0] sel,
   output do);
  wire n1_o;
  wire n2_o;
  wire n3_o;
  wire n6_o;
  wire n7_o;
  wire n8_o;
  wire n9_o;
  wire n12_o;
  wire n13_o;
  wire n14_o;
  wire n15_o;
  wire n18_o;
  wire n19_o;
  wire n20_o;
  wire n21_o;
  wire n24_o;
  wire n25_o;
  wire n26_o;
  wire n27_o;
  wire n30_o;
  wire n31_o;
  wire n32_o;
  wire n33_o;
  wire n36_o;
  wire n37_o;
  wire n38_o;
  wire n39_o;
  wire n42_o;
  wire n43_o;
  wire n44_o;
  wire n45_o;
  wire n48_o;
  wire n49_o;
  wire n50_o;
  wire n51_o;
  wire n52_o;
  wire n53_o;
  wire n54_o;
  wire n55_o;
  assign do = n55_o;
  /* multiplexers_3.vhdl:13:13  */
  assign n1_o = di[0];
  /* multiplexers_3.vhdl:13:25  */
  assign n2_o = sel[0];
  /* multiplexers_3.vhdl:13:28  */
  assign n3_o = ~n2_o;
  /* multiplexers_3.vhdl:13:17  */
  assign n6_o = n3_o ? n1_o : 1'bz;
  /* multiplexers_3.vhdl:14:13  */
  assign n7_o = di[1];
  /* multiplexers_3.vhdl:14:25  */
  assign n8_o = sel[1];
  /* multiplexers_3.vhdl:14:28  */
  assign n9_o = ~n8_o;
  /* multiplexers_3.vhdl:14:17  */
  assign n12_o = n9_o ? n7_o : 1'bz;
  /* multiplexers_3.vhdl:15:13  */
  assign n13_o = di[2];
  /* multiplexers_3.vhdl:15:25  */
  assign n14_o = sel[2];
  /* multiplexers_3.vhdl:15:28  */
  assign n15_o = ~n14_o;
  /* multiplexers_3.vhdl:15:17  */
  assign n18_o = n15_o ? n13_o : 1'bz;
  /* multiplexers_3.vhdl:16:13  */
  assign n19_o = di[3];
  /* multiplexers_3.vhdl:16:25  */
  assign n20_o = sel[3];
  /* multiplexers_3.vhdl:16:28  */
  assign n21_o = ~n20_o;
  /* multiplexers_3.vhdl:16:17  */
  assign n24_o = n21_o ? n19_o : 1'bz;
  /* multiplexers_3.vhdl:17:13  */
  assign n25_o = di[4];
  /* multiplexers_3.vhdl:17:25  */
  assign n26_o = sel[4];
  /* multiplexers_3.vhdl:17:28  */
  assign n27_o = ~n26_o;
  /* multiplexers_3.vhdl:17:17  */
  assign n30_o = n27_o ? n25_o : 1'bz;
  /* multiplexers_3.vhdl:18:13  */
  assign n31_o = di[5];
  /* multiplexers_3.vhdl:18:25  */
  assign n32_o = sel[5];
  /* multiplexers_3.vhdl:18:28  */
  assign n33_o = ~n32_o;
  /* multiplexers_3.vhdl:18:17  */
  assign n36_o = n33_o ? n31_o : 1'bz;
  /* multiplexers_3.vhdl:19:13  */
  assign n37_o = di[6];
  /* multiplexers_3.vhdl:19:25  */
  assign n38_o = sel[6];
  /* multiplexers_3.vhdl:19:28  */
  assign n39_o = ~n38_o;
  /* multiplexers_3.vhdl:19:17  */
  assign n42_o = n39_o ? n37_o : 1'bz;
  /* multiplexers_3.vhdl:20:13  */
  assign n43_o = di[7];
  /* multiplexers_3.vhdl:20:25  */
  assign n44_o = sel[7];
  /* multiplexers_3.vhdl:20:28  */
  assign n45_o = ~n44_o;
  /* multiplexers_3.vhdl:20:17  */
  assign n48_o = n45_o ? n43_o : 1'bz;
  /* multiplexers_3.vhdl:13:17  */
  n49_o <= n6_o;
  n49_o <= n12_o;
  /* multiplexers_3.vhdl:13:17  */
  n50_o <= n49_o;
  n50_o <= n18_o;
  /* multiplexers_3.vhdl:13:17  */
  n51_o <= n50_o;
  n51_o <= n24_o;
  /* multiplexers_3.vhdl:13:17  */
  n52_o <= n51_o;
  n52_o <= n30_o;
  /* multiplexers_3.vhdl:13:17  */
  n53_o <= n52_o;
  n53_o <= n36_o;
  /* multiplexers_3.vhdl:13:17  */
  n54_o <= n53_o;
  n54_o <= n42_o;
  /* multiplexers_3.vhdl:13:17  */
  n55_o <= n54_o;
  n55_o <= n48_o;
endmodule

