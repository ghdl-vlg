module ent
  (input  [7:0] sgn,
   input  [7:0] uns,
   input  [30:0] nat,
   input  [31:0] int,
   output [31:0] mul_int_int,
   output [15:0] mul_uns_uns,
   output [15:0] mul_uns_nat,
   output [15:0] mul_nat_uns,
   output [15:0] mul_sgn_sgn,
   output [15:0] mul_sgn_int,
   output [15:0] mul_int_sgn,
   output [31:0] div_int_int,
   output [7:0] div_uns_uns,
   output [7:0] div_uns_nat,
   output [7:0] div_nat_uns,
   output [7:0] div_sgn_sgn,
   output [7:0] div_sgn_int,
   output [7:0] div_int_sgn,
   output [31:0] rem_int_int,
   output [7:0] rem_uns_uns,
   output [7:0] rem_uns_nat,
   output [7:0] rem_nat_uns,
   output [7:0] rem_sgn_sgn,
   output [7:0] rem_sgn_int,
   output [7:0] rem_int_sgn,
   output [31:0] mod_int_int,
   output [7:0] mod_uns_uns,
   output [7:0] mod_uns_nat,
   output [7:0] mod_nat_uns,
   output [7:0] mod_sgn_sgn,
   output [7:0] mod_sgn_int,
   output [7:0] mod_int_sgn);
  wire [31:0] n28_o;
  wire [15:0] n29_o;
  wire [15:0] n30_o;
  wire [15:0] n31_o;
  wire [15:0] n32_o;
  wire [15:0] n33_o;
  wire [15:0] n34_o;
  wire [15:0] n35_o;
  wire [15:0] n36_o;
  wire [15:0] n37_o;
  wire [15:0] n38_o;
  wire [15:0] n39_o;
  wire [15:0] n40_o;
  wire [15:0] n41_o;
  wire [15:0] n42_o;
  wire [15:0] n43_o;
  wire [15:0] n44_o;
  wire [15:0] n45_o;
  wire [15:0] n46_o;
  wire [31:0] n47_o;
  wire [7:0] n48_o;
  wire [30:0] n49_o;
  wire [30:0] n50_o;
  wire [7:0] n51_o;
  wire [30:0] n52_o;
  wire [30:0] n53_o;
  wire [7:0] n54_o;
  wire [7:0] n55_o;
  wire [31:0] n56_o;
  wire [31:0] n57_o;
  wire [7:0] n58_o;
  wire [31:0] n59_o;
  wire [31:0] n60_o;
  wire [7:0] n61_o;
  wire [31:0] n62_o;
  wire [7:0] n63_o;
  wire [30:0] n64_o;
  wire [30:0] n65_o;
  wire [7:0] n66_o;
  wire [30:0] n67_o;
  wire [30:0] n68_o;
  wire [7:0] n69_o;
  wire [7:0] n70_o;
  wire [31:0] n71_o;
  wire [31:0] n72_o;
  wire [7:0] n73_o;
  wire [31:0] n74_o;
  wire [31:0] n75_o;
  wire [7:0] n76_o;
  wire [31:0] n77_o;
  wire [7:0] n78_o;
  wire [30:0] n79_o;
  wire [30:0] n80_o;
  wire [7:0] n81_o;
  wire [30:0] n82_o;
  wire [30:0] n83_o;
  wire [7:0] n84_o;
  wire [7:0] n85_o;
  wire [31:0] n86_o;
  wire [31:0] n87_o;
  wire [7:0] n88_o;
  wire [31:0] n89_o;
  wire [31:0] n90_o;
  wire [7:0] n91_o;
  assign mul_int_int = n28_o;
  assign mul_uns_uns = n31_o;
  assign mul_uns_nat = n34_o;
  assign mul_nat_uns = n37_o;
  assign mul_sgn_sgn = n40_o;
  assign mul_sgn_int = n43_o;
  assign mul_int_sgn = n46_o;
  assign div_int_int = n47_o;
  assign div_uns_uns = n48_o;
  assign div_uns_nat = n51_o;
  assign div_nat_uns = n54_o;
  assign div_sgn_sgn = n55_o;
  assign div_sgn_int = n58_o;
  assign div_int_sgn = n61_o;
  assign rem_int_int = n62_o;
  assign rem_uns_uns = n63_o;
  assign rem_uns_nat = n66_o;
  assign rem_nat_uns = n69_o;
  assign rem_sgn_sgn = n70_o;
  assign rem_sgn_int = n73_o;
  assign rem_int_sgn = n76_o;
  assign mod_int_int = n77_o;
  assign mod_uns_uns = n78_o;
  assign mod_uns_nat = n81_o;
  assign mod_nat_uns = n84_o;
  assign mod_sgn_sgn = n85_o;
  assign mod_sgn_int = n88_o;
  assign mod_int_sgn = n91_o;
  /* ent.vhdl:48:27  */
  n28_o <= std_logic_vector (resize (signed (int) * signed (int), 32));
  /* ent.vhdl:49:27  */
  assign n29_o = {8'b0, uns};  //  uext
  /* ent.vhdl:49:27  */
  assign n30_o = {8'b0, uns};  //  uext
  /* ent.vhdl:49:27  */
  n31_o <= std_logic_vector (resize (signed (n29_o) * signed (n30_o), 16));
  /* ent.vhdl:50:27  */
  assign n32_o = {8'b0, uns};  //  uext
  /* ent.vhdl:50:27  */
  n33_o <= nat (15 downto 0);  --  trunc
  /* ent.vhdl:50:27  */
  n34_o <= std_logic_vector (resize (signed (n32_o) * signed (n33_o), 16));
  /* ent.vhdl:51:27  */
  n35_o <= nat (15 downto 0);  --  trunc
  /* ent.vhdl:51:27  */
  assign n36_o = {8'b0, uns};  //  uext
  /* ent.vhdl:51:27  */
  n37_o <= std_logic_vector (resize (signed (n35_o) * signed (n36_o), 16));
  /* ent.vhdl:52:27  */
  n38_o <= std_logic_vector (resize (signed (sgn), 16));  --  sext
  /* ent.vhdl:52:27  */
  n39_o <= std_logic_vector (resize (signed (sgn), 16));  --  sext
  /* ent.vhdl:52:27  */
  n40_o <= std_logic_vector (resize (signed (n38_o) * signed (n39_o), 16));
  /* ent.vhdl:53:27  */
  n41_o <= std_logic_vector (resize (signed (sgn), 16));  --  sext
  /* ent.vhdl:53:27  */
  n42_o <= int (15 downto 0);  --  trunc
  /* ent.vhdl:53:27  */
  n43_o <= std_logic_vector (resize (signed (n41_o) * signed (n42_o), 16));
  /* ent.vhdl:54:27  */
  n44_o <= int (15 downto 0);  --  trunc
  /* ent.vhdl:54:27  */
  n45_o <= std_logic_vector (resize (signed (sgn), 16));  --  sext
  /* ent.vhdl:54:27  */
  n46_o <= std_logic_vector (resize (signed (n44_o) * signed (n45_o), 16));
  /* ent.vhdl:56:27  */
  n47_o <= std_logic_vector (signed (int) / signed (int));
  /* ent.vhdl:57:27  */
  n48_o <= std_logic_vector (unsigned (uns) / unsigned (uns));
  /* ent.vhdl:58:27  */
  assign n49_o = {23'b0, uns};  //  uext
  /* ent.vhdl:58:27  */
  n50_o <= std_logic_vector (unsigned (n49_o) / unsigned (nat));
  /* ent.vhdl:58:27  */
  n51_o <= n50_o (7 downto 0);  --  trunc
  /* ent.vhdl:59:27  */
  assign n52_o = {23'b0, uns};  //  uext
  /* ent.vhdl:59:27  */
  n53_o <= std_logic_vector (unsigned (nat) / unsigned (n52_o));
  /* ent.vhdl:59:27  */
  n54_o <= n53_o (7 downto 0);  --  trunc
  /* ent.vhdl:60:27  */
  n55_o <= std_logic_vector (signed (sgn) / signed (sgn));
  /* ent.vhdl:61:27  */
  n56_o <= std_logic_vector (resize (signed (sgn), 32));  --  sext
  /* ent.vhdl:61:27  */
  n57_o <= std_logic_vector (signed (n56_o) / signed (int));
  /* ent.vhdl:61:27  */
  n58_o <= n57_o (7 downto 0);  --  trunc
  /* ent.vhdl:62:27  */
  n59_o <= std_logic_vector (resize (signed (sgn), 32));  --  sext
  /* ent.vhdl:62:27  */
  n60_o <= std_logic_vector (signed (int) / signed (n59_o));
  /* ent.vhdl:62:27  */
  n61_o <= n60_o (7 downto 0);  --  trunc
  /* ent.vhdl:64:27  */
  n62_o <= std_logic_vector (signed (int) rem signed (int));
  /* ent.vhdl:65:27  */
  n63_o <= std_logic_vector (unsigned (uns) mod unsigned (uns));
  /* ent.vhdl:66:27  */
  assign n64_o = {23'b0, uns};  //  uext
  /* ent.vhdl:66:27  */
  n65_o <= std_logic_vector (unsigned (n64_o) mod unsigned (nat));
  /* ent.vhdl:66:27  */
  n66_o <= n65_o (7 downto 0);  --  trunc
  /* ent.vhdl:67:27  */
  assign n67_o = {23'b0, uns};  //  uext
  /* ent.vhdl:67:27  */
  n68_o <= std_logic_vector (unsigned (nat) mod unsigned (n67_o));
  /* ent.vhdl:67:27  */
  n69_o <= n68_o (7 downto 0);  --  trunc
  /* ent.vhdl:68:27  */
  n70_o <= std_logic_vector (signed (sgn) rem signed (sgn));
  /* ent.vhdl:69:27  */
  n71_o <= std_logic_vector (resize (signed (sgn), 32));  --  sext
  /* ent.vhdl:69:27  */
  n72_o <= std_logic_vector (signed (n71_o) rem signed (int));
  /* ent.vhdl:69:27  */
  n73_o <= n72_o (7 downto 0);  --  trunc
  /* ent.vhdl:70:27  */
  n74_o <= std_logic_vector (resize (signed (sgn), 32));  --  sext
  /* ent.vhdl:70:27  */
  n75_o <= std_logic_vector (signed (int) rem signed (n74_o));
  /* ent.vhdl:70:27  */
  n76_o <= n75_o (7 downto 0);  --  trunc
  /* ent.vhdl:72:27  */
  n77_o <= std_logic_vector (signed (int) mod signed (int));
  /* ent.vhdl:73:27  */
  n78_o <= std_logic_vector (unsigned (uns) mod unsigned (uns));
  /* ent.vhdl:74:27  */
  assign n79_o = {23'b0, uns};  //  uext
  /* ent.vhdl:74:27  */
  n80_o <= std_logic_vector (unsigned (n79_o) mod unsigned (nat));
  /* ent.vhdl:74:27  */
  n81_o <= n80_o (7 downto 0);  --  trunc
  /* ent.vhdl:75:27  */
  assign n82_o = {23'b0, uns};  //  uext
  /* ent.vhdl:75:27  */
  n83_o <= std_logic_vector (unsigned (nat) mod unsigned (n82_o));
  /* ent.vhdl:75:27  */
  n84_o <= n83_o (7 downto 0);  --  trunc
  /* ent.vhdl:76:27  */
  n85_o <= std_logic_vector (signed (sgn) mod signed (sgn));
  /* ent.vhdl:77:27  */
  n86_o <= std_logic_vector (resize (signed (sgn), 32));  --  sext
  /* ent.vhdl:77:27  */
  n87_o <= std_logic_vector (signed (n86_o) mod signed (int));
  /* ent.vhdl:77:27  */
  n88_o <= n87_o (7 downto 0);  --  trunc
  /* ent.vhdl:78:27  */
  n89_o <= std_logic_vector (resize (signed (sgn), 32));  --  sext
  /* ent.vhdl:78:27  */
  n90_o <= std_logic_vector (signed (int) mod signed (n89_o));
  /* ent.vhdl:78:27  */
  n91_o <= n90_o (7 downto 0);  --  trunc
endmodule

