module arr01
  (input  [31:0] a,
   input  [1:0] sel,
   input  clk,
   output [7:0] res);
  wire [33:0] s;
  wire [7:0] n4_o;
  wire [7:0] n5_o;
  wire [7:0] n6_o;
  wire [7:0] n7_o;
  wire [31:0] n8_o;
  wire [33:0] n9_o;
  wire [33:0] n12_q;
  wire [1:0] n16_o;
  wire [1:0] n18_o;
  wire [31:0] n20_o;
  wire [7:0] n24_q;
  wire [7:0] n25_o;
  wire [7:0] n26_o;
  wire [7:0] n27_o;
  wire [7:0] n28_o;
  wire [1:0] n29_o;
  wire [7:0] n30_o;
  assign res = n24_q;
  /* arr01.vhdl:19:10  */
  assign s = n12_q; // (signal)
  /* arr01.vhdl:25:19  */
  assign n4_o = a[31:24];
  /* arr01.vhdl:26:19  */
  assign n5_o = a[23:16];
  /* arr01.vhdl:27:19  */
  assign n6_o = a[15:8];
  /* arr01.vhdl:28:19  */
  assign n7_o = a[7:0];
  assign n8_o = {n4_o, n5_o, n6_o, n7_o};
  assign n9_o = {n8_o, sel};
  /* arr01.vhdl:23:5  */
  always @(posedge clk)
    n12_q <= n9_o;
  /* arr01.vhdl:35:23  */
  assign n16_o = s[1:0];
  /* arr01.vhdl:35:20  */
  assign n18_o = 2'b11 - n16_o;
  assign n20_o = s[33:2];
  /* arr01.vhdl:34:5  */
  always @(posedge clk)
    n24_q <= n30_o;
  /* arr01.vhdl:9:5  */
  assign n25_o = n20_o[7:0];
  assign n26_o = n20_o[15:8];
  /* arr01.vhdl:32:3  */
  assign n27_o = n20_o[23:16];
  assign n28_o = n20_o[31:24];
  /* arr01.vhdl:35:20  */
  assign n29_o = n18_o[1:0];
  /* arr01.vhdl:35:20  */
  always @*
    case (n29_o)
      2'b00: n30_o <= n25_o;
      2'b01: n30_o <= n26_o;
      2'b10: n30_o <= n27_o;
      2'b11: n30_o <= n28_o;
    endcase
endmodule

