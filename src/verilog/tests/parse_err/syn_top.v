module top
  (input  clk,
   input  di,
   output do);
  wire data;
  wire n5_o;
  wire n7_q;
  wire n8_o;
  assign do = n8_o;
  /* top.vhdl:13:12  */
  assign data = n5_o; // (signal)
  /* top.vhdl:22:17  */
  assign n5_o = ~n7_q;
  /* top.vhdl:19:9  */
  always @(posedge clk)
    n7_q <= di;
  /* top.vhdl:25:11  */
  assign n8_o = ~data;
endmodule

