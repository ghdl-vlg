module issue
  (input  foo);
  wire n4_o;
  wire n6_o;
  wire n7_o;
  wire n9_o;
  wire n10_o;
  /* issue.vhdl:11:21  */
  assign n4_o = foo ^ 1'b1;
  /* issue.vhdl:11:37  */
  assign n6_o = foo & 1'b0;
  /* issue.vhdl:11:30  */
  assign n7_o = n4_o == n6_o;
  /* issue.vhdl:11:9  */
  assign n9_o = ~1'b0;
  /* issue.vhdl:11:9  */
  assign n10_o = n9_o | n7_o;
  /* issue.vhdl:11:9  */
  n11: assert (n10_o); // assert
endmodule

