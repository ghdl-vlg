module task1();
   task inc(inout reg [3:0] a);
      a = a + 1;
   endtask

   reg [3:0] v;
   initial begin
      v = 0;
      inc(v);
      if (v != 1)
	begin
	   $display("FAIL-1");
	   $finish;
	end
      $display("PASS");
      $finish;
   end
endmodule
