module inst4;
   reg  [3:0] a;

   copy inst1 (|a, r1);

   initial begin
      a = 'b0001;
      #1 if (r1 != 1) begin
	 $display ("FAIL-1 %b", r1);
	 $finish;
      end
      a = 'b0000;
      #1 if (r1 != 0) begin
	 $display ("FAIL-2 %b", r1);
	 $finish;
      end
      a = 'b1101;
      #1 if (r1 != 1) begin
	 $display ("FAIL-3");
	 $finish;
      end
      $display ("PASS");
   end
endmodule

module copy (a, o);
   input a;
   output o;
   wire   o;

   assign o = a;
endmodule // copy
