module portdecl3a (output reg a);


   initial
        a <= 1;
endmodule // portdecl3a

module portdecl3 ();
   wire a1;

   portdecl3a inst (a1);
   initial
     # 1 if (a1 == 1)
       $display("PASS");
     else
       $display("FAIL");

endmodule // portdecl3
