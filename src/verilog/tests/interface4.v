interface MSBus (input Clk);
  logic [7:0] Addr, Data;
  logic RWn;
  modport Slave (input Addr, inout Data);
endinterface

module TestRAM;
  logic Clk;
  MSBus TheBus(.Clk(Clk));
  RAM TheRAM (.MemBus(TheBus.Slave));
  ...
endmodule

module RAM (MSBus.Slave MemBus);
  // MemBus.Addr is an input of RAM
endmodule
