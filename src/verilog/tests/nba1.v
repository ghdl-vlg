module nba1;
   reg a;
   initial
     begin
	a = 0;
	a <= 1;
	if (a != 0) begin
	   $display ("FAIL");
	   $finish;
	end
	#2 ;
	if (a != 1) begin
	   $display ("FAIL");
	   $finish;
	end
	$display ("PASS");
     end
endmodule
