module sconn1(input res,
              output [1:0] out);
  assign out = {res, 1'b1};
endmodule

module conn1;
  wire [3:0] tres;
  wire [1:0] tout;

  sconn1 inst (.res(tres[0]), .out(tout));

  assign tres = 2'b01;

  initial begin
    # 1;

    if (tout == 2'b11)
      $display("PASS");
    else
      $display("FAIL");
  end
endmodule 
