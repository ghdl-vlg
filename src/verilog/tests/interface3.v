interface Channel #(parameter N = 0)
    (input bit Clock, bit Ack, bit Sig);
  bit Buff[N-1:0];
  initial
    for (int i = 0; i < N; i++)
      Buff[i] = 0;
  always @ (posedge Clock)        
   if(Ack == 1)
     Sig = Buff[N-1];
   else
     Sig = 0;
endinterface

// Using the interface
module Top;
  bit Clock, Ack, Sig;
  // Instance the interface. The parameter N is set to 7using named
  // connection while the ports are connected using implicit connection
  Channel #(.N(7)) TheCh (.*);
  TX TheTx (.Ch(TheCh));
  //...
endmodule
