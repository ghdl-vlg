module sense1;
   reg [7:0] byt;
   wire [3:0] uh;
   wire       b0;

   assign uh = byt[3:0];
   assign b0 = byt[0];

   var int    cnt_uh = 0;
   var int    cnt_b0 = 0;

   always @(uh)
     cnt_uh++;

   always @(b0)
     cnt_b0++;


   initial begin
      byt = 0; // uh, b0
      # 1;
      byt = 4; // uh
      # 1;
      byt = 20; // -
      # 1;
      byt = 3; // uh, b0
      # 1;

      if (cnt_uh != 3)
        $fatal(0, "FAIL-uh");
      if (cnt_b0 != 2)
        $fatal(0, "FAIL-b0");
      $display("PASS");
      $finish;
   end
endmodule // sense1
