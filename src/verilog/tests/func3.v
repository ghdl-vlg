module func3();

   function automatic integer sum (input integer operand);
      if (operand >= 2)
	sum = sum (operand - 1) + operand;
      else
	sum = 1;
   endfunction: sum

   initial begin
      if (sum(7) != 28)
	begin
	   $display("FAIL-1");
	   $finish;
	end
      $display("PASS");
      $finish;
   end
endmodule
