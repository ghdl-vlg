module shift1;
   reg [1:0] resff;
   reg       res = 1;
   reg 	     clk = 0;
   integer   i;

   initial begin
      if (resff !== 2'bxx) begin
	 $display("FAIL");
	 $finish;
      end
      resff = resff<<1 | res;
      if (resff !== 2'bx1) begin
	 $display("FAIL");
	 $finish;
      end
      resff = resff<<1 | res;
      if (resff !== 2'b11) begin
	 $display("FAIL");
	 $finish;
      end
      $display("PASS");
   end
endmodule
