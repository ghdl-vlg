module array2();
   reg a [7:0] = '{0,0,0,0,0,0,0,1};

   initial begin
      if (a[0] != 1)
	begin
	   $display("FAIL 0");
	   $finish;
	end
      if (a[1] != 0)
	begin
	   $display("FAIL 1");
	   $finish;
	end
      if (a[7] != 0)
	begin
	   $display("FAIL 7");
	   $finish;
	end
      if (a[10] !== 1'bx)
	begin
	   $display("FAIL 10");
	   $finish;
	end
      $display("PASS");
   end
endmodule
