// CO
// Example
class Packet;
  integer command;
  function new();
    command = 1;
  endfunction
endclass

class LinkedPacket extends Packet;
  LinkedPacket next;
  function LinkedPacket get_next();
    get_next = next;
  endfunction
endclass
