// CO
// Test with extension given through a parameter
package pkg;
  class E#(type C) extends C;
    function integer get();
      return val;
    endfunction
  endclass

  class base;
    integer val;
  endclass

  typedef E#(base) my_e;
endpackage
