// NE
typedef class base;

class ext extends base;
  function int get();
     return val;
  endfunction // get
endclass // ext

class base;
  int val;
endclass // base

module m;
   ext e;
   initial begin
      e = new;
      e.val = 7;
      if (e.get() !== 7)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule // m
