// NE
// Test with extension given through a parameter
class E#(type C) extends C;
  function integer get();
    return val;
  endfunction
endclass

class base;
  integer val;
endclass

typedef E#(base) my_e;

module m;
   my_e c1;
   initial begin
      c1 = new;
      c1.val = 5;
      $display(c1.val);

      if (c1.get() !== 5)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
