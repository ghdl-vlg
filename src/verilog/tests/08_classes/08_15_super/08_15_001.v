// NE
// Example 1
class Packet;      // base class
   integer value = 2;
   function integer delay();
      delay = value * value;
   endfunction
endclass

class LinkedPacket extends Packet;// derived class
   integer value = 3;
   function integer delay();
      delay = super.delay() + value * super.value;
   endfunction
endclass // LinkedPacket

module m1;
   LinkedPacket p;
   initial begin
      p = new;
      if (p.delay() !== 10)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // m1
