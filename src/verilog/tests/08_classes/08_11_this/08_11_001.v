// NE

class C1;
   int v;

   function void copy(C1 src);
      v = src.v;
   endfunction // copy

   function C1 clone();
      C1 res;
      res = new;
      res.copy(this);
      return res;
   endfunction
endclass

module m;
   C1 c, d;
   initial begin
      c = new;
      c.v = 4;
      d = c.clone();
      if (d.v !== 4)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule
