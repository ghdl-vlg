// NE

class Packet ;
   typedef enum { ERR_OVERFLOW = 10, ERR_UNDERFLOW = 1123} PCKT_TYPE;
endclass

module t1;
   initial begin
      Packet p = new;
      if (p.ERR_OVERFLOW != 10)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
