// NE

class vector #(parameter width = 7, type T = int);
endclass

module t1;
   vector #(3) v = new;
//   initial $display (vector #(3)::T'(3.45));
//   initial $display ((v.T)'(3.45));

   initial  begin
      if (v.width != 3)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
