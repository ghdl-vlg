// NE

class Packet ;
   int v;
endclass

module t1;
   initial begin
      Packet p1, p2, p3;
      if (p1 != null)
	$fatal(0, "FAIL-1");
      if (p1 !== null)
	$fatal(0, "FAIL-2");
      p1 = new;
      p1.v = 3;
      if (p1 == null)
	$fatal(0, "FAIL-3");
      if (p1 === null)
	$fatal(0, "FAIL-4");
      p2 = new;
      p2.v = 2;
      p3 = p1;
      p1 = null;
      if (p3.v !== 3)
	$fatal(0, "FAIL-5");
      $display("PASS");
   end
endmodule
