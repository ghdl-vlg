// NE

class Packet ;
   integer buffer_size;
endclass

module t1;
   initial begin
      Packet p;
      if (null != p)
	$fatal(0, "FAIL-1");
      p = new;
      if (null == p)
	$fatal(0, "FAIL-2");
      $display("PASS");
   end
endmodule
