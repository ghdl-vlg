// NE

class Packet ;
   integer buffer_size;
endclass

module t1;
   initial begin
      Packet p;
      p = new;
      if (p.buffer_size != 0)
	$fatal(0, "FAIL-1");
      p.buffer_size = 5;
      if (p.buffer_size != 5)
	$fatal(0, "FAIL-2");
      $display("PASS");
   end
endmodule
