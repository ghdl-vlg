// NE

class Packet ;
   integer buffer_size;
endclass

module t1;
   initial begin
      Packet p;
      if (p != null)
	$fatal(0, "FAIL-1");
      p = new;
      if (p == null)
	$fatal(0, "FAIL-2");
      $display("PASS");
   end
endmodule
