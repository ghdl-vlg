// NE

class base;
   int v;
endclass // base

class ext extends base;
endclass

module m1;
   initial begin
      base b1, b2;
      ext e1;
      b2 = new;
      b2.v = 2;
      e1 = new;
      e1.v = 3;
      b1 = b1 == null ? e1 : b2;
      if (b1.v !== 3)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
