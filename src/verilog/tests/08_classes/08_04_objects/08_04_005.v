// NE

class base;
   int v;
endclass // base

class ext extends base;
endclass

module t1;
   initial begin
      base b1;
      ext e1;
      //  Allowed
      b1 = e1;
      $display("PASS");
   end
endmodule
