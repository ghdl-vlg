// NE

class Packet ;
   int v;
endclass

module t1;
   initial begin
      Packet p1, p2, p3;
      p3 = new;
      p3.v = 3;
      p2 = new;
      p2.v = 2;
      p1 = p1 == null ? p2 : p3;
      if (p1.v !== 2)
	$fatal(0, "FAIL-6");
      $display("PASS");
   end
endmodule
