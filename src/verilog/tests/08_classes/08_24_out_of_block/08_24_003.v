// NE

class C;
   extern function int f();
   int v;
endclass

typedef int myint;

function int C::f();
   myint r = v + 1;
   return r;
endfunction

module m;
   C c;
   initial begin
      c = new;
      c.v = 4;
      if (c.f() !== 5)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule
