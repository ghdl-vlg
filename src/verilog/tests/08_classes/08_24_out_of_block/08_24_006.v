// NE

class C2;
   static int v;
endclass

class C1;
   extern function int f();
   typedef C2 myclass;
endclass

function int C1::f();
   myclass::v = 7;
   return 4;
endfunction

module m;
   C1 c;
   initial begin
      c = new;
      if (c.f() !== 4)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule
