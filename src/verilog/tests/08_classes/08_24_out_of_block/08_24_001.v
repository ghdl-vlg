// NE
//  full example 2

typedef real T;

class C;
   typedef int T;
   extern function T f();
   extern function real f2();
endclass

function C::T C::f();
   // the return must use the class scope resolution
   // operator, since the type is defined within the
   // class
   return 1;
endfunction

function real C::f2();
   return 1.0;
endfunction

module m;
   C c;
   initial begin
      c = new;
      if (c.f() !== 1)
	$fatal(0, "FAIL-1");
      if (c.f2() !== 1.0)
	$fatal(0, "FAIL-2");
      $display("PASS");
   end
endmodule
