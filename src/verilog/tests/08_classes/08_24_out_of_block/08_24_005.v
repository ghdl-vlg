// NE

class C;
   extern function int f();
endclass

function int C::f();
   f = 7;
endfunction

module m;
   C c;
   initial begin
      c = new;
      if (c.f() !== 7)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule
