// NE

class C1;
   int v;
   extern function void copy(C1 src = null);
endclass

function void C1::copy(C1 src = null);
   v = src.v;
endfunction

module m;
   C1 c, d;
   initial begin
      c = new;
      c.v = 4;
      d = new;
      d.copy(c);
      if (d.v !== 4)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule
