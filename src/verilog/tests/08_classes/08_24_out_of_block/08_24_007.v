// NE

class C1;
   extern function int f();
   function int g();
      return 3;
   endfunction
endclass

function int C1::f();
   return g() + 1;
endfunction

module m;
   C1 c;
   initial begin
      c = new;
      if (c.f() !== 4)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule
