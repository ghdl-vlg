// NE
// Example

class id;
   static int current = 0;
   static function int next_id();
      next_id = ++current; // OK to access static class property
   endfunction
endclass // id

module t1;
   initial begin
      if (id::next_id() !== 1)
        $fatal(0, "FAIL-1");
      if (id::next_id() !== 2)
        $fatal(0, "FAIL-2");
      $display("PASS");
   end
endmodule
