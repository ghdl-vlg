// CE
class c1;
   local int i = 5;
   function int get_i();
      return i;
   endfunction // get_i
endclass // c1

module m1;
   initial begin
      c1 c;
      c = new;
      if (c.i !== 5)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // m1

