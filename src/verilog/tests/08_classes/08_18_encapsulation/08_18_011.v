// NE
class c0;
   protected int i = 5;
endclass // c1

class c1 extends c0;
   function int get_i();
      return this.i;
   endfunction // get_i
endclass // c1

module m1;
   initial begin
      c1 c;
      c = new;
      if (c.get_i() !== 5)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // m1

