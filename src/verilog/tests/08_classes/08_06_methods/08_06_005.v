// NE

class base;
   int val;
   task set_val(int v);
      val = v;
   endtask

   function base copy();
      copy = new;
      copy.val = this.val;
      return copy;
   endfunction

   function int get_val();
      return val;
   endfunction
endclass

module t;
   base b, c;
   initial begin
      b = new;
      b.set_val(5);
      c = b.copy();
      if (b.get_val() !== 5)
        $fatal(0, "FAIL-1");
      if (c.get_val() !== 5)
        $fatal(0, "FAIL-2");
      $display("PASS");
   end
endmodule // t

      
