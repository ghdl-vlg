// NE

class base;
   int def = 3;
   
   int val;
   task set_val(int v = def);
      val = v;
   endtask

   function int get_val();
      //  No return.
      get_val = val;
   endfunction

   function int get_val2();
      return get_val();
   endfunction
endclass

module t;
   base b;
   initial begin
      b = new;
      b.set_val();
      if (b.get_val() !== 3)
        $fatal(0, "FAIL-1");
      if (b.get_val2() !== 3)
        $fatal(0, "FAIL-2");
      $display("PASS");
   end
endmodule // t

      
