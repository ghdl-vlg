// NE

class base;
   int val;
   task set_val(int v);
      val = v;
   endtask

   function int get_val();
      return val;
   endfunction
endclass

module t;
   base b;
   initial begin
      b = new;
      b.set_val(5);
      if (b.get_val() !== 5)
        $fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // t

      
