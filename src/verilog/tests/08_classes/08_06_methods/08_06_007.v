// NE
//  Methods have visibility over all the properties (according to UVM).
class c1;
   function int get();
      return v;
   endfunction // get
   int v = 4;
endclass // c1

module mod1;
   initial begin
      c1 v = new;
      if (v.get !== 4)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // mod1
