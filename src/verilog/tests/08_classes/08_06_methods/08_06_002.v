// NE

class base;
   int def = 3;

   int val;
   task set_val(int v = def);
      val = v;
   endtask

   function int get_val();
      return val;
   endfunction
endclass

module t;
   base b;
   initial begin
      b = new;
      b.set_val();
      if (b.get_val() !== 3)
        $fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // t
