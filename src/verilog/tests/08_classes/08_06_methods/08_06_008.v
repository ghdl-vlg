// CO
class c1;
   function int get();
      return 4;
   endfunction // get
endclass // c1

module mod1;
   initial begin
      c1 v = new;
      if (c1.get !== 4)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // mod1
