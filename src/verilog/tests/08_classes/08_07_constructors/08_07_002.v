// NE
class C;
   int c1 = 1;
   int c2 = 1;
   int c3 = 1;
   function new(int a);
      c2 = 2;
      c3 = a;
   endfunction
endclass

module t1;
   initial begin
      C c = new(3);

      if (c.c1 !== 1)
        $fatal(0, "FAIL-1");
      if (c.c2 !== 2)
        $fatal(0, "FAIL-2");
      if (c.c3 !== 3)
        $fatal(0, "FAIL-3");

      $display("PASS");
   end
endmodule
