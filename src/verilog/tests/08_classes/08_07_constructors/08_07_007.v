// NE
module m1;
  class c;
     int n;

     function new(int v = 1);
	n += v;
     endfunction
  endclass

  class c1 extends c;
     function new;
	super.new(3);
	n += 1;
     endfunction
  endclass

  initial begin
     c1 i1 = new;
     $display("n=%x", i1.n);
     if (i1.n !== 4)
      $fatal(0, "FAIL-1");
    $display("PASS");
  end
endmodule
