// NE
module m1;
  class c;
     string vs;

     function new(string s = "he");
	vs = s;
     endfunction
  endclass

  class c1 extends c;
  endclass

  initial begin
     c1 i1 = new;
     if (i1.vs !== "he")
      $fatal(0, "FAIL-1");
    $display("PASS");
  end
endmodule
