// NE
class Packet;
   integer command;
   function new();
      // $display("new");
      command = 7;
   endfunction
endclass

module t;
   initial begin
      Packet p = new;
      //  $display(p.command);
      if (p.command !== 7)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
