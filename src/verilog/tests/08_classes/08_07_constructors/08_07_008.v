// NE
module m1;
  class c;
     int n;

     function new(int v = 3);
	n += v;
     endfunction
  endclass

  class c1 extends c;
     function new(int v = 2);
	n += v;
     endfunction
  endclass

  class c2 extends c1;
     function new;
	super.new(4);
	n += 1;
     endfunction
  endclass

  initial begin
     c2 i2 = new;
     // $display("n=%x", i2.n);
     if (i2.n !== 8)
      $fatal(0, "FAIL-1");
    $display("PASS");
  end
endmodule
