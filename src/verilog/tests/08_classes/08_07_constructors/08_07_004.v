// NE
class C;
   int c1 = 1;
   int c3 = 1;
   function new(int a);
      c3 = a;
   endfunction // new
endclass

class C2;
   int v = 5;
   function C create(int b);
      return new (v + b);
   endfunction
endclass

module t1;
   initial begin
      C2 d = new;
      C c = d.create (4);

      if (c.c3 !== 9)
        $fatal(0, "FAIL");

      $display("PASS");
   end
endmodule
