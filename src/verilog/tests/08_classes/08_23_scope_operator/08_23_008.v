// NE
class base;
   static string str;
endclass // base

class c1 extends base;
   static int i;
endclass

module top;
   initial begin
      c1::i = 1;
      base::str = "he";
      if (c1::str != "he")
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
