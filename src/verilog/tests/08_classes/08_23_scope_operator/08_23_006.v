// NE

class c1;
   static int v = 3;
endclass // c1

typedef c1 c2;
typedef c2 c3;

module m;
   initial begin
      if (c2::v !== 3)
	$fatal(0, "FAIL-1");
      if (c3::v !== 3)
	$fatal(0, "FAIL-2");
      $display("PASS");
   end
endmodule
