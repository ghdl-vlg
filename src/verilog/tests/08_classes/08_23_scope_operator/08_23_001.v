// NE
// scope operator with a type parameter name.
class c1 #(type T = int);
   function int f (int a);
      var int res;
      res = T::g(a);
      return res;
   endfunction // f
endclass // c1

class c2;
   static function int g (int a);
      return a + 3;
   endfunction // g
endclass // c2

module m;
   c1 #(.T(c2)) v;
   initial begin
      v = new;
      if (v.f(2) !== 5)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // m
