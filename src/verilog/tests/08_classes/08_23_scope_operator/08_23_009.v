// NE

class c1;
   function int get1();
      return 1;
   endfunction // get1
endclass // c1

class c2;
   static c1 v;
   
   static function c1 get_c();
      if (v == null)
	v = new;
      return v;
   endfunction // get_c
endclass // c2

module m1;
   initial begin
      if (c2::get_c().get1() !== 1)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // m1
