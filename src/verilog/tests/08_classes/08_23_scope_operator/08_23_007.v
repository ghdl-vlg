// NE

class c1 #(type T = int);
   typedef T TT;
   static function int get();
      return TT::get();
   endfunction
endclass // c1

class c2;
   static function int get();
      return 4;
   endfunction
endclass

typedef c1#(c2) c3;

module m;
   initial begin
      if (c3::get() !== 4)
	$fatal(0, "FAIL-1");
      $display("PASS");
   end
endmodule
