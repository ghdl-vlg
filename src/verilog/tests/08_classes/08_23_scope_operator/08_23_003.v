// NE
class c1 #(type T = int, type R = int);
   function R f (int a);
      var R res;
      res = T::g(a);
      return res;
   endfunction // f
endclass // c1

class c2 #(type R = int);
   static function R g (int a);
      return a + 1;
   endfunction // g
endclass // c2

module m;
   c1 #(.T(c2#(int)), .R(int)) v;
   initial begin
      v = new;
      if (v.f(2) !== 3)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // m
