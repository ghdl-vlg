// NE

class c1;
   static int v = 3;
endclass // c1

module m;
   initial begin
      if (c1::v !== 3)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
