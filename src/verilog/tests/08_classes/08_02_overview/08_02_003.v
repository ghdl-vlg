// NE
// Example 1

module t1;
  class Packet ;
     const integer buffer_size = 100;
     function int get_val();
        return buffer_size;
     endfunction
  endclass

   initial begin
      Packet p = new;
      if (p.get_val() !== 100)
        $fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
