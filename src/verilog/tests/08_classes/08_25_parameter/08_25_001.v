// NE
class c1 #(type T = int);
   static function int v();
      return 3;
   endfunction
endclass // c1

class c2 #(type T = int);

   function int get3();
      return c1#(T)::v();
   endfunction // get3
endclass // c2

module m1;
   c2#(bit) e2;
   typedef c1#(bit) c1_type;
   c1_type c1;

   initial begin
      int r;

      e2 = new;
      r = e2.get3();
      $display("%d", r);
      if (e2.get3() !== 3)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // m1
