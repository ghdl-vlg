`define my_line `__LINE__
`define get_line `my_line

module macro_nested1;
   initial begin
     integer l = `get_line;

     if (l == 6)
       $display ("PASS");
     else
       $display ("FAIL");
   end
endmodule
