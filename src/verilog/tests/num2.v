module num2;
   reg [63:0] a;
   
   initial
     begin
	a = 'h41;
	if (a != 65)
	  begin
	     $display("FAIL 1");
	     $finish;
	  end

	a = 64'habcd12348765fe90;
	# 1;
	
	if (a != 64'd12379570967530569360)
	  begin
	     $display("FAIL 2");
	     $finish;
	  end

	if ((a >> 32) != 32'habcd1234)
	  begin
	     $display("FAIL 3");
	     $finish;
	  end

/*	if (a != 'o1257150443220731377220)
	  begin
	     $display("FAIL 4");
	     $finish;
	  end
*/
	if (a != 64'b1010101111001101000100100011010010000111011001011111111010010000)
	  begin
	     $display("FAIL 5");
	     $finish;
	  end

	$display ("PASS");
     end
endmodule
