module same_port (.a(i), .b(i));
// Name 'i' is declared inside the module as an inout port.
// Names 'a' and 'b' are defined for port connections.
endmodule
