`define debug

module t;
   reg a;

   always @(a)
     begin
	a = 
	`debug
	    1;
     end
   initial
     $display("PASS");
endmodule // t

