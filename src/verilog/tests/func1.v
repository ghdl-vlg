module func1();
   function [3:0] add(reg [3:0] a, b);
      return a + b;
   endfunction // add

   initial begin
      if (add(1, 3) != 4)
	begin
	   $display("FAIL-1");
	   $finish;
	end
      $display("PASS");
      $finish;
   end
endmodule // func1
