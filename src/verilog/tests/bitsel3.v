module bitsel3;
   reg [0:3] r;
   reg 	     v;

   initial begin
      r = 'hd;
      v = r[4];

      # 1;
      if (v !== 1'bx) begin
	 $display ("FAIL-1");
	 $finish;
      end

      if (r['bx] != 1'bx) begin
	 $display ("FAIL-2");
	 $finish;
      end
      $display ("PASS");
   end
endmodule
