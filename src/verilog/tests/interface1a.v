// Interface definition
interface Bus;
  logic [7:0] Addr;
endinterface

// Using the interface
module TestRAM;
   reg [7:0] a;
   Bus TheBus();                   // Instance the interface

   initial begin
      a = 8'h5a;
      TheBus.Addr = 1;
      for (int I=0; I<7; I++)
	TheBus.Addr = TheBus.Addr + 1;
      if (TheBus.Addr !== 8'h08)
	$fatal(0, "FAIL-1");
      if (a !== 8'h5a)
	$fatal(0, "FAIL-2");
      $display("PASS");
   end
endmodule
