module shift2;
   reg [1:0] resff;
   reg       res = 1;
   reg 	     clk = 0;
   integer   i;

   initial begin
      if (resff !== 2'bxx) begin
	 $display("FAIL");
	 $finish;
      end
      resff = resff>>1;
      if (resff !== 2'b0x) begin
	 $display("FAIL");
	 $finish;
      end
      resff = resff>>1;
      if (resff !== 2'b00) begin
	 $display("FAIL");
	 $finish;
      end
      $display("PASS");
   end
endmodule
