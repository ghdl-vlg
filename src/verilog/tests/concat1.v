module concat1;
   reg a;
   reg [3:0] b;
   reg [7:0] c;

   initial begin
      a = 1;
      b = 'h7;
      c = {b, a};
      if (c == 'hf)
	$display("PASS");
      else
	$display("FAIL");
   end
endmodule
