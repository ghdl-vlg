module hierarchy1_sub;
   reg a;
endmodule // hierarchy1_sub

module hierarchy1;
   hierarchy1_sub s();

   initial begin
      s.a = 1;
      if (s.a !== 1)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule // hierarchy1
