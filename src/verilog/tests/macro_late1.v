`define my_macro(STMT) `my_stmt(STMT)
`define my_stmt(S) /* none */

module macro_late1;
   initial begin
      `my_macro($display("FAIL %d", 5));
      `my_macro($finish);

      $display ("PASS");
   end
endmodule
