module counter_tb;
  reg clk, reset, enable; 
  wire [3:0] count; 
    
  counter U0 ( 
  .clk    (clk), 
  .reset  (reset), 
  .enable (enable), 
  .count  (count) 
  ); 

  initial begin
     reset = 1;
     enable = 1;
     clk = 0;
     # 4;
     reset = 0;
  end

   always
     # 2 clk = ~clk;

   initial begin
      # 13
	if (count != 2)
	  $display ("FAIL");
	else
	  $display ("PASS");
      $finish;
   end
endmodule 

//-----------------------------------------------------
// Design Name : counter
// File Name   : counter.v
// Function    : 4 bit up counter
// Coder       : Deepak
//-----------------------------------------------------
module counter (clk, reset, enable, count);
input clk, reset, enable;
output [3:0] count;
reg [3:0] count;                                   

always @ (posedge clk)
if (reset == 1'b1) begin
  count <= 0;
end else if ( enable == 1'b1) begin
  count <= count + 1;
end

endmodule  
