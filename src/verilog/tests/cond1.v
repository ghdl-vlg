module cond1;
   reg r;
   wire [1:0]  w;

   assign w = r ? 2'h2 : r ? 2 : 0;

   initial begin
      r = 1;
      #1;
      r = 1'bx;
      $display("PASS");
   end
endmodule // cond1
