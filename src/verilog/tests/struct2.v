module struct2;
   struct { reg [7:0] a; logic b; } v = '{ 'ha5, 'b0 };

   initial begin
      if (v.b != 1'b0)
	begin
	   $display("FAIL-1");
	   $finish;
	end
      v.b = 1'b1;

      if (v.b == 1'b1)
	$display("PASS");
      else
	$display("FAIL-2");
   end
endmodule
