module t;
  typedef struct st1;
  typedef struct {
    int a;
    st1 b;
  } st2;

  typedef struct {
    int c;
  } st1;

  st2 v;

  initial begin
     v.b.c = 12;
     $display("PASS");
  end
endmodule
