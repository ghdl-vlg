// CE
package uvm;

   class uvm_void;
   endclass

   virtual class uvm_object extends uvm_void ;

      function void print ( uvm_printer printer = null ) ;
   endclass
endpackage // uvm
