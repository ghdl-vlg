// CO
class uvm_resource_types ;

  typedef bit [ 1 : 0 ] override_t ;
  typedef enum override_t { TYPE_OVERRIDE = 2 'b 0001 ,
                            NAME_OVERRIDE = 2 'b 0001 } override_e ;
endclass // uvm_resource_types
