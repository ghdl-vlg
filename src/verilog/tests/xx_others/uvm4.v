// CO
class uvm_resource # ( type T = int );

  protected T val ;


  function T read ( ) ;
    return val ;
  endfunction

endclass

class uvm_int_rsrc extends uvm_resource # ( int ) ;

  function string convert2string ( ) ;
    string s ;
    $sformat ( s ,"%0d" , read ( ) ) ;
    return s ;
  endfunction

endclass
