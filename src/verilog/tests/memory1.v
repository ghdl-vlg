module memory1 ();
   reg [7:0] mem[0:15];

   initial begin
      mem[0] = 23;

      if (mem[0] !== 23) begin
         $display("FAILED");
         $finish;
      end

      $display("PASSED");

   end
endmodule // memory1
