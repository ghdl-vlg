// NE
interface simple_bus; // Define the interface
  logic req, gnt;
  logic [7:0] addr, data;
  logic [1:0] mode;
  logic start, rdy;
endinterface: simple_bus

module memMod(simple_bus sb_intf, // Access the simple_bus interface
              input logic clk);
  logic avail;
  // When memMod is instantiated in module top, a.req is the req
  // signal in the sb_intf instance of the 'simple_bus' interface
  always @(posedge clk) sb_intf.gnt <= sb_intf.req & avail;
endmodule

module cpuMod(simple_bus sb_intf, input logic clk);
  // ...
endmodule

module top;
   logic clk = 0;
   simple_bus sb_intf(); // Instantiate the interface
   memMod mem(.*); // implicit port connections
   cpuMod cpu(.*); // implicit port connections
   initial
     $display("PASS");
endmodule
