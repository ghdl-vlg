module func8();
   function automatic init(bit v);
      bit t = v;
      return t;
   endfunction

   initial begin
      bit r;
      r = init(0);
      if (r !== 0)
	$fatal(0, "FAIL-0");
      r = init(1);
      if (r !== 1)
	$fatal(0, "FAIL-1");
      
      $display("PASS");
   end
endmodule
