module stime1;
   time t;
   initial begin
     t = $time;
     if (t != 0) begin
       $display ("FAIL");
       $finish;
     end
     #1 t = $time;
     if (t != 1) begin
       $display ("FAIL");
       $finish;
     end
     $display ("PASS");
   end
endmodule // stime1
