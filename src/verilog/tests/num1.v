module num1;
   reg [31:0] a;
   
   initial
     begin
	a = 'h41;
	if (a != 65)
	  begin
	     $display("FAIL 1");
	     $finish;
	  end

	a = 'habcd1234;
	# 1;
	
	if (a != 2882343476)
	  begin
	     $display("FAIL 2");
	     $finish;
	  end

	if (a != 'o025363211064)
	  begin
	     $display("FAIL 3");
	     $finish;
	  end

	if (a != 'b10101011110011010001001000110100)
	  begin
	     $display("FAIL 4");
	     $finish;
	  end

	$display ("PASS");
     end
endmodule
