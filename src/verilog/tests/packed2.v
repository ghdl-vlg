module packed2();
   reg [7:0][0:3] w;

   initial
     begin
	w = 'h1234;
	$display("w = %h", w);
	if (w != 16'h1234)
	  begin
	     $display("FAIL-1");
	     $finish;
	  end
	#1
	w[7] = w[0];
	$display("w = %h", w);
	if (w != 32'h40001234)
	  begin
	     $display("FAIL-2");
	     $finish;
	  end
	$display("PASS");
	$finish;
     end
endmodule
