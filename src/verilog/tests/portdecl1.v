module portdecl1a (
   output reg       a,
   output reg [3:0] b);

   initial
     begin
        a <= 1;
        b <= 5;
     end
endmodule // portdecl1a

module portdecl1 ();
   wire a1;
   wire [3:0] b1;

   portdecl1a inst (a1, b1);
   initial
     # 1 if (b1 == 4'b0101)
       $display("PASS");
     else
       $display("FAIL");

endmodule // portdecl1
