// NE
`define TRUE 1

module macro1;
   initial
`ifdef TRUE
       $display ("PASS");
`else
       $display ("FAIL");
`endif
endmodule
