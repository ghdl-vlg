// NE
`define NEG(x) x``_n

module t;
   reg [3:0] p_n = 1;

   initial begin
      if (`NEG(p) !== 4'h1)
	$fatal(0, "FAIL");
    $display("PASS");
  end
endmodule
