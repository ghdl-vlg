// NE
`define TRUE

module macro2;
   initial
`ifdef TRUE
       $display ("PASS");
`else
       $display ("FAIL");
`endif
endmodule
