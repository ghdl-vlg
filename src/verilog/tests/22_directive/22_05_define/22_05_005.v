// NE
`ifndef NOT_DEFINED
 `define VAL 12
`else
 `define VAL 15
`endif

module macro5;
   initial
     if (`VAL == 12)
       $display ("PASS");
     else
       $display ("FAIL");
endmodule
