// NE
`define OP(a,b) \
`ifdef PLUS \
  (a + b) \
`else \
  (a - b) \
`endif

module t;
  reg [7:0] m = `OP(5, 1);

`define PLUS 1

   reg [7:0] p = `OP(5, 1);
  initial begin
    if (m !== 4)
      $fatal(0, "FAIL-1");
    if (p !== 6)
      $fatal(0, "FAIL-2");
    $display("PASS");
  end
endmodule
