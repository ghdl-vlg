// NE
`define TRUE
`undef TRUE

module macro3;
   initial
`ifdef TRUE
       $display ("FAIL");
`else
       $display ("PASS");
`endif
endmodule
