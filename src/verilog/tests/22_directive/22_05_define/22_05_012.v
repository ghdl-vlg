// NE

`define set_signed_val(ARG, L) ARG[L] = 1;

`define set_unsigned_val(ARG, L) ARG[L] = 1;

`define clr(ARG, L, TYP) `set_``TYP``_val(ARG, L)

module m1;
   initial begin
      int v;

      `clr(v, 0, signed);
      if (v !== 1)
	$fatal(0, "FAIL");
      $display("PASS");
   end
endmodule
