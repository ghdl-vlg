// NE
`define OP_1(a, b = `DEFAULT_OP) \
    (a + b)

`define DEFAULT_OP 3

module t;
  reg [7:0] p = `OP_1(2);
  initial begin
    if (p !== 5)
      $fatal(0, "FAIL-2");
    $display("PASS");
  end
endmodule
