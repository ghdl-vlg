module test (
  input [7:0] a,
//Multiple ports that share all
//attributes can be declared together.
  input signed [7:0] b, c, d,

//Every attribute of the declaration
//must be in the one declaration.
  output [7:0] e,
  output var signed [7:0] f, g,
  output signed
  [7:0] h) ;
//
// It is illegal to redeclare any ports of
// the module in the body of the module.
endmodule
