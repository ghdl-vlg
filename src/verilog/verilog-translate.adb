with Tables;
with Interfaces; use Interfaces;
with Verilog.Errors; use Verilog.Errors;
with Verilog.Linearize;
with Types; use Types;
with Ortho_Ident; use Ortho_Ident;
with Name_Table;
with Str_Table;
with Trans_Utils; use Trans_Utils;
with Std_Names;

package body Verilog.Translate is
   Ghdl_U32 : O_Tnode;
   Ghdl_U32_0 : O_Cnode;
   Ghdl_U32_All1 : O_Cnode;

   Ghdl_Bool : O_Tnode;
   Ghdl_Bool_False : O_Cnode;
   Ghdl_Bool_True : O_Cnode;

   Ghdl_Logic32_Val : O_Fnode;
   Ghdl_Logic32_Xz : O_Fnode; -- X:0, Z:1
   Ghdl_Logic32 : O_Tnode;

   Ghdl_Logic32_Ptr : O_Tnode;

   Ghdl_Logic32_Vec : O_Tnode;
   Ghdl_Logic32_Vptr : O_Tnode;

   Ghdl_Task_Arg : O_Tnode;
   Ghdl_Task_Arg_Kind_Field : O_Fnode;
   Ghdl_Task_Arg_Len_Field : O_Fnode;
   Ghdl_Task_Arg_Ptr_Field : O_Fnode;

   Ghdl_Arg_Kind : O_Tnode;
   Ghdl_Arg_Logic : O_Cnode;
   Ghdl_Arg_String : O_Cnode;

   Ghdl_Task_Arg_Arr : O_Tnode;
   Ghdl_Task_Arg_Arr_Ptr : O_Tnode;

   Ghdl_Char : O_Tnode;
   Ghdl_Ptr : O_Tnode;

   function New_Acc_Obj (Obj : O_Dnode) return O_Lnode is
   begin
      return New_Access_Element (New_Obj_Value (Obj));
   end New_Acc_Obj;

   type Info_Kind is
     (
      Kind_Var,
      Kind_Module,
      Kind_Stmt
      );

   type Info_Type (Kind : Info_Kind) is record
      case Kind is
         when Kind_Var =>
            Var_Var : Var_Acc;
         when Kind_Module =>
            --  Type and pointer to the instance.
            Module_Inst_Type : O_Tnode;
            Module_Inst_Ptr : O_Tnode;

            --  Subprogram to elaborate the module.
            Module_Elab_Subprg : O_Dnode;
            Module_Elab_Instance : O_Dnode;

            --  Variable pointing to the instance.
            Module_Var : O_Dnode := O_Dnode_Null;
         when Kind_Stmt =>
            --  Number (used in identifier).
            Stmt_Number : Natural;

            --  Subprogram corresponding to the statement.
            Stmt_Subprg : O_Dnode;

            --  First argument: the instance.
            Stmt_Instance : O_Dnode;

            --  Copy of the second argument: step.
            Stmt_Step : Var_Acc;
      end case;
   end record;
   type Info_Acc is access Info_Type;

   package Info_Table is new Tables
     (Table_Component_Type => Info_Acc,
      Table_Index_Type => Node,
      Table_Low_Bound => 2,
      Table_Initial => 1024);

   procedure Adjust_Table
   is
      Pos : constant Node := Info_Table.Last + 1;
   begin
      Info_Table.Set_Last (Get_Last_Node);
      Info_Table.Table (Pos .. Info_Table.Last) := (others => null);
   end Adjust_Table;

   function Get_Info (N : Node) return Info_Acc is
   begin
      return Info_Table.Table (N);
   end Get_Info;

   function Add_Info (N : Node; Kind : Info_Kind) return Info_Acc
   is
      Res : Info_Acc;
   begin
      if Info_Table.Table (N) /= null then
         raise Internal_Error;
      end if;
      Res := new Info_Type (Kind);
      Info_Table.Table (N) := Res;
      return Res;
   end Add_Info;

   --  Create the built-in types and subprograms.
   procedure Initialize
   is
      El : O_Element_List;
      Enum_List : O_Enum_List;
      Interfaces : O_Inter_List;
      Param : O_Dnode;
   begin
      -- Create type __ghdl_bool_type is (false, true)
      New_Boolean_Type (Ghdl_Bool,
                        Get_Identifier ("false"),
                        Ghdl_Bool_False,
                        Get_Identifier ("true"),
                        Ghdl_Bool_True);
      New_Type_Decl (Get_Identifier ("__ghdl_bool"),
                     Ghdl_Bool);

      Ghdl_U32 := New_Unsigned_Type (32);
      New_Type_Decl (Get_Identifier ("__ghdl_u32"), Ghdl_U32);

      Ghdl_U32_0 := New_Unsigned_Literal (Ghdl_U32, 0);
      Ghdl_U32_All1 := New_Unsigned_Literal (Ghdl_U32, 16#Ffff_Ffff#);

      Start_Record_Type (El);
      New_Record_Field
        (El, Ghdl_Logic32_Val, Get_Identifier ("val"), Ghdl_U32);
      New_Record_Field
        (El, Ghdl_Logic32_Xz, Get_Identifier ("xz"), Ghdl_U32);
      Finish_Record_Type (El, Ghdl_Logic32);
      New_Type_Decl (Get_Identifier ("__ghdl_logic32"), Ghdl_Logic32);

      Ghdl_Logic32_Ptr := New_Access_Type (Ghdl_Logic32);
      New_Type_Decl (Get_Identifier ("__ghdl_logic32_ptr"), Ghdl_Logic32_Ptr);

      Ghdl_Logic32_Vec := New_Array_Type (Ghdl_Logic32, Ghdl_U32);
      New_Type_Decl (Get_Identifier ("__ghdl_logic32_vec"), Ghdl_Logic32_Vec);

      Ghdl_Logic32_Vptr := New_Access_Type (Ghdl_Logic32_Vec);
      New_Type_Decl (Get_Identifier ("__ghdl_logic32_vptr"),
                     Ghdl_Logic32_Vptr);

      Start_Enum_Type (Enum_List, 8);
      New_Enum_Literal
        (Enum_List, Get_Identifier ("__ghdl_arg_logic"), Ghdl_Arg_Logic);
      New_Enum_Literal
        (Enum_List, Get_Identifier ("__ghdl_arg_string"), Ghdl_Arg_String);
      Finish_Enum_Type (Enum_List, Ghdl_Arg_Kind);
      New_Type_Decl (Get_Identifier ("__ghdl_arg_kind"), Ghdl_Arg_Kind);

      Start_Record_Type (El);
      New_Record_Field (El, Ghdl_Task_Arg_Kind_Field, Get_Identifier ("kind"),
                        Ghdl_Arg_Kind);
      New_Record_Field (El, Ghdl_Task_Arg_Len_Field, Get_Identifier ("len"),
                        Ghdl_U32);
      New_Record_Field (El, Ghdl_Task_Arg_Ptr_Field, Get_Identifier ("ptr"),
                        Ghdl_Logic32_Ptr);
      Finish_Record_Type (El, Ghdl_Task_Arg);
      New_Type_Decl (Get_Identifier ("__ghdl_task_arg"), Ghdl_Task_Arg);

      Ghdl_Task_Arg_Arr := New_Array_Type (Ghdl_Task_Arg, Ghdl_U32);
      New_Type_Decl
        (Get_Identifier ("__ghdl_task_arg_arr"), Ghdl_Task_Arg_Arr);
      Ghdl_Task_Arg_Arr_Ptr := New_Access_Type (Ghdl_Task_Arg_Arr);
      New_Type_Decl
        (Get_Identifier ("__ghdl_task_arg_arr_ptr"), Ghdl_Task_Arg_Arr_Ptr);

      declare
         function Create_Systask (Id : String) return O_Dnode
         is
            Res : O_Dnode;
         begin
            Start_Procedure_Decl
              (Interfaces, Get_Identifier (Id), O_Storage_External);
            New_Interface_Decl
              (Interfaces, Param, Get_Identifier ("nbr"), Ghdl_U32);
            New_Interface_Decl (Interfaces, Param, Get_Identifier ("desc"),
                                Ghdl_Task_Arg_Arr_Ptr);
            Finish_Subprogram_Decl (Interfaces, Res);
            return Res;
         end Create_Systask;
      begin
         Ghdl_Systask_Display := Create_Systask ("__ghdl_systask_display");
         Ghdl_Systask_Finish := Create_Systask ("__ghdl_systask_finish");
      end;

      --  Create char
      Ghdl_Char := New_Unsigned_Type (8);
      New_Type_Decl (Get_Identifier ("__ghdl_char"), Ghdl_Char);

      --  Create generic pointer.
      Ghdl_Ptr := New_Access_Type (Ghdl_Char);
      New_Type_Decl (Get_Identifier ("__ghdl_ptr"), Ghdl_Ptr);

      -- function __ghdl_malloc (length : ghdl_index_type)
      --    return ghdl_ptr_type;
      Start_Function_Decl
        (Interfaces, Get_Identifier ("__ghdl_malloc"), O_Storage_External,
         Ghdl_Ptr);
      New_Interface_Decl (Interfaces, Param,
                          Get_Identifier ("length"), Ghdl_U32);
      Finish_Subprogram_Decl (Interfaces, Ghdl_Malloc);

      Start_Procedure_Decl
        (Interfaces, Get_Identifier ("__ghdl_initial_register"),
         O_Storage_External);
      New_Interface_Decl
        (Interfaces, Param, Get_Identifier ("this"), Ghdl_Ptr);
      New_Interface_Decl
        (Interfaces, Param, Get_Identifier ("proc"), Ghdl_Ptr);
      Finish_Subprogram_Decl (Interfaces, Ghdl_Initial_Register);

      Start_Procedure_Decl
        (Interfaces, Get_Identifier ("__ghdl_always_register"),
         O_Storage_External);
      New_Interface_Decl
        (Interfaces, Param, Get_Identifier ("this"), Ghdl_Ptr);
      New_Interface_Decl
        (Interfaces, Param, Get_Identifier ("proc"), Ghdl_Ptr);
      Finish_Subprogram_Decl (Interfaces, Ghdl_Always_Register);

      Start_Procedure_Decl
        (Interfaces, Get_Identifier ("__ghdl_process_delay"),
         O_Storage_External);
      New_Interface_Decl
        (Interfaces, Param, Get_Identifier ("delay"), Ghdl_U32);
      Finish_Subprogram_Decl (Interfaces, Ghdl_Process_Delay);
   end Initialize;

   procedure Append_Identifier_Prefix (N : Node)
   is
      use Name_Table;
   begin
      Image (Get_Identifier (N));
      if Name_Buffer (1) = '\' then
         --  FIXME: handle extended identifier.
         raise Internal_Error;
      else
         Append_Identifier_Prefix (Name_Length);
         Append_Identifier_Prefix (Name_Buffer (1 .. Name_Length));
         Append_Identifier_Prefix ('_');
      end if;
   end Append_Identifier_Prefix;

   procedure Push_Identifier_Prefix (Mark : out Id_Mark_Type;
                                     K : Character;
                                     N : Node)
   is
      use Name_Table;
   begin
      Save_Mark (Mark);

      --  Set kind of prefix.
      Append_Identifier_Prefix (K);
      Append_Identifier_Prefix (N);
   end Push_Identifier_Prefix;

   procedure Push_Identifier_Prefix (Mark : out Id_Mark_Type;
                                     K : Character;
                                     N : Natural)
   is
      use Name_Table;
   begin
      Save_Mark (Mark);

      --  Set kind of prefix.
      Append_Identifier_Prefix (K);
      Append_Identifier_Prefix (N);
      Append_Identifier_Prefix ('_');
   end Push_Identifier_Prefix;

   procedure Push_Identifier_Prefix (Mark : out Id_Mark_Type;
                                     K : Character)
   is
      use Name_Table;
   begin
      Save_Mark (Mark);

      --  Set kind of prefix.
      Append_Identifier_Prefix (K);
      Append_Identifier_Prefix ('a');
      Append_Identifier_Prefix_Uniq;
      Append_Identifier_Prefix ('_');
   end Push_Identifier_Prefix;
   pragma Unreferenced (Push_Identifier_Prefix);

--     procedure Pop_Identifier (Mark : Id_Mark_Type) is
--     begin
--        Id_Length := Mark.Length;
--     end Pop_Identifier;

   function Create_Var_Identifier (K : Character; N : Node)
                                  return O_Ident
   is
      L : Natural;
   begin
      L := Get_Identifier_Mark;

      Append_Identifier_Prefix (K);
      Append_Identifier_Prefix (N);

      return Create_Var_Identifier (L);
   end Create_Var_Identifier;

   --  Create a temporary variable.
   type Temp_Level_Type;
   type Temp_Level_Acc is access Temp_Level_Type;
   type Temp_Level_Type is record
      Prev : Temp_Level_Acc;
      Level : Natural;
      Id : Natural;
      Emitted : Boolean;
   end record;
   --  Current level.
   Temp_Level : Temp_Level_Acc := null;

   --  List of unused temp_level_type structures.  To be faster, they are
   --  never deallocated.
   Old_Level : Temp_Level_Acc := null;

   procedure Open_Temp
   is
      L : Temp_Level_Acc;
   begin
      if Old_Level /= null then
         L := Old_Level;
         Old_Level := L.Prev;
      else
         L := new Temp_Level_Type;
      end if;
      L.all := (Prev => Temp_Level,
                Level => 0,
                Id => 0,
                Emitted => False);
      if Temp_Level /= null then
         L.Level := Temp_Level.Level + 1;
      end if;
      Temp_Level := L;
   end Open_Temp;

   procedure Close_Temp
   is
      L : Temp_Level_Acc;
   begin
      if Temp_Level = null then
         --  OPEN_TEMP was not called.
         raise Internal_Error;
      end if;
      if Temp_Level.Emitted then
         Finish_Declare_Stmt;
      end if;

      --  Unlink temp_level.
      L := Temp_Level;
      Temp_Level := L.Prev;
      L.Prev := Old_Level;
      Old_Level := L;
   end Close_Temp;

   function Create_Temp (Atype : O_Tnode) return O_Dnode
   is
      Str : String (1 .. 12);
      Val : Natural;
      Res : O_Dnode;
      P : Natural;
   begin
      if Temp_Level = null then
         --  OPEN_TEMP was never called.
         raise Internal_Error;
         --  This is an hack, just to allow array subtype to array type
         --  conversion.
         --New_Var_Decl
         --  (Res, Create_Uniq_Identifier, O_Storage_Private, Atype);
         --return Res;
      else
         if not Temp_Level.Emitted then
            Temp_Level.Emitted := True;
            Start_Declare_Stmt;
         end if;
      end if;
      Val := Temp_Level.Id;
      Temp_Level.Id := Temp_Level.Id + 1;
      P := Str'Last;
      loop
         Str (P) := Character'Val (Val mod 10 + Character'Pos ('0'));
         Val := Val / 10;
         P := P - 1;
         exit when Val = 0;
      end loop;
      Str (P) := '_';
      P := P - 1;
      Val := Temp_Level.Level;
      loop
         Str (P) := Character'Val (Val mod 10 + Character'Pos ('0'));
         Val := Val / 10;
         P := P - 1;
         exit when Val = 0;
      end loop;
      Str (P) := 'T';
      --Str (12) := Nul;
      New_Var_Decl
        (Res, Get_Identifier (Str (P .. Str'Last)), O_Storage_Local, Atype);
      return Res;
   end Create_Temp;

   function Create_Temp_Init (Atype : O_Tnode; Value : O_Enode)
                             return O_Dnode
   is
      Res : O_Dnode;
   begin
      Res := Create_Temp (Atype);
      New_Assign_Stmt (New_Obj (Res), Value);
      return Res;
   end Create_Temp_Init;

   function Create_Temp_Ptr (Atype : O_Tnode; Name : O_Lnode)
                            return O_Dnode is
   begin
      return Create_Temp_Init (Atype, New_Address (Name, Atype));
   end Create_Temp_Ptr;

   procedure Translate_Decl_Item (Item : Node; Num : Natural);

   procedure Translate_Decl_Chain (Chain : Node)
   is
      Num : Natural;
      Item : Node;
   begin
      Item := Chain;
      Num := 0;
      while Item /= Null_Node loop
         Translate_Decl_Item (Item, Num);
         Item := Get_Chain (Item);
         Num := Num + 1;
      end loop;
   end Translate_Decl_Chain;

   procedure Translate_Decl_Item (Item : Node; Num : Natural)
   is
      Info : Info_Acc;
   begin
      case Get_Kind (Item) is
         when N_Output =>
            null;

         when N_Seq_Block =>
            null;

         when N_Var =>
            Info := Add_Info (Item, Kind_Var);
            Info.Var_Var := Create_Var (Create_Var_Identifier ('V', Item),
                                        Ghdl_Logic32);
         when N_Initial
           | N_Always =>
            declare
               Mark : Id_Mark_Type;
            begin
               Info := Add_Info (Item, Kind_Stmt);
               Info.Stmt_Number := Num;
               Push_Identifier_Prefix (Mark, 'S', Num);
               Info.Stmt_Step := Create_Var (Create_Var_Identifier ("step"),
                                             Ghdl_U32);
               Pop_Identifier_Prefix (Mark);
            end;

         when N_Task =>
            declare
               Mark : Id_Mark_Type;
            begin
               Push_Identifier_Prefix (Mark, 'T', Num);
               Translate_Decl_Chain (Get_Items_Chain (Item));
               Pop_Identifier_Prefix (Mark);
            end;

         when others =>
            Error_Kind ("translate_decl_item", Item);
      end case;
   end Translate_Decl_Item;

   function Get_Hier (N : Node) return O_Lnode
   is
      Info : Info_Acc;
   begin
      case Get_Kind (N) is
         when N_Module =>
            Info := Get_Info (N);
            if Info.Module_Var = O_Dnode_Null then
               raise Internal_Error;
            end if;
            return New_Access_Element (New_Obj_Value (Info.Module_Var));
         when others =>
            Error_Kind ("get_hier", N);
      end case;
   end Get_Hier;

   Cur_Stmt : Node;

   function Get_Current_Step return O_Lnode
   is
   begin
      return New_Selected_Element
        (Get_Hier (Get_Parent (Cur_Stmt)),
         Get_Var_Field (Get_Info (Cur_Stmt).Stmt_Step));
   end Get_Current_Step;

   function Translate_Lvalue (V : Node) return O_Lnode
   is
      Info : Info_Acc;
   begin
      case Get_Kind (V) is
         when N_Name =>
            return Translate_Lvalue (Get_Declaration (V));
         when N_Var =>
            Info := Get_Info (V);
            return New_Selected_Element
              (Get_Hier (Get_Parent (V)), Get_Var_Field (Info.Var_Var));
         when others =>
            Error_Kind ("translate_lvalue", V);
      end case;
   end Translate_Lvalue;

   function Stabilize (L : O_Lnode) return O_Dnode is
   begin
      return Create_Temp_Ptr (Ghdl_Logic32_Ptr, L);
   end Stabilize;
   pragma Unreferenced (Stabilize);

   function Stabilize (V : O_Enode) return O_Dnode
   is
      Res : O_Dnode;
   begin
      Res := Create_Temp_Init (Ghdl_Logic32_Ptr, V);
      return Res;
   end Stabilize;

   function Translate_Expression (Expr : Node) return O_Enode;

   function Translate_Binary_Op (Expr : Node) return O_Enode
   is
      L, R : O_Dnode;
      Res : O_Dnode;
   begin
      L := Stabilize (Translate_Expression (Get_Left (Expr)));
      R := Stabilize (Translate_Expression (Get_Right (Expr)));
      case Get_Binary_Op (Expr) is
         when Binop_Add =>
            declare
               If_Blk : O_If_Block;
            begin
               --  If xz then return z.
               Res := Create_Temp (Ghdl_Logic32);

               Start_If_Stmt
                 (If_Blk,
                  New_Dyadic_Op
                  (ON_Or,
                   New_Compare_Op (ON_Neq,
                                   New_Value (New_Selected_Element
                                              (New_Acc_Obj (L),
                                               Ghdl_Logic32_Xz)),
                                   New_Lit (Ghdl_U32_0),
                                   Ghdl_Bool),
                   New_Compare_Op (ON_Neq,
                                   New_Value (New_Selected_Element
                                              (New_Acc_Obj (R),
                                               Ghdl_Logic32_Xz)),
                                   New_Lit (Ghdl_U32_0),
                                   Ghdl_Bool)));
               New_Assign_Stmt (New_Selected_Element (New_Obj (Res),
                                                      Ghdl_Logic32_Xz),
                                New_Lit (Ghdl_U32_All1));
               New_Assign_Stmt (New_Selected_Element (New_Obj (Res),
                                                      Ghdl_Logic32_Val),
                                New_Lit (Ghdl_U32_0));
               New_Else_Stmt (If_Blk);
               New_Assign_Stmt (New_Selected_Element (New_Obj (Res),
                                                      Ghdl_Logic32_Xz),
                                New_Lit (Ghdl_U32_0));
               New_Assign_Stmt
                 (New_Selected_Element (New_Obj (Res),
                                        Ghdl_Logic32_Val),
                  New_Dyadic_Op (ON_Add_Ov,
                                 New_Value (New_Selected_Element
                                            (New_Acc_Obj (L),
                                             Ghdl_Logic32_Val)),
                                 New_Value (New_Selected_Element
                                            (New_Acc_Obj (R),
                                             Ghdl_Logic32_Val))));
               Finish_If_Stmt (If_Blk);

               return New_Address (New_Obj (Res), Ghdl_Logic32_Ptr);
            end;
         when others =>
            Error_Kind ("translate_binary_op: " &
                        Binary_Ops'Image (Get_Binary_Op (Expr)), Expr);
            return O_Enode_Null;
      end case;
   end Translate_Binary_Op;

   function Translate_Unary_Op (Expr : Node) return O_Enode
   is
      Op : O_Dnode;
      Res : O_Dnode;
   begin
      Op := Stabilize (Translate_Expression (Get_Expression (Expr)));
      case Get_Unary_Op (Expr) is
         when Unop_Bit_Neg =>
            Res := Create_Temp (Ghdl_Logic32);

            --  Xz = Xz
            New_Assign_Stmt
              (New_Selected_Element (New_Obj (Res), Ghdl_Logic32_Xz),
               New_Value (New_Selected_Element (New_Obj (Op),
                                                Ghdl_Logic32_Xz)));

            --  V = (~V) & (~Xz).
            New_Assign_Stmt
              (New_Selected_Element (New_Obj (Res), Ghdl_Logic32_Val),
               New_Dyadic_Op
               (ON_And,
                New_Monadic_Op
                (ON_Not,
                 New_Value (New_Selected_Element (New_Obj (Op),
                                                  Ghdl_Logic32_Val))),
                New_Monadic_Op
                (ON_Not,
                 New_Value (New_Selected_Element (New_Obj (Op),
                                                  Ghdl_Logic32_Xz)))));
            return New_Address (New_Obj (Res), Ghdl_Logic32_Ptr);
         when others =>
            Error_Kind ("translate_unary_op: " &
                        Unary_Ops'Image (Get_Unary_Op (Expr)), Expr);
            return O_Enode_Null;
      end case;
   end Translate_Unary_Op;

   function Translate_Expression (Expr : Node) return O_Enode is
   begin
      case Get_Kind (Expr) is
         when N_Number =>
            declare
               D : O_Dnode;
            begin
               D := Create_Temp (Ghdl_Logic32);
               New_Assign_Stmt
                 (New_Selected_Element (New_Obj (D), Ghdl_Logic32_Val),
                  New_Lit (New_Unsigned_Literal
                           (Ghdl_U32,
                            Unsigned_64 (Get_Number_Val (Expr)))));
               New_Assign_Stmt
                 (New_Selected_Element (New_Obj (D), Ghdl_Logic32_Xz),
                  New_Lit (New_Unsigned_Literal
                           (Ghdl_U32,
                            Unsigned_64 (Get_Number_Xz (Expr)))));
               return New_Address (New_Obj (D), Ghdl_Logic32_Ptr);
            end;
         when N_String =>
            declare
               Cst_Type : O_Tnode;
               Cst : O_Dnode;
               Len : Int32;
               Str : String_Fat_Acc;
               V : Unsigned_32;
               Pos : Natural;
               Val2 : O_Cnode;
               Array_Aggr : O_Array_Aggr_List;
               Rec_Aggr : O_Record_Aggr_List;
            begin
               Len := Get_Bit_Length (Expr);
               Str := Str_Table.Get_String_Fat_Acc (Get_String_Id (Expr));
               Cst_Type := New_Constrained_Array_Type
                 (Ghdl_Logic32_Vec,
                  New_Unsigned_Literal (Ghdl_U32,
                                        Unsigned_64 ((Len + 31) / 32)));
               New_Const_Decl (Cst, O_Ident_Nul, O_Storage_Private, Cst_Type);
               Start_Const_Value (Cst);
               Start_Array_Aggr (Array_Aggr, Cst_Type);
               Pos := 0;
               V := 0;
               for I in reverse 1 .. Natural (Len / 8) loop
                  V := V + Shift_Left (Character'Pos (Str (I)), Pos);
                  Pos := Pos + 8;
                  if Pos = 32 or I = 1 then
                     Start_Record_Aggr (Rec_Aggr, Ghdl_Logic32);
                     New_Record_Aggr_El
                       (Rec_Aggr,
                        New_Unsigned_Literal (Ghdl_U32, Unsigned_64 (V)));
                     New_Record_Aggr_El
                       (Rec_Aggr, Ghdl_U32_0);
                     Finish_Record_Aggr (Rec_Aggr, Val2);
                     New_Array_Aggr_El (Array_Aggr, Val2);
                     Pos := 0;
                     V := 0;
                  end if;
               end loop;
               Finish_Array_Aggr (Array_Aggr, Val2);
               Finish_Const_Value (Cst, Val2);
               return New_Address (New_Obj (Cst), Ghdl_Logic32_Vptr);
            end;
         when N_Name =>
            return New_Address (Translate_Lvalue (Expr), Ghdl_Logic32_Ptr);
         when N_Binary_Op =>
            return Translate_Binary_Op (Expr);
         when N_Unary_Op =>
            return Translate_Unary_Op (Expr);
         when others =>
            Error_Kind ("translate_expression", Expr);
      end case;
   end Translate_Expression;

   --  X/Z is considered as false.
   function Translate_Expr_Bool (Expr : Node) return O_Enode
   is
      Op : ON_Op_Kind;
      L, R : O_Dnode;
   begin
      case Get_Kind (Expr) is
         when N_Binary_Op =>
            case Get_Binary_Op (Expr) is
               when Binop_Less =>
                  Op := ON_Lt;
               when others =>
                  Error_Kind ("translate_Expr_Bool: " &
                              Binary_Ops'Image (Get_Binary_Op (Expr)), Expr);
            end case;
            L := Stabilize (Translate_Expression (Get_Left (Expr)));
            R := Stabilize (Translate_Expression (Get_Right (Expr)));
            return New_Dyadic_Op
              (ON_And,
               New_Dyadic_Op (ON_And,
                              New_Compare_Op (ON_Eq,
                                              New_Value (New_Selected_Element
                                                         (New_Acc_Obj (L),
                                                          Ghdl_Logic32_Xz)),
                                              New_Lit (Ghdl_U32_0),
                                              Ghdl_Bool),
                              New_Compare_Op (ON_Eq,
                                              New_Value (New_Selected_Element
                                                         (New_Acc_Obj (R),
                                                          Ghdl_Logic32_Xz)),
                                              New_Lit (Ghdl_U32_0),
                                              Ghdl_Bool)),
               New_Compare_Op (Op,
                               New_Value (New_Selected_Element
                                          (New_Acc_Obj (L),
                                           Ghdl_Logic32_Val)),
                               New_Value (New_Selected_Element
                                          (New_Acc_Obj (R),
                                           Ghdl_Logic32_Val)),
                               Ghdl_Bool));
         when N_Name =>
            L := Stabilize (Translate_Expression (Expr));
            return New_Dyadic_Op
              (ON_And,
               New_Compare_Op (ON_Neq,
                               New_Value (New_Selected_Element
                                          (New_Acc_Obj (L),
                                           Ghdl_Logic32_Val)),
                               New_Lit (Ghdl_U32_0),
                               Ghdl_Bool),
               New_Compare_Op (ON_Eq,
                               New_Value (New_Selected_Element
                                          (New_Acc_Obj (L),
                                           Ghdl_Logic32_Xz)),
                               New_Lit (Ghdl_U32_0),
                               Ghdl_Bool));
         when others =>
            Error_Kind ("translate_expr_bool", Expr);
      end case;
      return O_Enode_Null;
   end Translate_Expr_Bool;

   Cur_Stmt_Loop : O_Snode;

   procedure Translate_Stmt (Item : Node);

   procedure Translate_Stmt_Chain (Chain : Node)
   is
      Stmt : Node;
   begin
      Stmt := Chain;
      while Stmt /= Null_Node loop
         Translate_Stmt (Stmt);
         Stmt := Get_Chain (Stmt);
      end loop;
   end Translate_Stmt_Chain;

   procedure Translate_Stmt (Item : Node) is
   begin
      if Item = Null_Node then
         return;
      end if;
      case Get_Kind (Item) is
         when N_For =>
            declare
               Label : O_Snode;
               If_Blk : O_If_Block;
            begin
               Translate_Stmt (Get_Initial_Assign (Item));
               Start_Loop_Stmt (Label);

               Open_Temp;
               Start_If_Stmt
                 (If_Blk,
                  New_Monadic_Op
                  (ON_Not, Translate_Expr_Bool (Get_Expression (Item))));
               New_Exit_Stmt (Label);
               Finish_If_Stmt (If_Blk);
               Close_Temp;

               Translate_Stmt_Chain (Get_Statement (Item));
               Translate_Stmt (Get_Step_Assign (Item));
               Finish_Loop_Stmt (Label);
            end;
         when N_If =>
            declare
               If_Blk : O_If_Block;
               Stmt1 : Node;
            begin
               Open_Temp;
               Start_If_Stmt (If_Blk,
                              Translate_Expr_Bool (Get_Condition (Item)));
               Translate_Stmt_Chain (Get_Cond_True (Item));
               Stmt1 := Get_Cond_False (Item);
               if Stmt1 /= Null_Node then
                  New_Else_Stmt (If_Blk);
                  Translate_Stmt_Chain (Stmt1);
               end if;
               Finish_If_Stmt (If_Blk);
               Close_Temp;
            end;
         when N_While =>
            declare
               Label2 : O_Snode;
               If_Blk : O_If_Block;
            begin
               Start_Loop_Stmt (Label2);
               Open_Temp;
               Start_If_Stmt (If_Blk,
                              New_Monadic_Op
                              (ON_Not,
                               Translate_Expr_Bool (Get_Expression (Item))));
               New_Exit_Stmt (Label2);
               Finish_If_Stmt (If_Blk);
               Close_Temp;
               Translate_Stmt_Chain (Get_Statement (Item));
               Finish_Loop_Stmt (Label2);
            end;
         when N_Blocking_Assign =>
            declare
               N : O_Dnode;
               V : O_Dnode;
            begin
               Open_Temp;

               V := Create_Temp (Ghdl_Logic32_Ptr);
               N := Create_Temp (Ghdl_Logic32_Ptr);
               New_Assign_Stmt
                 (New_Obj (N),
                  New_Address (Translate_Lvalue (Get_Lvalue (Item)),
                               Ghdl_Logic32_Ptr));

               New_Assign_Stmt (New_Obj (V),
                                Translate_Expression (Get_Expression (Item)));
               New_Assign_Stmt
                 (New_Selected_Element (New_Acc_Obj (N), Ghdl_Logic32_Val),
                  New_Value
                  (New_Selected_Element (New_Acc_Obj (V), Ghdl_Logic32_Val)));
               New_Assign_Stmt
                 (New_Selected_Element (New_Acc_Obj (N), Ghdl_Logic32_Xz),
                  New_Value
                  (New_Selected_Element (New_Acc_Obj (V), Ghdl_Logic32_Xz)));
--               New_Assign_Stmt (New_Obj (V),
--                                New_Address (N, Ghdl_Logic32_Ptr));
               Close_Temp;
            end;
         when N_System_Task_Enable =>
            Open_Temp;
            declare
               Nbr_Args : Natural;
               First_Arg, Arg : Node;
               Desc_Type : O_Tnode;
               Desc : O_Dnode;
               N : Natural;
               Expr : Node;
               Len : Int32;
               Kind : O_Cnode;
               Assoc : O_Assoc_List;
               Proc : O_Dnode;
            begin
               First_Arg := Get_Arguments (Item);

               --  1. Count number of args.
               Nbr_Args := 0;
               Arg := First_Arg;
               while Arg /= Null_Node loop
                  Nbr_Args := Nbr_Args + 1;
                  Arg := Get_Chain (Arg);
               end loop;

               -- 2. Create arg descriptor.
               Desc_Type := New_Constrained_Array_Type
                 (Ghdl_Task_Arg_Arr,
                  New_Unsigned_Literal (Ghdl_U32, Unsigned_64 (Nbr_Args)));
               Desc := Create_Temp (Desc_Type);

               -- 3. Translate arguments.
               Arg := First_Arg;
               N := 0;
               while Arg /= Null_Node loop
                  Expr := Get_Expression (Arg);
                  New_Assign_Stmt
                    (New_Selected_Element
                     (New_Indexed_Element
                      (New_Obj (Desc),
                       New_Lit (New_Unsigned_Literal
                                (Ghdl_U32, Unsigned_64 (N)))),
                      Ghdl_Task_Arg_Ptr_Field),
                     New_Convert_Ov (Translate_Expression (Expr),
                                     Ghdl_Logic32_Ptr));
                  case Get_Kind (Expr) is
                     when N_String =>
                        Kind := Ghdl_Arg_String;
                        Len := Get_Bit_Length (Expr);
                     when others =>
                        Kind := Ghdl_Arg_Logic;
                        Len := Get_Bit_Length (Get_Type (Expr));
                  end case;
                  New_Assign_Stmt
                    (New_Selected_Element
                     (New_Indexed_Element
                      (New_Obj (Desc),
                       New_Lit (New_Unsigned_Literal
                                (Ghdl_U32, Unsigned_64 (N)))),
                      Ghdl_Task_Arg_Kind_Field),
                     New_Lit (Kind));
                  New_Assign_Stmt
                    (New_Selected_Element
                     (New_Indexed_Element
                      (New_Obj (Desc),
                       New_Lit (New_Unsigned_Literal
                                (Ghdl_U32, Unsigned_64 (N)))),
                      Ghdl_Task_Arg_Len_Field),
                     New_Lit (New_Unsigned_Literal (Ghdl_U32,
                                                    Unsigned_64 (Len))));

                  Arg := Get_Chain (Arg);
                  N := N + 1;
               end loop;

               -- 4. Call the task.
               case Get_Identifier (Item) is
                  when Std_Names.Name_Display =>
                     Proc := Ghdl_Systask_Display;
                  when Std_Names.Name_Finish =>
                     Proc := Ghdl_Systask_Finish;
                  when others =>
                     raise Internal_Error;
               end case;
               Start_Association (Assoc, Proc);
               New_Association
                 (Assoc,
                  New_Lit (New_Unsigned_Literal (Ghdl_U32,
                                                 Unsigned_64 (Nbr_Args))));
               New_Association
                 (Assoc, New_Address (New_Obj (Desc), Ghdl_Task_Arg_Arr_Ptr));
               New_Procedure_Call (Assoc);
            end;
            Close_Temp;
         when N_Seq_Block =>
            declare
               Stmt : Node;
            begin
               Stmt := Get_Statements_Chain (Item);
               while Stmt /= Null_Node loop
                  Translate_Stmt (Stmt);
                  Stmt := Get_Chain (Stmt);
               end loop;
            end;
         when N_Goto =>
            declare
               Num : Int32;
            begin
               Num := Get_Label_Number (Get_Label (Item));
               if Num = -1 then
                  New_Exit_Stmt (Cur_Stmt_Loop);
               else
                  New_Assign_Stmt (Get_Current_Step,
                                   New_Lit (New_Unsigned_Literal
                                            (Ghdl_U32, Unsigned_64 (Num))));
                  if Get_Suspend_Flag (Item) then
                     New_Exit_Stmt (Cur_Stmt_Loop);
                  else
                     New_Next_Stmt (Cur_Stmt_Loop);
                  end if;
               end if;
            end;
            --  xx;
--           when N_Label =>
--              declare
--                 Stmt : Node;
--              begin
--                 Stmt := Get_Statements_Chain (Item);
--                 while Stmt /= Null_Node loop
--                    Translate_Stmt (Stmt);
--                    Stmt := Get_Chain (Stmt);
--                 end loop;
--              end;
         when N_Delay_Control =>
            declare
               V : O_Dnode;
               T : O_Dnode;
               If_Blk : O_If_Block;
               Assoc : O_Assoc_List;
               N_Stmt : Node;
            begin
               Open_Temp;

               V := Create_Temp (Ghdl_Logic32_Ptr);
               T := Create_Temp (Ghdl_U32);

               New_Assign_Stmt (New_Obj (V),
                                Translate_Expression (Get_Expression (Item)));
               --  If XZ, then 0.
               Start_If_Stmt
                 (If_Blk,
                  New_Compare_Op (ON_Neq,
                                  New_Value (New_Selected_Element
                                             (New_Acc_Obj (V),
                                              Ghdl_Logic32_Xz)),
                                  New_Lit (Ghdl_U32_0),
                                  Ghdl_Bool));
               New_Assign_Stmt (New_Obj (T), New_Lit (Ghdl_U32_0));
               New_Else_Stmt (If_Blk);
               New_Assign_Stmt (New_Obj (T),
                                New_Value (New_Selected_Element
                                           (New_Acc_Obj (V),
                                            Ghdl_Logic32_Val)));
               Finish_If_Stmt (If_Blk);

               Start_Association (Assoc, Ghdl_Process_Delay);
               New_Association (Assoc, New_Obj_Value (T));
               New_Procedure_Call (Assoc);
               Close_Temp;

               N_Stmt := Get_Statement (Item);
               if Get_Kind (N_Stmt) /= N_Goto then
                  raise Internal_Error;
               end if;
               New_Assign_Stmt
                 (Get_Current_Step,
                  New_Lit
                  (New_Unsigned_Literal
                   (Ghdl_U32,
                    Unsigned_64 (Get_Label_Number (Get_Label (N_Stmt))))));
               New_Return_Stmt;
            end;
         when others =>
            Error_Kind ("translate_stmt", Item);
      end case;
   end Translate_Stmt;

   procedure Translate_Always_Subprg (Item : Node; Module : Node)
   is
      Mark : Id_Mark_Type;
      Info : Info_Acc;
      Parent_Info : Info_Acc;
      Inter_List : O_Inter_List;
      Stmt, Stmt1 : Node;
      Case_Block : O_Case_Block;
   begin
      Parent_Info := Get_Info (Module);
      Info := Get_Info (Item);
      Push_Identifier_Prefix (Mark, 'S', Natural'(0));

      Linearize.Linearize_Stmt (Item);

      --  Declaration.
      Start_Procedure_Decl (Inter_List, Create_Identifier ("PROC"),
                           O_Storage_Private);
      New_Interface_Decl
        (Inter_List, Info.Stmt_Instance,
         Get_Identifier ("THIS"), Parent_Info.Module_Inst_Ptr);
      Finish_Subprogram_Decl (Inter_List, Info.Stmt_Subprg);

      --  Body.
      Parent_Info.Module_Var := Info.Stmt_Instance;
      Start_Subprogram_Body (Info.Stmt_Subprg);
      Cur_Stmt := Item;

      Stmt := Get_Statement (Item);

      if Get_Kind (Stmt) /= N_Label then
         Translate_Stmt (Stmt);
      else
         Start_Loop_Stmt (Cur_Stmt_Loop);
         Start_Case_Stmt (Case_Block, New_Value (Get_Current_Step));

         while Stmt /= Null_Node loop
            if Get_Kind (Stmt) /= N_Label then
               raise Internal_Error;
            end if;

            Start_Choice (Case_Block);
            New_Expr_Choice
              (Case_Block, New_Unsigned_Literal
               (Ghdl_U32, Unsigned_64 (Get_Label_Number (Stmt))));
            Finish_Choice (Case_Block);

            Stmt1 := Get_Chain (Stmt);
            while Stmt1 /= Null_Node loop
               Translate_Stmt (Stmt1);
               Stmt1 := Get_Chain (Stmt1);
            end loop;

            Stmt := Get_Label_Chain (Stmt);
         end loop;
         Finish_Case_Stmt (Case_Block);
         Finish_Loop_Stmt (Cur_Stmt_Loop);
      end if;

      Finish_Subprogram_Body;
      Cur_Stmt := Null_Node;
      Pop_Identifier_Prefix (Mark);
   end Translate_Always_Subprg;

   procedure Translate_Item_Subprg (Item : Node; Module : Node) is
   begin
      case Get_Kind (Item) is
         when N_Var =>
            null;
         when N_Initial
           | N_Always =>
            Translate_Always_Subprg (Item, Module);
         when others =>
            Error_Kind ("translate_item_subprg", Item);
      end case;
   end Translate_Item_Subprg;

   procedure Register_Process (Reg_Proc : O_Dnode; Item : Node; Module : Node)
   is
      Assoc : O_Assoc_List;
   begin
      Cur_Stmt := Item;
      New_Assign_Stmt (Get_Current_Step, New_Lit (Ghdl_U32_0));
      Cur_Stmt := Null_Node;

      Start_Association (Assoc, Reg_Proc);
      New_Association
        (Assoc, New_Convert_Ov (New_Value
                                (New_Obj (Get_Info (Module).Module_Var)),
                                Ghdl_Ptr));
      New_Association
        (Assoc, New_Lit (New_Subprogram_Address (Get_Info (Item).Stmt_Subprg,
                                                 Ghdl_Ptr)));
      New_Procedure_Call (Assoc);
   end Register_Process;

   procedure Translate_Item_Elab (Item : Node; Module : Node) is
   begin
      case Get_Kind (Item) is
         when N_Var =>
            --  FIXME: set to xxx
            null;
         when N_Initial =>
            Register_Process (Ghdl_Initial_Register, Item, Module);
         when N_Always =>
            Register_Process (Ghdl_Always_Register, Item, Module);
         when others =>
            Error_Kind ("translate_item_elab", Item);
      end case;
   end Translate_Item_Elab;

   --  Generate code for a module.
   procedure Translate_Module (Module : Node)
   is
      Mark : Id_Mark_Type;
      Item : Node;
      Info : Info_Acc;
      Inter_List : O_Inter_List;
      Num : Natural;
   begin
      if Get_Kind (Module) /= N_Module then
         Error_Kind ("translate_module", Module);
      end if;
      Adjust_Table;

      Info := Add_Info (Module, Kind_Module);

      Reset_Identifier_Prefix;
      Push_Identifier_Prefix (Mark, 'M', Module);

      Push_Instance_Factory (O_Tnode_Null);

      --  Create the structure.
      Item := Get_Items_Chain (Module);
      Num := 0;
      while Item /= Null_Node loop
         Translate_Decl_Item (Item, Num);
         Item := Get_Chain (Item);
         Num := Num + 1;
      end loop;

      Pop_Instance_Factory (Info.Module_Inst_Type);
      New_Type_Decl (Create_Identifier ("INSTANCE"), Info.Module_Inst_Type);
      Info.Module_Inst_Ptr := New_Access_Type (Info.Module_Inst_Type);
      New_Type_Decl (Create_Identifier ("INSTPTR"), Info.Module_Inst_Ptr);

      --  Create subprograms.
      Item := Get_Items_Chain (Module);
      while Item /= Null_Node loop
         Translate_Item_Subprg (Item, Module);
         Item := Get_Chain (Item);
      end loop;

      Start_Procedure_Decl (Inter_List, Create_Identifier ("ELAB"),
                            O_Storage_Public);
      New_Interface_Decl (Inter_List, Info.Module_Elab_Instance,
                          Get_Identifier ("INSTANCE"), Info.Module_Inst_Ptr);
      Finish_Subprogram_Decl (Inter_List, Info.Module_Elab_Subprg);

      Start_Subprogram_Body (Info.Module_Elab_Subprg);
      Info.Module_Var := Info.Module_Elab_Instance;
      Item := Get_Items_Chain (Module);
      while Item /= Null_Node loop
         Translate_Item_Elab (Item, Module);
         Item := Get_Chain (Item);
      end loop;
      Info.Module_Var := O_Dnode_Null;
      Finish_Subprogram_Body;

      Pop_Identifier_Prefix (Mark);
   end Translate_Module;

   procedure Elaborate (Module : Node)
   is
      Inter_List : O_Inter_List;
      Instance : O_Dnode;
      Info : Info_Acc;
      Assoc : O_Assoc_List;
   begin
      Start_Procedure_Decl (Inter_List, Get_Identifier ("__ghdl_ELABORATE"),
                            O_Storage_Public);
      Finish_Subprogram_Decl (Inter_List, Ghdl_Elaborate);

      --  Create body.
      Start_Subprogram_Body (Ghdl_Elaborate);
      Info := Get_Info (Module);

      --  Create instance var.
      New_Var_Decl (Instance, Get_Identifier ("INSTANCE"),
                    O_Storage_Local, Info.Module_Inst_Ptr);

      --  Allocate instance space.
      Start_Association (Assoc, Ghdl_Malloc);
      New_Association (Assoc, New_Lit (New_Sizeof (Info.Module_Inst_Type,
                                                   Ghdl_U32)));
      New_Assign_Stmt (New_Obj (Instance),
                       New_Convert_Ov (New_Function_Call (Assoc),
                                       Info.Module_Inst_Ptr));

      --  Init instance.
      Start_Association (Assoc, Info.Module_Elab_Subprg);
      New_Association (Assoc, New_Value (New_Obj (Instance)));
      New_Procedure_Call (Assoc);
      Finish_Subprogram_Body;
   end Elaborate;
end Verilog.Translate;
