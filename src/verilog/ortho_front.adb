with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_IO; use Ada.Text_IO;
with Types; use Types;
with Name_Table;
with Std_Names;
with Files_Map;
with Verilog.Scans; use Verilog.Scans;
with Verilog.Nodes; use Verilog.Nodes;
with Verilog.Errors;
with Verilog.Parse;
with Verilog.Options;
with Verilog.Disp_Verilog;
with Verilog.Disp_Tree;
with Verilog.Sem;
with Verilog.Elaborate;
with Verilog.Translate;

package body Ortho_Front is
   procedure Usage is
   begin
      Put_Line ("usage: " & Command_Name & " FILE");
   end Usage;

   procedure Init is
   begin
      Name_Table.Initialize;
      Std_Names.Std_Names_Initialize;
      Verilog.Scans.Init_Pathes;
      Verilog.Parse.Create_Basetypes;
   end Init;

   function Decode_Option (Opt : String_Acc; Arg : String_Acc) return Natural
   is
      pragma Unreferenced (Arg);
   begin
      if not Verilog.Options.Parse_Option (Opt.all) then
         Usage;
         return 0;
      end if;
      return 1;
   end Decode_Option;


   function Parse (Filename : String_Acc) return Boolean
   is
      Id : Name_Id;
      Sfe : Source_File_Entry;
      Res : Node;
      First, Last : Node;
   begin
      if Filename = null then
         Usage;
         return False;
      end if;


      First := Null_Node;
      Last := Null_Node;

      Id := Name_Table.Get_Identifier (Filename.all);
      Sfe := Files_Map.Load_Source_File (Null_Identifier, Id);
      if Sfe = No_Source_File_Entry then
         Put_Line (Command_Name & ": cannot open " & Filename.all);
         return False;
      end if;

      --  Parse file.
      Set_File (Sfe);
      Res := Verilog.Parse.Parse_File;
      if First = Null_Node then
         First := Res;
      else
         Set_Chain (Last, Res);
      end if;

      --  Sem descriptions.
      if Res /= Null_Node then
         loop
            Verilog.Sem.Sem_Description (Res);
            exit when Get_Chain (Res) = Null_Node;
            Res := Get_Chain (Res);
         end loop;
         Last := Res;
      end if;

      --  Next file.
      --N := N + 1;

      --  A verilog file can be empty.
      if First = Null_Node then
         return True;
      end if;

      Verilog.Elaborate.Elab_Design (First);
      if False then
         Verilog.Disp_Verilog.Disp_File (First);
      end if;
      if False then
         Verilog.Disp_Tree.Disp_Tree (First, 0, True);
      end if;

      Verilog.Translate.Initialize;

      Verilog.Translate.Translate_Module (First);
      Verilog.Translate.Elaborate (First);
      return True;
   exception
      when Verilog.Errors.Parse_Error =>
         return False;
   end Parse;
end Ortho_Front;

