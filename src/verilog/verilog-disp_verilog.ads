with Verilog.Nodes; use Verilog.Nodes;

package Verilog.Disp_Verilog is
   --  Disp implicit cast.
   Flag_Disp_Implicit_Cast : Boolean := False;

   --  Disp implicit types as if it were explicit.
   Flag_Disp_Implicit_Type : Boolean := False;

   --  Disp omitted part in port declarations (direction, data_type, kind).
   Flag_Disp_Port_Omitted : Boolean := False;

   procedure Disp_Item (Item : Node);
   procedure Disp_Module (M : Node; Indent : Natural := 0);
   procedure Disp_Source (Source : Node);

   procedure Disp_One_Net_Declaration (Indent : Natural; Net : Node);
   procedure Disp_Expression (Expr : Node);
   procedure Disp_Control (Ctrl : Node);
   procedure Disp_If_Header (Stmt : Node);
   procedure Disp_While_Header (Stmt : Node);
   procedure Disp_For_Header (Stmt : Node);
   procedure Disp_Case_Header (Stmt : Node);
   procedure Disp_Blocking_Assignment (Stmt : Node);
   procedure Disp_Continuous_Assignment (Stmt : Node);
   procedure Disp_Non_Blocking_Assignment (Stmt : Node);
end Verilog.Disp_Verilog;
