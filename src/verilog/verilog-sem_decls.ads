with Verilog.Nodes; use Verilog.Nodes;

package Verilog.Sem_Decls is
   --  Analyze type of declarations, typedef, parameters and type of classes.
   --  Has to deal with forward declarations.

   procedure Sem_Decl_Type (Decl : Node);
   procedure Sem_Decl_Type_Chain (Chain : Node);

   procedure Sem_Class_Type (Klass : Node);

   procedure Sem_Typedef_Type (Def : Node);

   --  For list of identifiers: they share the same type.
   --  Used by Sem_Types to analyze structure and union members.
   procedure Sem_Decl_List_Data_Type (Head : Node);

   --  Analyze the type of DECL.
   procedure Sem_Decl_Data_Type (Decl : Node);

   procedure Sem_Tf_Ports (Tf_Decl : Node);
end Verilog.Sem_Decls;
