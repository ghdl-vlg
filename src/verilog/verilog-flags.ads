package Verilog.Flags is
   type Standard_Type is (Verilog_Xl, Verilog_Ovi, Verilog_1995,
                          Verilog_2001,
                          Verilog_2005,
                          Verilog_Sv_3_0,
                          Verilog_Sv_3_1,
                          Verilog_Sv_3_1a,
                          Verilog_Sv2005,
                          Verilog_Sv2009,
                          Verilog_Sv2012,
                          Verilog_Sv2017);

   --  The verilog standard.
   Std : Standard_Type := Verilog_Sv2017;

   subtype Verilog_Standard is Standard_Type range Verilog_Xl .. Verilog_2005;
   subtype Systemverilog_Standard is
     Standard_Type range Verilog_Sv_3_0 .. Verilog_Sv2017;

   --  If true, the parser create a node for parentheses.
   Flag_Keep_Parentheses : Boolean := False;
end Verilog.Flags;
