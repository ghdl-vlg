with Types; use Types;
with Verilog.Nodes; use Verilog.Nodes;

package Verilog.Sem_Eval is
   --  Analyze and evaluate expressions.

   --  Used for enumeration literals, parameters...
   function Sem_Constant_Expression (Expr : Node; Atype : Node) return Node;

   --  Used for array bounds, repetitions.
   function Sem_Constant_Integer_Expression (Expr : Node) return Int32;
end Verilog.Sem_Eval;
