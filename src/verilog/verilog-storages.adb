package body Verilog.Storages is
   function "+" (Data : Data_Ptr; Off : Storage_Index) return Data_Ptr is
   begin
      return To_Storage (Data)(Off)'Address;
   end "+";

   function Is_Null (Data : Data_Ptr) return Boolean
   is
      use System;
   begin
      return Data = Null_Address;
   end Is_Null;
end Verilog.Storages;
