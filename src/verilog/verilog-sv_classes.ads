with System;
with Ada.Unchecked_Conversion;
with Types; use Types;
with Verilog.Nodes;
with Verilog.Storages; use Verilog.Storages;

package Verilog.Sv_Classes is
   type Sv_Class_Type is record
      --  Important: like a subroutine frame, the first word is orig.
      Orig : Nodes.Node;
      Refcnt : Uns32;
      Vtable : System.Address;
   end record;

   --  Like frames.
   Class_Link_Size   : constant Storage_Index := Sv_Class_Type'Size / 8;
   Class_Link_Align  : constant Storage_Index := Sv_Class_Type'Alignment;
   Class_Link_Offset : constant Storage_Index := 0;

   type Sv_Class_Handle is access all Sv_Class_Type;
   type Sv_Class_Ptr is access all Sv_Class_Handle;

   function To_Sv_Class_Ptr is
      new Ada.Unchecked_Conversion (System.Address, Sv_Class_Ptr);

   function To_Frame_Ptr is new Ada.Unchecked_Conversion
     (Sv_Class_Handle, Verilog.Storages.Frame_Ptr);
   function To_Sv_Class_Handle is new Ada.Unchecked_Conversion
     (Verilog.Storages.Frame_Ptr, Sv_Class_Handle);
end Verilog.Sv_Classes;
