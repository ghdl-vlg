with Verilog.Nodes; use Verilog.Nodes;

package Verilog.Sem_Stmts is
   procedure Sem_Statement (Stmt : Node);
   procedure Sem_Statement_Or_Null (Stmt : Node);
   procedure Sem_Statements (Parent : Node);
   procedure Sem_Subroutine_Statements (Rtn : Node);

   --  Analyze variables of a foreach-loop or foreach constraint_expression.
   procedure Sem_Foreach_Variables (Stmt : Node);

   procedure Sem_System_Function_Call (Expr : Node; Etype : Node);
end Verilog.Sem_Stmts;
