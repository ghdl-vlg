with Types; use Types;
with Verilog.Types; use Verilog.Types;
with Verilog.Allocates; use Verilog.Allocates;
with Verilog.Nodes; use Verilog.Nodes;
with Verilog.Storages; use Verilog.Storages;

package Verilog.Simulation is
   --  Get current simulation time.
   function Get_Current_Time return Uns32;

   procedure Activate_Process (Proc : Process_Acc);

   procedure Execute_Statements
     (Link : in out Frame_Link_Type; Proc : Process_Acc);

   --  Exported for VPI.
   procedure Blocking_Assign_Lvalue
     (Frame : Frame_Ptr; Tgt : Node; Val : Data_Ptr; Etype : Node);

   procedure Assign_Vector (Dest : Data_Ptr;
                            Dest_Offset : Bit_Offset;
                            Dest_Width : Width_Type;
                            Dest_Type : Node;
                            Update : Update_Acc;
                            Val : Data_Ptr;
                            Voffset : Bit_Offset);

   --  Display values (reg and nets) initially and after each cycles.
   Flag_Trace_Values : Boolean := False;

   --  Include memories while displaying values.
   Flag_Trace_Memories : Boolean := False;

   Flag_Trace_Time : Boolean := False;

   Flag_Trace_Exec : Boolean := False;

   --  For $settrace/$cleartrace
   Flag_Trace : Boolean := False;

   procedure Run (Root : Node);
end Verilog.Simulation;
