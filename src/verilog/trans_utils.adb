with Ada.Unchecked_Deallocation;
with Name_Table;

package body Trans_Utils is
   --  Identifiers.
   --  The following functions are helpers to create ortho identifiers.
   Identifier_Buffer : String (1 .. 512);
   Identifier_Start : Natural := 1;
   Identifier_Local : Local_Identifier_Type := 0;


   Inst_Build : Inst_Build_Acc := null;
   procedure Unchecked_Deallocation is new Ada.Unchecked_Deallocation
     (Object => Inst_Build_Type, Name => Inst_Build_Acc);

   procedure Set_Global_Storage (Storage : O_Storage) is
   begin
      Global_Storage := Storage;
   end Set_Global_Storage;

   procedure Pop_Build_Instance
   is
      Old : Inst_Build_Acc;
   begin
      Old := Inst_Build;
      Identifier_Start := Old.Prev_Id_Start;
      Inst_Build := Old.Prev;
      Unchecked_Deallocation (Old);
   end Pop_Build_Instance;

   --       procedure Push_Global_Factory (Storage : O_Storage)
   --       is
   --          Inst : Inst_Build_Acc;
   --       begin
   --          if Inst_Build /= null then
   --             raise Internal_Error;
   --          end if;
   --          Inst := new Inst_Build_Type (Global);
   --          Inst.Prev := Inst_Build;
   --          Inst_Build := Inst;
   --          Global_Storage := Storage;
   --       end Push_Global_Factory;

   --       procedure Pop_Global_Factory is
   --       begin
   --          if Inst_Build.Kind /= Global then
   --             raise Internal_Error;
   --          end if;
   --          Pop_Build_Instance;
   --          Global_Storage := O_Storage_Private;
   --       end Pop_Global_Factory;

   procedure Push_Instance_Factory (Instance_Type : O_Tnode)
   is
      Inst : Inst_Build_Acc;
   begin
      if Inst_Build /= null and then Inst_Build.Kind /= Instance then
         raise Internal_Error;
      end if;
      Inst := new Inst_Build_Type (Instance);
      Inst.Prev := Inst_Build;

      Inst.Prev_Id_Start := Identifier_Start;
      Identifier_Start := Identifier_Len + 1;

      if Instance_Type /= O_Tnode_Null then
         Start_Uncomplete_Record_Type (Instance_Type, Inst.Elements);
      else
         Start_Record_Type (Inst.Elements);
      end if;
      Inst.Vars := null;
      Inst_Build := Inst;
   end Push_Instance_Factory;

   function Add_Instance_Factory_Field (Name : O_Ident; Ftype : O_Tnode)
                                       return O_Fnode
   is
      Res : O_Fnode;
   begin
      New_Record_Field (Inst_Build.Elements, Res, Name, Ftype);
      return Res;
   end Add_Instance_Factory_Field;

   procedure Pop_Instance_Factory (Instance_Type : out O_Tnode)
   is
      Res : O_Tnode;
      V : Var_Acc;
   begin
      if Inst_Build.Kind /= Instance then
         --  Not matching.
         raise Internal_Error;
      end if;
      Finish_Record_Type (Inst_Build.Elements, Res);
      --  Set type of all variable declared in this instance.
      V := Inst_Build.Vars;
      while V /= null loop
         V.I_Type := Res;
         V := V.I_Link;
      end loop;
      Pop_Build_Instance;
      Instance_Type := Res;
   end Pop_Instance_Factory;

   procedure Push_Local_Factory
   is
      Inst : Inst_Build_Acc;
   begin
      if Inst_Build /= null
        and then (Inst_Build.Kind /= Global and Inst_Build.Kind /= Local)
      then
         --  Cannot create a local factory on an instance.
         raise Internal_Error;
      end if;
      Inst := new Inst_Build_Type (Kind => Local);
      Inst.Prev := Inst_Build;
      Inst.Prev_Global_Storage := Global_Storage;

      Inst.Prev_Id_Start := Identifier_Start;
      Identifier_Start := Identifier_Len + 1;

      Inst_Build := Inst;
      case Global_Storage is
         when O_Storage_Public =>
            Global_Storage := O_Storage_Private;
         when O_Storage_Private
           | O_Storage_External =>
            null;
         when O_Storage_Local =>
            raise Internal_Error;
      end case;
   end Push_Local_Factory;

   --  Return TRUE is the current scope is local.
   function Is_Local_Scope return Boolean is
   begin
      if Inst_Build = null then
         return False;
      end if;
      case Inst_Build.Kind is
         when Local
           | Instance =>
            return True;
         when Global =>
            return False;
      end case;
   end Is_Local_Scope;

   procedure Pop_Local_Factory is
   begin
      if Inst_Build.Kind /= Local then
         --  Not matching.
         raise Internal_Error;
      end if;
      Global_Storage := Inst_Build.Prev_Global_Storage;
      Pop_Build_Instance;
   end Pop_Local_Factory;

   type Scope_Type;
   type Scope_Acc is access Scope_Type;
   type Scope_Type is record
      Is_Ptr : Boolean;
      Stype : O_Tnode;
      Field : O_Fnode;
      Parent : O_Tnode;
      Prev : Scope_Acc;
   end record;
   type Scope_Var_Type;
   type Scope_Var_Acc is access Scope_Var_Type;
   type Scope_Var_Type is record
      Svtype : O_Tnode;
      Var : O_Dnode;
      Prev : Scope_Var_Acc;
   end record;

   Scopes : Scope_Acc := null;
   --  Chained list of unused scopes, in order to reduce number of
   --  dynamic allocation.
   Scopes_Old : Scope_Acc := null;

   Scopes_Var : Scope_Var_Acc := null;
   --  Chained list of unused var_scopes, to reduce number of allocations.
   Scopes_Var_Old : Scope_Var_Acc := null;

   --  Get a scope, either from the list of free scope or by allocation.
   function Get_A_Scope return Scope_Acc is
      Res : Scope_Acc;
   begin
      if Scopes_Old /= null then
         Res := Scopes_Old;
         Scopes_Old := Scopes_Old.Prev;
      else
         Res := new Scope_Type;
      end if;
      return Res;
   end Get_A_Scope;

   procedure Push_Scope (Scope_Type : O_Tnode;
                         Scope_Field : O_Fnode; Scope_Parent : O_Tnode)
   is
      Res : Scope_Acc;
   begin
      Res := Get_A_Scope;
      Res.all := (Is_Ptr => False,
                  Stype => Scope_Type,
                  Field => Scope_Field,
                  Parent => Scope_Parent,
                  Prev => Scopes);
      Scopes := Res;
   end Push_Scope;

   procedure Push_Scope_Via_Field_Ptr
     (Scope_Type : O_Tnode;
      Scope_Field : O_Fnode; Scope_Parent : O_Tnode)
   is
      Res : Scope_Acc;
   begin
      Res := Get_A_Scope;
      Res.all := (Is_Ptr => True,
                  Stype => Scope_Type,
                  Field => Scope_Field,
                  Parent => Scope_Parent,
                  Prev => Scopes);
      Scopes := Res;
   end Push_Scope_Via_Field_Ptr;

   procedure Push_Scope (Scope_Type : O_Tnode; Scope_Param : O_Dnode)
   is
      Res : Scope_Var_Acc;
   begin
      if Scopes_Var_Old /= null then
         Res := Scopes_Var_Old;
         Scopes_Var_Old := Res.Prev;
      else
         Res := new Scope_Var_Type;
      end if;
      Res.all := (Svtype => Scope_Type,
                  Var => Scope_Param,
                  Prev => Scopes_Var);
      Scopes_Var := Res;
   end Push_Scope;

   procedure Pop_Scope (Scope_Type : O_Tnode)
   is
      Old : Scope_Acc;
      Var_Old : Scope_Var_Acc;
   begin
      --  Search in var scope.
      if Scopes_Var /= null and then Scopes_Var.Svtype = Scope_Type then
         Var_Old := Scopes_Var;
         Scopes_Var := Var_Old.Prev;
         Var_Old.Prev := Scopes_Var_Old;
         Scopes_Var_Old := Var_Old;
      elsif Scopes.Stype /= Scope_Type then
         --  Bad pop order.
         raise Internal_Error;
      else
         Old := Scopes;
         Scopes := Old.Prev;
         Old.Prev := Scopes_Old;
         Scopes_Old := Old;
      end if;
   end Pop_Scope;

   procedure Push_Scope_Soft (Scope_Type : O_Tnode; Scope_Param : O_Dnode)
   is
   begin
      if Scope_Type /= O_Tnode_Null then
         Push_Scope (Scope_Type, Scope_Param);
      end if;
   end Push_Scope_Soft;

   procedure Pop_Scope_Soft (Scope_Type : O_Tnode)
   is
   begin
      if Scope_Type /= O_Tnode_Null then
         Pop_Scope (Scope_Type);
      end if;
   end Pop_Scope_Soft;

   function Create_Global_Var
     (Name : O_Ident; Vtype : O_Tnode; Storage : O_Storage)
     return Var_Acc
   is
      Var : O_Dnode;
   begin
      New_Var_Decl (Var, Name, Storage, Vtype);
      return new Var_Type'(Kind => Var_Global, E => Var);
   end Create_Global_Var;

   function Create_Global_Const
     (Name : O_Ident;
      Vtype : O_Tnode;
      Storage : O_Storage;
      Initial_Value : O_Cnode)
     return Var_Acc
   is
      Res : O_Dnode;
   begin
      New_Const_Decl (Res, Name, Storage, Vtype);
      if Storage /= O_Storage_External
        and then Initial_Value /= O_Cnode_Null
      then
         Start_Const_Value (Res);
         Finish_Const_Value (Res, Initial_Value);
      end if;
      return new Var_Type'(Kind => Var_Global, E => Res);
   end Create_Global_Const;

   procedure Define_Global_Const (Const : Var_Acc; Val : O_Cnode) is
   begin
      Start_Const_Value (Const.E);
      Finish_Const_Value (Const.E, Val);
   end Define_Global_Const;

   function Create_Var
     (Name : O_Ident;
      Vtype : O_Tnode;
      Storage : O_Storage := Global_Storage)
     return Var_Acc
   is
      Res : O_Dnode;
      Field : O_Fnode;
      V : Var_Acc;
      K : Inst_Build_Kind_Type;
   begin
      if Inst_Build = null then
         K := Global;
      else
         K := Inst_Build.Kind;
      end if;
      case K is
         when Global =>
            --  The global scope is in use...
            return Create_Global_Var (Name, Vtype, Storage);
         when Local =>
            --  It is always possible to create a variable in a local scope.
            --  Create a var.
            New_Var_Decl (Res, Name, O_Storage_Local, Vtype);
            return new Var_Type'(Kind => Var_Local, E => Res);
         when Instance =>
            --  Create a field.
            New_Record_Field (Inst_Build.Elements, Field, Name, Vtype);
            V := new Var_Type'(Kind => Var_Scope, I_Field => Field,
                               I_Type => O_Tnode_Null,
                               I_Link => Inst_Build.Vars);
            Inst_Build.Vars := V;
            return V;
      end case;
   end Create_Var;

   function Find_Scope_Type (Stype : O_Tnode) return O_Lnode
   is
      S : Scope_Acc;
      Sv : Scope_Var_Acc;
   begin
      --  Find in var.
      Sv := Scopes_Var;
      while Sv /= null loop
         if Sv.Svtype = Stype then
            return New_Access_Element (New_Value (New_Obj (Sv.Var)));
         end if;
         Sv := Sv.Prev;
      end loop;

      --  Find in fields.
      S := Scopes;
      while S /= null loop
         if S.Stype = Stype then
            if S.Is_Ptr then
               return New_Access_Element
                 (New_Value
                  (New_Selected_Element (Find_Scope_Type (S.Parent),
                                         S.Field)));
            else
               return New_Selected_Element
                 (Find_Scope_Type (S.Parent), S.Field);
            end if;
         end if;
         S := S.Prev;
      end loop;

      --  Not found.
      raise Internal_Error;
   end Find_Scope_Type;

--     function Get_Instance_Access (Block : Iir) return O_Enode
--     is
--        Info : Block_Info_Acc;
--     begin
--        Info := Get_Info (Block);
--        if Info.Block_Decls_Type = Scopes_Var.Svtype then
--           return New_Value (New_Obj (Scopes_Var.Var));
--        else
--           return New_Address (Get_Instance_Ref (Info.Block_Decls_Type),
--                               Info.Block_Decls_Ptr_Type);
--        end if;
--     end Get_Instance_Access;

   function Get_Instance_Ref (Itype : O_Tnode) return O_Lnode
   is
   begin
      --  Variables cannot be referenced if there is an instance being
      --  built.
      if Inst_Build /= null and then Inst_Build.Kind = Instance then
         raise Internal_Error;
      end if;
      return Find_Scope_Type (Itype);
   end Get_Instance_Ref;

   function Get_Var (Var : Var_Acc) return O_Lnode
   is
   begin
      case Var.Kind is
         when Var_Local
           | Var_Global =>
            return New_Obj (Var.E);
         when Var_Scope =>
            null;
      end case;

      return New_Selected_Element (Get_Instance_Ref (Var.I_Type),
                                   Var.I_Field);
   end Get_Var;

   function Get_Alloc_Kind_For_Var (Var : Var_Acc) return Allocation_Kind is
   begin
      case Var.Kind is
         when Var_Local =>
            return Alloc_Stack;
         when Var_Global
           | Var_Scope =>
            return Alloc_System;
      end case;
   end Get_Alloc_Kind_For_Var;

   function Is_Var_Stable (Var : Var_Acc) return Boolean is
   begin
      case Var.Kind is
         when Var_Local
           | Var_Global =>
            return True;
         when Var_Scope =>
            return False;
      end case;
   end Is_Var_Stable;

   function Is_Var_Field (Var : Var_Acc) return Boolean is
   begin
      case Var.Kind is
         when Var_Local
           | Var_Global =>
            return False;
         when Var_Scope =>
            return True;
      end case;
   end Is_Var_Field;

   function Get_Var_Field (Var : Var_Acc) return O_Fnode is
   begin
      case Var.Kind is
         when Var_Local
           | Var_Global =>
            raise Internal_Error;
         when Var_Scope =>
            return Var.I_Field;
      end case;
   end Get_Var_Field;

   function Get_Var_Label (Var : Var_Acc) return O_Dnode is
   begin
      case Var.Kind is
         when Var_Local
           | Var_Global =>
            return Var.E;
         when Var_Scope =>
            raise Internal_Error;
      end case;
   end Get_Var_Label;

   procedure Save_Local_Identifier (Id : out Local_Identifier_Type) is
   begin
      Id := Identifier_Local;
   end Save_Local_Identifier;

   procedure Restore_Local_Identifier (Id : Local_Identifier_Type) is
   begin
      if Identifier_Local > Id then
         --  If the value is restored with a smaller value, some identifiers
         --  will be reused.  This is certainly an internal error.
         raise Internal_Error;
      end if;
      Identifier_Local := Id;
   end Restore_Local_Identifier;

   --  Reset the identifier.
   procedure Reset_Identifier_Prefix is
   begin
      if Identifier_Len /= 0 or else Identifier_Local /= 0 then
         raise Internal_Error;
      end if;
   end Reset_Identifier_Prefix;

   procedure Pop_Identifier_Prefix (Mark : in Id_Mark_Type) is
   begin
      Identifier_Len := Mark.Len;
      Identifier_Local := Mark.Local_Id;
   end Pop_Identifier_Prefix;

   procedure Append_Identifier_Prefix (C : Character) is
   begin
      Identifier_Len := Identifier_Len + 1;
      Identifier_Buffer (Identifier_Len) := C;
   end Append_Identifier_Prefix;

   procedure Append_Identifier_Prefix (S : String) is
   begin
      Identifier_Buffer (Identifier_Len + 1 .. Identifier_Len + S'Length) := S;
      Identifier_Len := Identifier_Len + S'Length;
   end Append_Identifier_Prefix;

   procedure Add_String (Len : in out Natural; Str : String)
   is
   begin
      Identifier_Buffer (Len + 1 .. Len + Str'Length) := Str;
      Len := Len + Str'Length;
   end Add_String;

   procedure Add_Nat (Len : in out Natural; Val : Natural)
   is
      Num : String (1 .. 10);
      V : Natural;
      P : Natural;
   begin
      P := Num'Last;
      V := Val;
      loop
         Num (P) := Character'Val (Character'Pos ('0') + V mod 10);
         V := V / 10;
         exit when V = 0;
         P := P - 1;
      end loop;
      Add_String (Len, Num (P .. Num'Last));
   end Add_Nat;

   procedure Append_Identifier_Prefix (N : Natural) is
   begin
      Add_Nat (Identifier_Len, N);
   end Append_Identifier_Prefix;

   procedure Append_Identifier_Prefix_Uniq is
   begin
      Add_Nat (Identifier_Len, Natural (Identifier_Local));
      Identifier_Local := Identifier_Local + 1;
   end Append_Identifier_Prefix_Uniq;


   type Hexstr_Type is array (Integer range 0 .. 15) of Character;
   N2hex : constant Hexstr_Type := "0123456789abcdef";

   --  Convert name_id NAME to a string stored to
   --  NAME_BUFFER (1 .. NAME_LENGTH).
   --
   --  This encodes extended identifiers.
   --
   --  Extended identifier encoding:
   --  They start with 'X'.
   --  Non extended character [0-9a-zA-Z] are left as is,
   --  others are encoded to _XX, where XX is the character position in hex.
   --  They finish with "__".
   procedure Name_Id_To_String (Name : Name_Id)
   is
      use Name_Table;

      type Bool_Array_Type is array (Character) of Boolean;
      pragma Pack (Bool_Array_Type);
      Is_Extended_Char : constant Bool_Array_Type :=
        ('0' .. '9' | 'A' .. 'Z' | 'a' .. 'z' => False,
         others => True);

      N_Len : Natural;
      P : Natural;
      C : Character;
   begin
      if Is_Character (Name) then
         P := Character'Pos (Name_Table.Get_Character (Name));
         Name_Buffer (1) := 'C';
         Name_Buffer (2) := N2hex (P / 16);
         Name_Buffer (3) := N2hex (P mod 16);
         Name_Length := 3;
         return;
      else
         Image (Name);
      end if;
      if Name_Buffer (1) /= '\' then
         return;
      end if;
      --  Extended identifier.
      --  Supress trailing backslash.
      Name_Length := Name_Length - 1;

      --  Count number of characters in the extended string.
      N_Len := Name_Length;
      for I in 2 .. Name_Length loop
         if Is_Extended_Char (Name_Buffer (I)) then
            N_Len := N_Len + 2;
         end if;
      end loop;

      --  Convert.
      Name_Buffer (1) := 'X';
      P := N_Len;
      for J in reverse 2 .. Name_Length loop
         C := Name_Buffer (J);
         if Is_Extended_Char (C) then
            Name_Buffer (P - 0) := N2hex (Character'Pos (C) mod 16);
            Name_Buffer (P - 1) := N2hex (Character'Pos (C) / 16);
            Name_Buffer (P - 2) := '_';
            P := P - 3;
         else
            Name_Buffer (P) := C;
            P := P - 1;
         end if;
      end loop;
      Name_Buffer (N_Len + 1) := '_';
      Name_Buffer (N_Len + 2) := '_';
      Name_Length := N_Len + 2;
   end Name_Id_To_String;

   procedure Save_Mark (Mark : out Id_Mark_Type) is
   begin
      Mark.Len := Identifier_Len;
      Mark.Local_Id := Identifier_Local;
   end Save_Mark;

   procedure Push_Identifier_Prefix (Mark : out Id_Mark_Type;
                                     Name : String;
                                     Val : Iir_Int32 := 0)
   is
      P : Natural;
   begin
      Save_Mark (Mark);
      Identifier_Local := 0;
      P := Identifier_Len;
      Add_String (P, Name);
      if Val > 0 then
         Add_String (P, "O");
         Add_Nat (P, Natural (Val));
      end if;
      Add_String (P, "__");
      Identifier_Len := P;
   end Push_Identifier_Prefix;

   --  Add a suffix to the prefix (!!!).
   procedure Push_Identifier_Prefix
     (Mark : out Id_Mark_Type; Name : Name_Id; Val : Iir_Int32 := 0)
   is
      use Name_Table;
   begin
      Name_Id_To_String (Name);
      Push_Identifier_Prefix (Mark, Name_Buffer (1 .. Name_Length), Val);
   end Push_Identifier_Prefix;

   --       procedure Add_Local_Identifier (Len : in out Natural)
   --       is
   --          Str : String := Local_Identifier_Type'Image (Identifier_Local);
   --       begin
   --          Identifier_Local := Identifier_Local + 1;

   --          if Inst_Build = null then
   --             Str (1) := 'N';
   --          else
   --             case Inst_Build.Kind is
   --                when Local =>
   --                   Str (1) := 'L';
   --                when Global =>
   --                   Str (1) := 'G';
   --                when Instance =>
   --                   Str (1) := 'I';
   --             end case;
   --          end if;
   --          Add_String (Len, Str);
   --       end Add_Local_Identifier;

   procedure Push_Identifier_Prefix_Uniq (Mark : out Id_Mark_Type)
   is
      Str : String := Local_Identifier_Type'Image (Identifier_Local);
   begin
      Identifier_Local := Identifier_Local + 1;
      Str (1) := 'U';
      Push_Identifier_Prefix (Mark, Str, 0);
   end Push_Identifier_Prefix_Uniq;

--     --  Create an identifier from IIR node ID without the prefix.
--     function Create_Identifier_Without_Prefix (Id : Iir) return O_Ident
--     is
--        use Name_Table;
--     begin
--        Name_Id_To_String (Get_Identifier (Id));
--        return Get_Identifier (Name_Buffer (1 .. Name_Length));
--     end Create_Identifier_Without_Prefix;

   function Create_Identifier_Without_Prefix (Id : Name_Id; Str : String)
                                             return O_Ident
   is
      use Name_Table;
   begin
      Name_Id_To_String (Id);
      Name_Buffer (Name_Length + 1 .. Name_Length + Str'Length) := Str;
      return Get_Identifier (Name_Buffer (1 .. Name_Length + Str'Length));
   end Create_Identifier_Without_Prefix;

   function Get_Identifier_Mark return Natural is
   begin
      return Identifier_Len;
   end Get_Identifier_Mark;

   function Create_Var_Identifier (Mark : Natural) return O_Ident
   is
      Res : O_Ident;
   begin
      --Identifier_Buffer (L + Str'Length + 1) := Nul;
      if Is_Local_Scope then
         Res := Get_Identifier
           (Identifier_Buffer (Mark + 1 .. Identifier_Len - 1));
      else
         Res := Get_Identifier (Identifier_Buffer (1 .. Identifier_Len - 1));
      end if;
      Identifier_Len := Mark;
      return Res;
   end Create_Var_Identifier;

   function Create_Identifier (Id : Name_Id; Str : String := "")
                              return O_Ident
   is
      use Name_Table;
      M : Natural;
      Res : O_Ident;
   begin
      M := Get_Identifier_Mark;
      Name_Id_To_String (Id);
      Append_Identifier_Prefix (Name_Buffer (1 .. Name_Length));
      Append_Identifier_Prefix (Str);
      Res := Get_Identifier (Identifier_Buffer (1 .. Identifier_Len));
      Identifier_Len := M;
      return Res;
   end Create_Identifier;

--     function Create_Identifier (Id : Iir; Str : String := "")
--                                return O_Ident
--     is
--     begin
--        return Create_Id (Get_Identifier (Id), Str, False);
--     end Create_Identifier;

--     function Create_Identifier
--       (Id : Iir; Val : Iir_Int32; Str : String := "")
--       return O_Ident
--     is
--        Len : Natural;
--     begin
--        Len := Identifier_Len;
--        Add_Identifier (Len, Get_Identifier (Id));

--        if Val > 0 then
--           Add_String (Len, "O");
--           Add_Nat (Len, Natural (Val));
--        end if;
--        Add_String (Len, Str);
--        return Get_Identifier (Identifier_Buffer (1 .. Len));
--     end Create_Identifier;

   function Create_Identifier (Str : String)
                              return O_Ident
   is
      Len : Natural;
   begin
      Len := Identifier_Len;
      Add_String (Len, Str);
      return Get_Identifier (Identifier_Buffer (1 .. Len));
   end Create_Identifier;

   function Create_Identifier return O_Ident
   is
   begin
      return Get_Identifier (Identifier_Buffer (1 .. Identifier_Len - 2));
   end Create_Identifier;

--     function Create_Var_Identifier (Id : Iir)
--                                    return Var_Ident_Type
--     is
--        Res : Var_Ident_Type;
--     begin
--        Res.Id := Create_Id (Get_Identifier (Id), "", Is_Local_Scope);
--        return Res;
--     end Create_Var_Identifier;

   function Create_Var_Identifier (Id : String)
                                  return O_Ident
   is
      use Name_Table;
      M : Natural;
      Res : O_Ident;
   begin
      M := Get_Identifier_Mark;
      Append_Identifier_Prefix (Id);
      Res := Get_Identifier (Identifier_Buffer (1 .. Identifier_Len));
      Identifier_Len := M;
      return Res;
   end Create_Var_Identifier;

--     function Create_Uniq_Identifier return Var_Ident_Type
--     is
--        Res : Var_Ident_Type;
--     begin
--        Res.Id := Create_Uniq_Identifier;
--        return Res;
--     end Create_Uniq_Identifier;
end Trans_Utils;
