catch exception

define pt
call verilog.disp_tree.disp_tree ($arg0, 0, 10)
end

define vlg_pt1
call verilog.disp_tree.disp_tree ($arg0, 0, 2)
end

define pt1
call verilog.disp_tree.disp_tree ($arg0, 0, 2)
end

document pt
Print the structure of the iirs that is $arg0.
end

define ptf
call verilog.disp_tree.disp_tree ($arg0, 0, 0)
end

document ptf
Print the iirs that is $arg0.
end

set startup-with-shell 0
