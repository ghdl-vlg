package Verilog.Options is
   --  Parse option OPT; return FALSE if not known.
   function Parse_Option (Option : String) return Boolean;
end Verilog.Options;
