with Dyn_Tables;
with Types; use Types;
with Verilog.Types; use Verilog.Types;

package Verilog.Bn_Tables is
   package Bignum_Table is new Dyn_Tables
     (Table_Component_Type => Logic_32,
      Table_Index_Type => Bn_Index,
      Table_Low_Bound => No_Bn_Index + 1);

   Bn_Table : Bignum_Table.Instance;
end Verilog.Bn_Tables;
