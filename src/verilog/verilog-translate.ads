with Verilog.Nodes; use Verilog.Nodes;
with Ortho_Nodes; use Ortho_Nodes;

package Verilog.Translate is
   Ghdl_Systask_Display : O_Dnode;
   Ghdl_Systask_Finish : O_Dnode;

   Ghdl_Malloc : O_Dnode;

   Ghdl_Initial_Register : O_Dnode;
   Ghdl_Always_Register : O_Dnode;

   Ghdl_Elaborate : O_Dnode;

   Ghdl_Process_Delay : O_Dnode;

   --  Create the built-in types and subprograms.
   procedure Initialize;

   --  Generate code for a module.
   procedure Translate_Module (Module : Node);

   --  Generate the main procedure.
   procedure Elaborate (Module : Node);
end Verilog.Translate;
