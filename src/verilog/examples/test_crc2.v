module CRC5D1_test_bench;
    
		parameter integer initial_value=-1;
		parameter integer poly_width=5;
		parameter integer maximum_no_of_errors=3;
		parameter integer data_length=10;
		
		reg data;
		reg clock=0;
		reg sync_reset=1;
		reg hold=0, receiver_hold=0;
		wire [poly_width-1:0] crc_sender,crc_receiver;
		reg done=0;
		reg error=0;
		integer i,test_loops,error_counter;
		wire receive_data=data^ error;
		
	
CRC5_D1_Module #(.initial_value(initial_value),.poly_width(poly_width)) crc_tx( .clock (clock),
  						    .data(data),.sync_reset(sync_reset),.hold(hold),
							   .crc(crc_sender));

CRC5_D1_Module #(.initial_value(initial_value),.poly_width(poly_width)) crc_rx( .clock(clock),
						    .data(receive_data),.sync_reset(sync_reset),.hold(receiver_hold),
						   .crc(crc_receiver));


	always #10 clock=~clock;

	initial begin
			#5;
			test_loops=0;
//Test 1 Make sure no crc error occurs
			repeat (100) begin
				test_loops=test_loops+1;
				if (test_loops%100==0) $display("Test Loops=%d",test_loops);
				
				sync_reset=1; 
				@(negedge clock);
				@(negedge clock);
				sync_reset=0; hold=0;
				for (i=0;i<data_length;i=i+1) begin
					data=$random;
					@(negedge clock);
				end
				hold=1;
				for (i=0;i<poly_width;i=i+1) begin
				  data=crc_sender[poly_width-1-i];
					@(negedge clock);
				end
				done=1;
				if (crc_receiver !==0) $display("Miss Detection or Programming Error");
				@(negedge clock);
				done=0;
			end
//Make sure crc error occurs
			test_loops=0;
			repeat (100) begin
				test_loops=test_loops+1;
				if (test_loops%1000==0) $display("Test Loops=%d",test_loops);
			
				sync_reset=1; 
				@(negedge clock);
				@(negedge clock);
				sync_reset=0; hold=0;error_counter=0;
				for (i=0;i<data_length;i=i+1) begin
					data=$random;
					if (i==0) begin
						error=1;//We add at least one error. 
						error_counter=error_counter+1;
					end else begin//Limit maxinum no of errors
						if (error_counter <maximum_no_of_errors) begin
							error=$random;//$random error addition
							error_counter=error_counter+1;
						end
					end
					@(negedge clock);
				end
				hold=1;//Stop CRC generation in sender
				for (i=0;i<poly_width;i=i+1) begin
				  	data=crc_sender[poly_width-1-i];//CRC polynominal are sent
					@(negedge clock);
				end
				done=1;
				if (crc_receiver ==0) $display("Miss Error Detection or Programming Error Test Loops=%d",test_loops);
				@(negedge clock);
				done=0;
			end

			$finish;
	end
endmodule
