module counter_tb;
  reg clk, reset, enable; 
  wire [3:0] count; 
    
  counter U0 ( 
  .clk    (clk), 
  .reset  (reset), 
  .enable (enable), 
  .count  (count) 
  ); 

  initial begin
     reset = 1;
     enable = 1;
     clk = 0;
     # 4;
     reset = 0;
  end

   always
     # 2 clk = ~clk;

   initial begin
      # 13
	if (count != 2)
	  $display ("FAIL");
	else
	  $display ("PASS");
      $stop;
   end
endmodule 
