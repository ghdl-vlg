with Ada.Unchecked_Deallocation;

package body Verilog.Sv_Arrays is
   procedure Delete (Arr : in out Sv_Dyn_Array_Ptr)
   is
      procedure Free is new Ada.Unchecked_Deallocation
        (Sv_Dyn_Array_Type, Sv_Dyn_Array_Ptr);
   begin
      Free (Arr);
   end Delete;
end Verilog.Sv_Arrays;
