with Types; use Types;

package Verilog.Abi is
   --  For pointers.
   Ptr_Size : constant := Standard'Address_Size / 8;
   Ptr_Align : constant := Ptr_Size;

   --  For SV strings.
   Sv_String_Size : constant := Ptr_Size;
   Sv_String_Align : constant := Ptr_Align;

   --  For real/shortreal.
   Real_Size : constant := Fp64'Size / 8;
   Real_Align : constant := Fp64'Alignment;

   Shortreal_Size : constant := Fp32'Size / 8;
   Shortreal_Align : constant := Fp32'Alignment;
end Verilog.Abi;
