with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_IO; use Ada.Text_IO;
with GNAT.OS_Lib; use GNAT.OS_Lib;
with GNAT.Directory_Operations;
with Types; use Types;
with Name_Table;
with Std_Names;
with Files_Map;
with Errorout; use Errorout;
with Errorout.Console;
with Verilog.Flags; use Verilog.Flags;
with Verilog.Nodes; use Verilog.Nodes;
with Verilog.Scans; use Verilog.Scans;
with Verilog.Parse;
with Verilog.Sem;
with Verilog.Sem_Types;
with Verilog.Sem_Scopes;
with Verilog.Standard;
with Verilog.Options;
with Verilog.Errors;
with Verilog.Disp_Verilog;
with Verilog.Disp_Tree;
with Verilog.Disp_Preproc;
with Verilog.Elaborate; use Verilog.Elaborate;
with Verilog.Allocates;
with Verilog.Simulation;
with Verilog.Nutils; use Verilog.Nutils;
with Verilog.Vpi;
pragma Unreferenced (Verilog.Vpi);

procedure Vlg
is
   procedure Usage is
   begin
      Put_Line ("usage: " & Command_Name & " [options] FILEs");
   end Usage;

   --  From ghdllocal:

   --  Installation prefix (deduced from executable path).
   Exec_Prefix : String_Access;

   function Is_Directory_Separator (C : Character) return Boolean is
   begin
      return C = '/' or else C = Directory_Separator;
   end Is_Directory_Separator;

   function Get_Basename_Pos (Pathname : String) return Natural is
   begin
      for I in reverse Pathname'Range loop
         if Is_Directory_Separator (Pathname (I)) then
            return I;
         end if;
      end loop;
      return Pathname'First - 1;
   end Get_Basename_Pos;

   function Is_Basename (Pathname : String) return Boolean is
   begin
      return Get_Basename_Pos (Pathname) < Pathname'First;
   end Is_Basename;

   --  Simple lower case conversion, used to compare with "bin".
   function To_Lower (S : String) return String
   is
      Res : String (S'Range);
      C : Character;
   begin
      for I in S'Range loop
         C := S (I);
         if C >= 'A' and then C <= 'Z' then
            C := Character'Val
              (Character'Pos (C) - Character'Pos ('A') + Character'Pos ('a'));
         end if;
         Res (I) := C;
      end loop;
      return Res;
   end To_Lower;

   procedure Set_Prefix_From_Program_Path (Prog_Path : String)
   is
      Last : Natural;
   begin
      Last := Get_Basename_Pos (Prog_Path);
      if Last < Prog_Path'First then
         --  No directory in Prog_Path.  This is not expected.
         return;
      end if;

      declare
         Pathname : String :=
           Normalize_Pathname (Prog_Path (Last + 1 .. Prog_Path'Last),
                               Prog_Path (Prog_Path'First .. Last - 1));
         Pos : Natural;
      begin
         --  Stop now in case of error.
         if Pathname'Length = 0 then
            return;
         end if;

         --  Skip executable name
         Last := Get_Basename_Pos (Pathname);
         if Last < Pathname'First then
            return;
         end if;

         --  Simplify path:
         --    /./ => /
         --    // => /
         Pos := Last - 1;
         while Pos >= Pathname'First loop
            if Is_Directory_Separator (Pathname (Pos)) then
               if Is_Directory_Separator (Pathname (Pos + 1)) then
                  --  // => /
                  Pathname (Pos .. Last - 1) := Pathname (Pos + 1 .. Last);
                  Last := Last - 1;
               elsif Pos + 2 <= Last
                 and then Pathname (Pos + 1) = '.'
                 and then Is_Directory_Separator (Pathname (Pos + 2))
               then
                  --  /./ => /
                  Pathname (Pos .. Last - 2) := Pathname (Pos + 2 .. Last);
                  Last := Last - 2;
               end if;
            end if;
            Pos := Pos - 1;
         end loop;

         --  Simplify path:
         --    /xxx/../ => /
         --  Do it forward as xxx/../../ must not be simplified as xxx/
         --  This is done after the previous simplification to avoid to deal
         --  with cases like /xxx//../ or /xxx/./../
         Pos := Pathname'First;
         while Pos <= Last - 3 loop
            if Is_Directory_Separator (Pathname (Pos))
              and then Pathname (Pos + 1) = '.'
              and then Pathname (Pos + 2) = '.'
              and then Is_Directory_Separator (Pathname (Pos + 3))
            then
               declare
                  Last_Dir : Natural;
                  Len : Natural;
               begin
                  --  Search backward
                  Last_Dir := Pos;
                  loop
                     if Last_Dir = Pathname'First then
                        Last_Dir := Pos;
                        exit;
                     end if;
                     Last_Dir := Last_Dir - 1;
                     exit when Is_Directory_Separator (Pathname (Last_Dir));
                  end loop;

                  --  /xxxxxxxxxx/../
                  --  ^          ^
                  --  Last_Dir   Pos
                  Len := Pos + 3 - Last_Dir;
                  Pathname (Last_Dir + 1 .. Last - Len) :=
                    Pathname (Pos + 4 .. Last);
                  Last := Last - Len;
                  Pos := Last_Dir;
               end;
            else
               Pos := Pos + 1;
            end if;
         end loop;

         --  Remove last '/'
         Last := Last - 1;

         --  Skip '/bin' directory if present
         Pos := Get_Basename_Pos (Pathname (Pathname'First .. Last));
         if Pos < Pathname'First then
            return;
         end if;
         if To_Lower (Pathname (Pos + 1 .. Last)) = "bin" then
            Last := Pos - 1;
         end if;

         Exec_Prefix := new String'(Pathname (Pathname'First .. Last));
      end;
   end Set_Prefix_From_Program_Path;

   --  Extract Exec_Prefix from executable name.
   procedure Set_Exec_Prefix
   is
      use GNAT.Directory_Operations;
      Prog_Path : constant String := Ada.Command_Line.Command_Name;
      Exec_Path : String_Access;
   begin
      --  If the command name is an absolute path, deduce prefix from it.
      if Is_Absolute_Path (Prog_Path) then
         Set_Prefix_From_Program_Path (Prog_Path);
         return;
      end if;

      --  If the command name is a relative path, deduce prefix from it
      --  and current path.
      if not Is_Basename (Prog_Path) then
         if Is_Executable_File (Prog_Path) then
            Set_Prefix_From_Program_Path
              (Get_Current_Dir & Directory_Separator & Prog_Path);
         end if;
         return;
      end if;

      --  Look for program name on the path.
      Exec_Path := Locate_Exec_On_Path (Prog_Path);
      if Exec_Path /= null then
         Set_Prefix_From_Program_Path (Exec_Path.all);
         Free (Exec_Path);
      end if;
   end Set_Exec_Prefix;

   --  -de : disp elaborated design tree.
   Flag_Dump_Elab : Boolean := False;

   --  -dp : disp parse design tree.
   Flag_Dump_Parse : Boolean := False;

   --  -le : disp elaborated design as verilog.
   Flag_List_Elab : Boolean := False;

   --  -lp : disp parsed module as verilog
   Flag_List_Parse : Boolean := False;

   --  -du : dump updates table after elaboration
   Flag_Dump_Updates : Boolean := False;

   --  -E : preprocess only
   Flag_Preprocessor : Boolean := False;

   --  -Es : disp preprocessed tokens as a stream.
   Flag_Pp_Stream : Boolean := False;

   --  --no-elab: stop before elaboration.
   Flag_No_Elab : Boolean := False;

   --  --disp-simulation-time-unit
   Flag_Disp_Simulation_Time_Unit : Boolean := False;

   Flag_Expect_Failure : Boolean := False;

   Flag_Stop : Boolean := False;

   --  Use std package.  That's the default.
   Flag_Std : Boolean := True;

   --  Automatically load uvm_pkg.sv before user files.
   Flag_Uvm : Boolean := False;

   Id : Name_Id;
   Dir_Id : Name_Id;
   Sfe : Source_File_Entry;
   N : Natural;
   Res : Node;
   First_File, Last_File : Node;
   Root : Node;
begin
   --  Failure by default.
   Set_Exit_Status (Failure);

   Name_Table.Initialize;
   Std_Names.Std_Names_Initialize;
   Files_Map.Initialize;
   Errorout.Console.Install_Handler;
   Errorout.Console.Set_Program_Name (Command_Name);
   Verilog.Errors.Initialize;
   Verilog.Scans.Init_Pathes;
   Verilog.Sem_Types.Create_Basetypes;

   Set_Exec_Prefix;

   --  Decode options.
   N := 1;
   while N <= Argument_Count loop
      declare
         Opt : constant String := Argument (N);
         pragma Assert (Opt'First = 1);
      begin
         exit when Opt (1) /= '-' and Opt (1) /= '+';

         if Opt = "-E" then
            Flag_Preprocessor := True;
         elsif Opt = "-Es" then
            Flag_Preprocessor := True;
            Flag_Pp_Stream := True;
         elsif Opt = "-Esynth" then
            Flag_Preprocessor := True;
            Flag_Pragma_Comment := True;
         elsif Opt = "-de" then
            Flag_Dump_Elab := True;
         elsif Opt = "-dp" then
            Flag_Dump_Parse := True;
         elsif Opt = "-le" then
            Flag_List_Elab := True;
         elsif Opt = "-lp" then
            Flag_List_Parse := True;
            Verilog.Flags.Flag_Keep_Parentheses := True;
         elsif Opt = "--list-implicit-type" then
            Verilog.Disp_Verilog.Flag_Disp_Implicit_Type := True;
         elsif Opt = "--list-implicit-cast" then
            Verilog.Disp_Verilog.Flag_Disp_Implicit_Cast := True;
         elsif Opt = "--list-port-omitted" then
            Verilog.Disp_Verilog.Flag_Disp_Port_Omitted := True;
         elsif Opt = "--no-elab" then
            Flag_No_Elab := True;
         elsif Opt = "-du" then
            Flag_Dump_Updates := True;
         elsif Opt = "--trace-values" then
            Verilog.Simulation.Flag_Trace_Values := True;
            Verilog.Simulation.Flag_Trace_Memories := False;
            Verilog.Simulation.Flag_Trace_Time := True;
         elsif Opt = "--trace-time" then
            Verilog.Simulation.Flag_Trace_Time := True;
         elsif Opt = "--trace-exec" then
            Verilog.Simulation.Flag_Trace_Exec := True;
         elsif Opt = "-t" then
            Verilog.Simulation.Flag_Trace := True;
         elsif Opt = "--expect-failure" then
            Flag_Expect_Failure := True;
         elsif Opt = "-s" then
            Flag_Stop := True;
         elsif Opt = "--disp-simulation-time-unit" then
            Flag_Disp_Simulation_Time_Unit := True;
         elsif Opt = "--no-std" then
            Flag_Std := False;
         elsif Opt = "--uvm" then
            Flag_Uvm := True;
         elsif not Verilog.Options.Parse_Option (Opt) then
            Put_Line ("unknown option '" & Opt & ''');
            Usage;
            return;
         end if;
      end;
      N := N + 1;
   end loop;

   --  Stop now if no arguments.
   if N > Argument_Count then
      Usage;
      return;
   end if;

   --  By default, use the same standard for keywords.
   Verilog.Scans.Keywords_Std := Verilog.Flags.Std;

   --  Initialize scopes.
   Verilog.Sem_Scopes.Init;

   Verilog.Vpi.Initialize;

   Init_Chain (First_File, Last_File);

   if Flag_Std and then Std > Verilog_Sv_3_1 then
      --  Load the built-in std package.
      --  FIXME: check standard.
      Dir_Id := Name_Table.Get_Identifier
        (Exec_Prefix.all & Directory_Separator);
      Id := Name_Table.Get_Identifier ("std.sv");
      Sfe := Files_Map.Read_Source_File (Dir_Id, Id);
      if Sfe = No_Source_File_Entry then
         Error_Msg_Option ("cannot open %i", (1 => +Id));
      end if;
      Res := Verilog.Parse.Parse_File (Sfe);
      Verilog.Sem.Sem_Compilation_Unit (Res);

      pragma Assert (Get_Kind (Res) = N_Compilation_Unit);
      Verilog.Standard.Built_In_Std_Unit := Res;
      Res := Get_Descriptions (Res);
      pragma Assert (Get_Kind (Res) = N_Package);
      pragma Assert (Get_Chain (Res) = Null_Node);
      Verilog.Standard.Built_In_Std_Package := Res;
   end if;

   if Flag_Uvm then
      Dir_Id := Name_Table.Get_Identifier
        (Exec_Prefix.all & Directory_Separator
           & "uvm-src" & Directory_Separator);
      Id := Name_Table.Get_Identifier ("uvm_pkg.sv");
      Sfe := Files_Map.Read_Source_File (Dir_Id, Id);
      if Sfe = No_Source_File_Entry then
         Error_Msg_Option ("cannot open %i", (1 => +Id));
      end if;
      Res := Verilog.Parse.Parse_File (Sfe);
      Verilog.Sem.Sem_Compilation_Unit (Res);
      Append_Chain (First_File, Last_File, Res);
   end if;

   Free (Exec_Prefix);

   --  Parse files on the command line.
   while N <= Argument_Count loop
      --  Load the file.
      Id := Name_Table.Get_Identifier (Argument (N));
      Dir_Id := Null_Identifier;
      Files_Map.Normalize_Pathname (Dir_Id, Id);
      Sfe := Files_Map.Read_Source_File (Dir_Id, Id);
      if Sfe = No_Source_File_Entry then
         Error_Msg_Option ("cannot open " & Argument (N));
         return;
      end if;

      --  Parse file.
      if Flag_Preprocessor then
         Set_File (Sfe);

         if Flag_Pp_Stream then
            Verilog.Disp_Preproc.Disp_Tokens;
         else
            Verilog.Disp_Preproc.Disp_Preprocessor;
         end if;

         Close_File;
      else
         Res := Verilog.Parse.Parse_File (Sfe);

         if Flag_List_Parse then
            Verilog.Disp_Verilog.Disp_Source (Res);
         end if;

         --  Analyze the compilation unit.
         --  FIXME: add support for one compilation unit ?
         Verilog.Sem.Sem_Compilation_Unit (Res);

         if Flag_Dump_Parse then
            Verilog.Disp_Tree.Disp_Tree (Res);
         end if;

         Append_Chain (First_File, Last_File, Res);
      end if;

      --  Next file.
      N := N + 1;
   end loop;

   --  Exit now if only preprocessing.
   if Flag_Preprocessor then
      return;
   end if;

   --  Exit now in case of parse error.
   if Errorout.Nbr_Errors /= 0 then
      if Flag_Expect_Failure then
         Set_Exit_Status (Success);
      end if;
      return;
   end if;

   if Flag_No_Elab then
      if not Flag_Expect_Failure then
         Set_Exit_Status (Success);
      end if;
      return;
   end if;

   --  Elaborate: build hierarchy, finish analysis.
   Verilog.Elaborate.Units_Chain := First_File;
   Root := Verilog.Elaborate.Elab_Design;

   if Errorout.Nbr_Errors /= 0 then
      if Flag_Expect_Failure then
         Set_Exit_Status (Success);
      end if;
      return;
   end if;

   if Flag_Disp_Simulation_Time_Unit then
      declare
         use Verilog.Parse;
      begin
         Put ("simulation time unit: ");
         if Simulation_Time_Unit = Unset_Simulation_Time_Unit then
            Put ("unset");
         else
            case Simulation_Time_Unit mod 3 is
               when 0 => Put ("1");
               when 1 => Put ("10");
               when 2 => Put ("100");
               when others =>
                  raise Program_Error;
            end case;
            case (Simulation_Time_Unit - 2) / 3 is
               when 0 => Put ("s");
               when -1 => Put ("ms");
               when -2 => Put ("us");
               when -3 => Put ("ns");
               when -4 => Put ("ps");
               when -5 => Put ("fs");
               when others => Put ("??");
            end case;
         end if;
         New_Line;
      end;
   end if;

   if Flag_List_Elab then
      Verilog.Disp_Verilog.Disp_Module (Root);
   end if;
   if Flag_Dump_Elab then
      Verilog.Disp_Tree.Disp_Tree (Root);
   end if;

   if False then
      Verilog.Disp_Verilog.Disp_Source (First_File);
   end if;
   if False then
      Verilog.Disp_Tree.Disp_Tree (First_File);
   end if;

   Verilog.Vpi.End_Of_Compile;

   Verilog.Allocates.Allocate_Resources (First_File, Root);

   if Flag_Dump_Updates then
      Verilog.Allocates.Disp_All_Vars_Update;
   end if;

   if Flag_Stop then
      Verilog.Vpi.Vpip_Control := Natural (Verilog.Vpi.VpiStop);
      Verilog.Vpi.Interractive_Scope := Root;
      Verilog.Vpi.Interractive_Frame := null;
   end if;

   Verilog.Vpi.Blocking_Assign :=
     Verilog.Simulation.Blocking_Assign_Lvalue'Access;

   Verilog.Simulation.Run (Root);

   Verilog.Vpi.End_Of_Simulation;

   if (Flag_Expect_Failure and Verilog.Vpi.Vpip_Exit_Status /= 0)
     or (not Flag_Expect_Failure and Verilog.Vpi.Vpip_Exit_Status = 0)
   then
      Set_Exit_Status (Success);
   end if;
exception
   when Fatal_Error =>
      null;
   when Runtime_Error =>
      if Flag_Expect_Failure then
         Set_Exit_Status (Success);
      end if;
end Vlg;
