with Types; use Types;
with Verilog.Nodes; use Verilog.Nodes;

--  1800-2017 23.8 Upwards name referencing

package Verilog.Sem_Upwards is
   --  Start with the root instance.
   --  Add ROOT and the submodules.
   procedure Init (Root : Node);

   --  Enter within a scope.
   --  N must be either a subroutine name, a module, program or interface
   --  instance name or a generate block name.
   --  Add the scopes within N.  If N is an instance (module instance,
   --  program instance or interface instance), the name of the module,
   --  program or interface is also added.
   --  There is no check for duplicated names, this is done by the normal
   --  namespace rules (cf sem_scopes).
   procedure Enter_Scope (N : Node);

   --  Leave the last entered scope.  Remove them from the table.
   procedure Leave_Scope;

   --  Find the scope named ID.  Return it or Null_Node if no such scope.
   function Find_Scope (Id : Name_Id) return Node;
end Verilog.Sem_Upwards;
