with Vhdl.Types;

package Verilog.Vhdl_Export is

   function Convert_Unit_To_Vhdl (N : Vhdl.Types.Vhdl_Node) return Boolean;
end Verilog.Vhdl_Export;
