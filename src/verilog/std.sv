// 1800-2017 26.7 the std built-in package
package std;
   // 1800-2017 15.3 Semaphores
   class semaphore;
     function new(int keyCount = 0);
     endfunction

     function void put(int keyCount = 1);
     endfunction

     task get(int keyCount = 1);
     endtask

     function int try_get(int keyCount = 1);
     endfunction
   endclass

   // 1800-2017 15.4 Mailboxes
   class mailbox #(type T /* = dynamic_singular_type */);
      function new(int bount = 0);
      endfunction

      function int num();
      endfunction

      task put (T message);
      endtask

      function int try_put (T message);
      endfunction

      task get (ref T message);
      endtask

      function int try_get (ref T message);
      endfunction

      task peek (ref T message);
      endtask

      function int try_peek (ref T message);
      endfunction
   endclass

   // 1800-2017 9.7 Fine-grain process control
   // Note: declaration in annex G is incorrect.
   class process;
      typedef enum { FINISHED, RUNNING, WAITING, SUSPENDED, KILLED } state;

      static function process self();
      endfunction

      function state status();
      endfunction

      function void kill();
      endfunction

      task await();
      endtask

      function void suspend();
      endfunction

      function void resume();
      endfunction

      function void srandom (int seed);
      endfunction

      function string get_randstate();
      endfunction

      function void set_randstate (string state);
      endfunction
   endclass
endpackage
