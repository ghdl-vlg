with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_IO; use Ada.Text_IO;
with Types; use Types;
with Name_Table;
with Std_Names;
with Files_Map;
with Flags;

with Verilog.Scans; use Verilog.Scans;
with Verilog.Nodes; use Verilog.Nodes;
with Verilog.Errors;
with Verilog.Parse;
with Verilog.Options;
with Verilog.Disp_Verilog;
with Verilog.Disp_Tree;
with Verilog.Sem;
with Verilog.Elaborate;
with Verilog.Translate;

with Ada.Unchecked_Conversion;
with Interfaces;
with Interfaces.C;
with System; use System;
with Ortho_Mcode; use Ortho_Mcode;
with Ortho_Code.Binary;
with Ortho_Code.Debug;
with Binary_File;
with Binary_File.Memory;

with Grt.Types;
with Grt.Processes;
with Grt.Systasks;
with Grt.Options;
with Grt.Main;

package body Vlgrun is
   procedure Usage is
   begin
      Put_Line ("usage: " & Command_Name & " FILE");
   end Usage;

   procedure Init is
   begin
      Name_Table.Initialize;
      Std_Names.Std_Names_Initialize;
      Verilog.Scans.Init_Pathes;
      Verilog.Parse.Create_Basetypes;
   end Init;

   Flag_Disp_Verilog : Boolean := False;

   function Parse (Filename : String) return Boolean
   is
      Id : Name_Id;
      Sfe : Source_File_Entry;
      Res : Node;
      First, Last : Node;
   begin
      First := Null_Node;
      Last := Null_Node;

      Id := Name_Table.Get_Identifier (Filename);
      Sfe := Files_Map.Load_Source_File (Null_Identifier, Id);
      if Sfe = No_Source_File_Entry then
         Put_Line (Command_Name & ": cannot open " & Filename);
         return False;
      end if;

      --  Parse file.
      Set_File (Sfe);
      Res := Verilog.Parse.Parse_File;
      Set_Chain (Last, Res);

      --  Sem descriptions.
      if Res /= Null_Node then
         loop
            Verilog.Sem.Sem_Description (Res);
            exit when Get_Chain (Res) = Null_Node;
            Res := Get_Chain (Res);
         end loop;
         Last := Res;
      end if;

      --  Next file.
      --N := N + 1;

      --  A verilog file can be empty.
      if First = Null_Node then
         return True;
      end if;

      Verilog.Elaborate.Elab_Design (First);
      if Flag_Disp_Verilog then
         Verilog.Disp_Verilog.Disp_File (First);
      end if;
      if Boolean'(False) then
         Verilog.Disp_Tree.Disp_Tree (First, 0, True);
      end if;

      Verilog.Translate.Initialize;

      Verilog.Translate.Translate_Module (First);
      Verilog.Translate.Elaborate (First);
      return True;
   exception
      when Verilog.Errors.Parse_Error =>
         return False;
   end Parse;

   --  Toplevel function, defined by grt.
   Flag_String : String (1 .. 5);
   pragma Export (C, Flag_String, "__ghdl_flag_string");

   procedure Ghdl_Elaborate;
   pragma Export (C, Ghdl_Elaborate, "__ghdl_ELABORATE");

   type Elaborate_Acc is access procedure;
   Elaborate_Proc : Elaborate_Acc := null;

   procedure Ghdl_Elaborate is
   begin
      --Ada.Text_IO.Put_Line (Standard_Error, "ghdl_elaborate");
      Elaborate_Proc.all;
   end Ghdl_Elaborate;

   Std_Standard_Bit_RTI_Ptr : Address := Null_Address;

   Std_Standard_Boolean_RTI_Ptr : Address := Null_Address;

   pragma Export (C, Std_Standard_Bit_RTI_Ptr,
                  "std__standard__bit__RTI_ptr");

   pragma Export (C, Std_Standard_Boolean_RTI_Ptr,
                  "std__standard__boolean__RTI_ptr");


   function Malloc (Sz : Integer) return System.Address;
   pragma Import (C, Malloc);

--     function Shift_Left (V : Ghdl_U32; C : Natural) return Ghdl_U32;
--     pragma Import (Intrinsic, Shift_Left);

--     function Shift_Right (V : Ghdl_U32; C : Natural) return Ghdl_U32;
--     pragma Import (Intrinsic, Shift_Right);


   procedure Def (Decl : O_Dnode; Addr : Address)
   is
      use Ortho_Code.Binary;
   begin
      Binary_File.Memory.Set_Symbol_Address (Get_Decl_Symbol (Decl), Addr);
   end Def;

   function Get_Address (Decl : O_Dnode) return Address
   is
      use Interfaces;
      use Ortho_Code.Binary;

      function Conv is new Ada.Unchecked_Conversion
        (Source => Unsigned_32, Target => Address);
   begin
      return Conv (Binary_File.Get_Symbol_Vaddr (Get_Decl_Symbol (Decl)));
   end Get_Address;

   procedure Run
   is
      First_File : Natural;
      Err : Boolean;

      function Conv is new Ada.Unchecked_Conversion
        (Source => Address, Target => Elaborate_Acc);
   begin
      Init;

      First_File := 0;
      for I in 1 .. Argument_Count loop
         declare
            Arg : constant String := Argument (I);
         begin
            if Arg (1) = '-' then
               if Arg'Length > 5 and then Arg (1 .. 5) = "--be-" then
                  Ortho_Code.Debug.Set_Be_Flag (Arg);
               elsif Verilog.Options.Parse_Option (Arg) then
                  null;
               elsif Arg = "-ls" then
                  Flag_Disp_Verilog := True;
               else
                  Usage;
                  return;
               end if;
            else
               First_File := I;
               exit;
            end if;
         end;
      end loop;
      if First_File = 0 then
         Put_Line (Command_Name & ": missing filename");
         return;
      end if;
      Ortho_Mcode.Init;
      if not Parse (Argument (First_File)) then
         return;
      end if;
      Ortho_Mcode.Finish;

      Binary_File.Memory.Write_Memory_Init;

      Def (Verilog.Translate.Ghdl_Malloc, Malloc'Address);
      Def (Verilog.Translate.Ghdl_Initial_Register,
           Grt.Processes.Ghdl_Initial_Register'Address);
      Def (Verilog.Translate.Ghdl_Always_Register,
           Grt.Processes.Ghdl_Always_Register'Address);
      Def (Verilog.Translate.Ghdl_Process_Delay,
           Grt.Processes.Ghdl_Process_Delay'Address);
      Def (Verilog.Translate.Ghdl_Systask_Display,
           Grt.Systasks.Ghdl_Systask_Display'Address);
      Def (Verilog.Translate.Ghdl_Systask_Finish,
           Grt.Systasks.Ghdl_Systask_Finish'Address);

      Binary_File.Memory.Write_Memory_Relocate (Err);
      if Err then
         return;
      end if;

      --  Set options.
      --  This is a little bit over-kill: from C to Ada and then again to C...
      declare
         use Interfaces.C;
         use Grt.Options;
         use Grt.Types;

         function Malloc (Size : size_t) return Argv_Type;
         pragma Import (C, Malloc);

         function Strdup (Str : String) return Ghdl_C_String;
         pragma Import (C, Strdup);
      begin
         Argc := 1 + Argument_Count - First_File;
         Argv := Malloc
           (size_t (Argc * (Ghdl_C_String'Size / System.Storage_Unit)));
         Argv (0) := Strdup (Ada.Command_Line.Command_Name & Grt.Types.NUL);
         Progname := Argv (0);
         for I in First_File + 1 .. Argument_Count loop
            Argv (I - First_File) := Strdup (Argument (I) & Grt.Types.NUL);
         end loop;
      end;

      Flags.Create_Flag_String;
      Flag_String := Flags.Flag_String;

      Put_Line ("Ready to run...");
      Elaborate_Proc := Conv (Get_Address (Verilog.Translate.Ghdl_Elaborate));
      Grt.Main.Run;
   end Run;

   Ieee_Std_Logic_1164_Resolved_Resolv_Ptr : Address := Null_Address;
   pragma Export (C, Ieee_Std_Logic_1164_Resolved_Resolv_Ptr,
                  "ieee__std_logic_1164__resolved_RESOLV_ptr");

end Vlgrun;

