with System;
with Ada.Unchecked_Conversion;
with Types; use Types;
with Verilog.Storages; use Verilog.Storages;

package Verilog.Sv_Arrays is
   type Sv_Dyn_Array_Type (Ssize : Storage_Index) is record
      --  Number of elements.
      Size : Int32;
      --  The data.
      Base : Storage_Type (1 .. Ssize);
   end record;
   type Sv_Dyn_Array_Ptr is access all Sv_Dyn_Array_Type;

   type Sv_Dyn_Array_Ptr_Ptr is access all Sv_Dyn_Array_Ptr;

   function To_Sv_Dyn_Array_Ptr_Ptr is
      new Ada.Unchecked_Conversion (System.Address, Sv_Dyn_Array_Ptr_Ptr);

   procedure Delete (Arr : in out Sv_Dyn_Array_Ptr);
end Verilog.Sv_Arrays;
