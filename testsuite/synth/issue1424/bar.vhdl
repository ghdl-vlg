library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity bar is
  port (
    input1    : in unsigned(3 downto 0);
    output1   : out std_logic
  );
end bar;

architecture bar of bar is
begin
  output1 <= '1' when input1(3 downto 0) = conv_unsigned(7, 4)
                else '0';
end bar;


