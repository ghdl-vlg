library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity bug is
	generic(
		LEN : positive := 32
	);
	port(
		input1  :  in unsigned(LEN-1 downto 0);
		output1 : out unsigned(LEN-1 downto 0)
	);
end bug;

architecture behav of bug is
begin
	output1 <= input1 and not to_unsigned(4096-1, input1'length);
end architecture;
