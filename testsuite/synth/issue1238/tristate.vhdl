library ieee;
use ieee.std_logic_1164.all;

entity tristate is
  port (i, en : std_logic;
        o : out std_logic);
end tristate;

architecture behav of tristate is
begin
  o <= i when en = '1' else 'Z';
end behav;
