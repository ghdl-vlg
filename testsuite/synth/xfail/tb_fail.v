module tb_fail;
   reg a;

   initial begin
      a <= 0;
      # 1;
      if (!a)
        $fatal(1, "bad result");
      $finish;
   end
endmodule
