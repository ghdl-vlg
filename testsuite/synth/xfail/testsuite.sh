#! /bin/sh

. ../../testenv.sh

if verilog_run tb_fail.v; then
    echo "FAILED expected"
    exit 1
else
    echo "XFAILED"
fi

echo "Test successful"
